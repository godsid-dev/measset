<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('dateThai')) 
{
	function dateThai($format,$time="")
	{
		$CI =& get_instance();
		if(empty($time)){
			$time = time();
		}
		$output = $format;
		$thaiDate = $CI->config->item('thaidate');
		$output = str_replace("Y", (date("Y",$time)+543), $output);
		$output = str_replace("y", substr((date("Y",$time)+543), 2), $output);
		$output = str_replace("F", $thaiDate['monthFull'][date("n",$time)], $output);
		$output = str_replace("M", $thaiDate['monthCut'][date("n",$time)], $output);
		$output = str_replace("D", $thaiDate['dayFull'][date("N",$time)], $output);
		$output = str_replace("l", $thaiDate['dayCut'][date("N",$time)], $output);

		return date($output,$time);
	}
}