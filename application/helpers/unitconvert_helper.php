<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function toThaiMatric($wah){
	return array(
			'rai'=>floor($wah/400),
			'ngan'=>floor(($wah%400)/100),
			'wah'=>(($wah%400)%100),
		);
}

function toThaiWah($squareMeter){
	return floor($squareMeter/2);
}


