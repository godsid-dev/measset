<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



if ( !function_exists('assets_url') ) 
{
	function assets_url($url = '')
	{
		$CI =& get_instance();
		if (is_array($url)) 
		{
			$url = implode('/', $url);
		}
		return ($url == '') ? $CI->config->slash_item('assets_url') : $CI->config->slash_item('assets_url').trim($url, '/');
	}
}

if ( !function_exists('site_url') ) 
{
	function site_url($url = '')
	{
		$CI =& get_instance();
		if (is_array($url)) 
		{
			$url = implode('/', $url);
		}

		return ($url == '') ? $CI->config->item('base_url') : $CI->config->slash_item('base_url').trim($url, '/');	
	}
}

if ( !function_exists('vendor_url') ) 
{
	function vendor_url($url = '')
	{
		$CI =& get_instance();
		if (is_array($url)) 
		{
			$url = implode('/', $url);
		}
		return ($url == '') ? $CI->config->item('vendor_url') : $CI->config->slash_item('vendor_url').trim($url, '/');	
	}
}
if ( !function_exists('static_url') ) 
{
	function static_url($url = '')
	{
		$CI =& get_instance();
		if (is_array($url)) 
		{
			$url = implode('/', $url);
		}

		return ($url == '') ? $CI->config->item('upload_url') : $CI->config->slash_item('upload_url').trim($url, '/');	
	}
}
if ( !function_exists('upload_url') ) 
{
	function upload_url($url = ''){
		return static_url($url);			
	}
}

if ( !function_exists('engines_url') ) 
{
	function engines_url($url = ''){
		$CI =& get_instance();
		if (is_array($url)) 
		{
			$url = implode('/', $url);
		}

		return ($url == '') ? $CI->config->item('engines_url') : $CI->config->slash_item('engines_url').trim($url, '/');	
	}
}

?>