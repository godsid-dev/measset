<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Paging{

		var $correntpage;
		var $startpage;
		var $endpage;
		var $prevpage;
		var $nextpage; 
		var $firstpage;
		var $lastpage;
		var $linkto = '';

		function Paging(){

		}

		function set($TotalRecordCount,$ShowPerPage,$ShowPage,$Page,$fpage=1){
			$TotalPage = ceil($TotalRecordCount/$ShowPerPage);

			if($ShowPage % 2 == 0){
				$CBackPage = $ShowPage/2;
				$CNextPage = $CBackPage-1;
			}else{
				$CBackPage = intval($ShowPage/2);
				$CNextPage = $CBackPage;
			}

			$PreviousPage = $Page-1;
			$NextPage = $Page+1;

			$LimitStartPage = ($TotalPage-$ShowPage)+1;
			if($LimitStartPage < 1){
				$LimitStartPage = 1;
			}
			$ChkStartPage = $Page-$CBackPage;
			if($ChkStartPage < 1){
				$StartPage = 1;
			}else if($ChkStartPage > $LimitStartPage){
				$StartPage = $LimitStartPage;
			}else{
				$StartPage = $ChkStartPage;
			}

			$LimitEndPage = $ShowPage;
			if($LimitEndPage > $TotalPage){
				$LimitEndPage = $TotalPage;
			}
			$ChkEndPage = $Page+$CNextPage;
			if($ChkEndPage > $TotalPage){
				$EndPage = $TotalPage;
			}else if($ChkEndPage < $LimitEndPage){
				$EndPage = $LimitEndPage;
			}else{
				$EndPage = $ChkEndPage;
			}

			$this->currentpage = $Page;
			$this->prevpage = $PreviousPage;
			$this->nextpage = $NextPage;
			$this->startpage = $StartPage;
			$this->endpage = $EndPage;
			$this->firstpage =$fpage;
			$this->lastpage = $TotalPage;

			//return $this->prevpage."-".$this->nextpage."-".$this->startpage."-".$this->endpage;
		}

		function start(){
			return $this->startpage;
		}

		function end(){
			return $this->endpage;
		}

		function prev(){
			return $this->prevpage;
		}

		function next(){
			return $this->nextpage;
		}

		function first(){
			return $this->firstpage;
		}

		function last(){
			return $this->lastpage;
		}

		function linkto($url, $query_string=NULL)
		{	
			if(!is_null($query_string)):
				$tmp	= explode("&",$query_string);
				if($tmp[0]!=''):
					$query	= array();
					foreach($tmp as $tmp_data):
						list($param, $value)	= explode("=",$tmp_data);
						if($param != 'p'):
							$query[]	= $tmp_data;
						endif;
					endforeach;
					$query_string	= count($query)>0?implode("&",$query):"";
				endif;
			endif;

			$this->linkto = $url."?".$query_string;
			return $this;
		}

		function create_link($page_no){
			$url = $this->linkto;
			$exp_link = explode('?',$this->linkto);
			if(count($exp_link) == 1){
				$url .= '?p='.$page_no;
			}else{
				if($exp_link[1] == ''){
					$url .= 'p='.$page_no;
				}else{
					$url .= '&p='.$page_no;
				}
			}
			return $url;
		}

		function myRender(){
			$paging = '<div class="row">';
			$paging .= '<div class="col-sm-6">';
			$paging .= '<div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">';
			$paging .= 'หน้า '.$this->currentpage.' / '.$this->endpage;
			$paging .= '</div>';
			$paging .= '</div>';
			$paging .= '<div class="col-sm-6">';
			$paging .= '<div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">';
			$paging .= '<ul class="pagination">';


				if($this->prevpage >= 1){
					$paging .='<li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="'.$this->create_link($this->firstpage).'"> first </a></li>';
					$paging .='<li class="paginate_button previous" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="'.$this->create_link($this->prevpage).'"> &laquo; </a></li>';
				}


				for($i=$this->startpage;$i<=$this->endpage;$i++){
					if($i == $this->currentpage){
						$paging .= '<li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="javascript:;">'.$i.'</a></li>';
					}else{
						$paging .= '<li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="'.$this->create_link($i).'">'.$i.'</a></li>';
					}

				}


				if($this->nextpage <= $this->lastpage){
					$paging .= '<li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="'.$this->create_link($this->nextpage).'">&raquo;</a></li>';
					$paging .= '<li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="'.$this->create_link($this->lastpage).'">last</a></li>';

				}

			$paging .= '</ul>';
			$paging .= '</div>';
			$paging .= '</div>';
			$paging .= '</div>';
			return $paging;

		}

		function render(){


			$paging = '<div class="ctrl-paging">';

			$paging .= '<p><span class="result">หน้า '.$this->currentpage.' / '.$this->endpage.'</span>';

			if($this->prevpage >= 1){
				$paging .= '<a href="'.$this->create_link($this->firstpage).'">first</a>&nbsp;<a href="'.$this->create_link($this->prevpage).'">&laquo;</a>';
			}

			for($i=$this->startpage;$i<=$this->endpage;$i++){
				if($i == $this->currentpage){
					$paging .= '&nbsp;<strong>'.$i.'</strong>';
				}else{
					$paging .= '&nbsp;<a href="'.$this->create_link($i).'">'.$i.'</a>';
				}

			}

			if($this->nextpage <= $this->lastpage){
				$paging .= '&nbsp;<a href="'.$this->create_link($this->nextpage).'">&raquo;</a>&nbsp;<a href="'.$this->create_link($this->lastpage).'">last</a>';
			}

			/*if($this->lastpage > $this->currentpage){
				$paging .= '<a href="#">&raquo;</a>';
			}*/

			$paging .= '</p></div>';

			return $paging;
		}

	}
?>