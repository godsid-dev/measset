<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
class File
{
	
	private $CI;
	private $pathPrefix;
	public $error;
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('upload');
		$this->CI->load->library('image_lib');
		$this->CI->load->helper('file');

		$this->pathPrefix = $this->CI->config->item('upload_dir');
	}

	public function uploadImageTemp($name){
		return $this->uploadImage('temp',$name);
	}

	public function uploadMemberImage($name){
		$path = $this->uploadImage('members',$name);
		return $this->moveMembersImage($path);
	}

	public function uploadProductImage($name){
		$path =  $this->uploadImage('products',$name);
		return $this->moveProductsImage($path);
	}

	private function uploadImage($type,$name){
		$path = $this->pathPrefix.'/'.$type.date('/Y/md');
		$this->createFolder($path);
		$config = array(
					'upload_path'=>$path,
					'encrypt_name'=>true,
					'allowed_types'=>'gif|jpg|png',
					);
		$this->CI->upload->initialize($config);
		if($this->CI->upload->do_upload($name)){
			$data = $this->CI->upload->data();
			$this->resizeOriginalImage($path.'/'.$data['file_name']);
			return $this->replaceDoubleSlash($path.'/'.$data['file_name']);
		}else{
			$this->error = $this->CI->upload->display_errors();
			return false;
		}
	}

	public function moveMembersImage($sourcePath){
		$destinationPath = preg_replace("#temp/#","members/",$sourcePath);
		$this->createFolder(dirname($destinationPath));
		if(is_file($sourcePath) && is_file($destinationPath)){
			rename($sourcePath, $destinationPath);
		}
		
		return $this->replaceDoubleSlash(preg_replace("#^.*/farms#", "/", $destinationPath));
	}

	public function moveProductsImage($sourcePath){
		$destinationPath = preg_replace("#temp/#","products/",$sourcePath);
		$this->createFolder(dirname($destinationPath));
		rename($sourcePath, $destinationPath);
		return $this->replaceDoubleSlash(preg_replace("#^.*/farms#", "/", $destinationPath));

	}
	public function uploadFileTemp($name){
		$path = $this->pathPrefix.'/valuation'.date('/Ymd');
		$this->createFolder($path);
		$config = array(
					'upload_path'=>$path,
					'encrypt_name'=>true,
					'allowed_types'=>'doc|pdf|jpg|jpeg',
					);
		$this->CI->upload->initialize($config);

		if($this->CI->upload->do_upload($name)){
			$data = $this->CI->upload->data();
			return $this->replaceDoubleSlash($path.'/'.$data['file_name']);
		}else{
			$this->error = $this->CI->upload->display_errors();
			return false;
		}
	}


	public function resizeOriginalImage($path){
		$config = array(
					'source_image'=>$path,
					'maintain_ratio'=>true,
					'quality'=>80,
					'width'=>800,
					'height'=>800
					);
		$this->CI->image_lib->initialize($config);
		if ($this->CI->image_lib->resize()){
			return true;
		}else{
			$this->error = $this->CI->image_lib->display_errors();
			return false;
		}

	}	

	public function createFolder($path){
		$path = str_replace($this->pathPrefix, "", $path);
		$path = explode("/", $path);
		$relativePath = $this->pathPrefix;
		foreach ($path as  $value) {
			if($value != ""){
				$relativePath .= '/'.$value;
				if(!file_exists($relativePath)){
					mkdir($relativePath,DIR_WRITE_MODE);
				}
			}
		}	
	}

	private function replaceDoubleSlash($str=""){
		return str_replace("//","/",$str);
	}

}