<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
class Notification
{
	
	private $CI;
	public $error;
	private $config;
	private $adminEmail = 'banpot.sr@gmail.com';
	private $assessorEmail = 'banpot.sr@gmail.com';
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('email',true);
		$this->CI->load->library('email');
		$this->CI->load->model('message_model','mMessage');

		$this->config = $this->CI->config->item('email');
		
		$this->CI->email->initialize($this->config);
		$this->CI->email->from('webmaster@measset.com','Webmaster MeAsset');
	}

	public function verifyEmail($to,$data){
		$this->CI->email->to($to);
		$this->CI->email->subject('ตรวจสอบยืนยันที่อยู่อีเมลของคุณ');
		$message = $this->CI->load->view('email/verifyEmail',$data,true);
		$this->CI->email->message(nl2br($message));
		return $this->CI->email->send();
	}

	//สมัครเป็นนักลงทุนใหม่
	public function newInvestor(){

	}

	//Admin ยืนยันการเป็นนักลงทุน
	public function adminConfirmInvestor(){

	}
	// ยืนยันการแจ้งโอนเงิน
	public function confirmInvestorTranfers(){

	}

	// ยืนยันการแจ้งโอนเงินสำเร็จ
	public function confirmInvestorTranfersSuccess(){

	}

	//แจ้งลงสินทรัพย์ใหม่
	public function newProduct(){

	}

	//Admin ยืนยันสินทรัพย์
	public function adminConfirmProduct($product){
		//แจ้ง Seller 
		$this->CI->email->to($this->product['seller']['email']);
		$this->CI->email->subject('ตรวจสอบสอบรายการสินทรัพย์ '.$product['title'].' เรียบร้อยแล้ว');
		$message = $this->CI->load->view('email/alertSellerApproveAssessment',$product,true);
		$this->CI->email->message(nl2br($message));
		$this->CI->email->send();

		$message = array(
					'member'=>array('_id'=>$product['seller']['_id']),
					'type'=>'adminConfirmProduct',
					'title'=>'ตรวจสอบสอบรายการสินทรัพย์ '.$product['title'].' เรียบร้อยแล้ว',
					'create_date'=>date('Y-m-d H:i:s'),
					'readed'=>false,
					'message'=>'ตรวจสอบสอบรายการสินทรัพย์ '.$product['title'].' เรียบร้อยแล้ว',
					'data'=>array(
							'product'=>array('_id'=>$product['_id'],'title'=>$product['title'])
							)
					);
		$this->CI->mMessage
			->insert($message);
	}

	//Admin ยกเลิกสินทรัพย์
	public function adminUnConfirmproduct(){

	}
	//บ. ประเมิน กรอกข้อมูล
	public function assessmentSuccess($product){

		$message = array(
					'member'=>array('_id'=>$product['seller']['_id']),
					'type'=>'assessmentSuccess',
					'title'=>'รายการสินทรัพย์ '.$product['title'].' ประเมินเรียบร้อยแล้ว',
					'create_date'=>date('Y-m-d H:i:s'),
					'readed'=>false,
					'message'=>'รายการสินทรัพย์ '.$product['title'].' ประเมินเรียบร้อยแล้ว',
					'data'=>array(
							'product'=>array('_id'=>$product['_id'],'title'=>$product['title'])
							)
					);
		$this->CI->mMessage
			->insert($message);

		//แจ้ง Admin
		$this->CI->email->to($this->adminEmail);
		$this->CI->email->subject('แจ้งผลการประเมินสินทรัพย์');

		$message = $this->CI->load->view('email/alertAdminAssessment',$product,true);
		$this->CI->email->message(nl2br($message));
		try{
			$this->CI->email->send();
		}catch(Exception $e){
			
		}

	}

	//Admin ยืนยันข้อมูลการประเมิน
	public function adminConfirmAssessment($product,$seller){
		$message = array(
					'member'=>array('_id'=>$product['seller']['_id']),
					'type'=>'assessmentSuccess',
					'title'=>'รายการสินทรัพย์ '.$product['title'].' ประเมินเรียบร้อยแล้ว',
					'create_date'=>date('Y-m-d H:i:s'),
					'readed'=>false,
					'message'=>'รายการสินทรัพย์ '.$product['title'].' ประเมินเรียบร้อยแล้ว',
					'data'=>array(
							'product'=>array('_id'=>$product['_id'],'title'=>$product['title'])
							)
					);
		$this->CI->mMessage
			->insert($message);

		
		$this->CI->email->to($seller['email']);
		$this->CI->email->subject('แจ้งผลการประเมินสินทรัพย์');

		$message = $this->CI->load->view('email/alertSellerApproveAssessment',$product,true);
		$this->CI->email->message(nl2br($message));
		try{
			$this->CI->email->send();
		}catch(Exception $e){
			
		}
	}
	//Seller ยืนยันข้อมูลการประเมิน
	public function sellerConfirmAssessment($product){

	}
	//Admin นำสินทรัพย์ขึ้นเว็บไซด์
	public function adminSubmitProduct($product){

	}
	//มีรายการ Bid มาใหม่
	public function newBiding($product,$seller){
		$investor = array_pop($product['investor']);
		$message = array(
					'member'=>array('_id'=>$product['seller']['_id']),
					'type'=>'newBiding',
					'title'=>'มีผู้เสนอราคาสินทรัพย์ '.$product['title'].' ',
					'create_date'=>date('Y-m-d H:i:s'),
					'readed'=>false,
					'message'=>'เสนอราคา '.$product['investor']['price'].',ดอกเบี้ย '.$product['investor']['interest'],
					'data'=>array(
							'product'=>array('_id'=>$product['_id'],'title'=>$product['title'])
							)
					);
		$this->CI->mMessage
			->insert($message);

		$this->CI->email->to($seller['email']);
		$this->CI->email->subject('มีการประเมินเข้ามาใหม่');

		$message = $this->CI->load->view('email/alertSellerNewBid',$product,true);
		$this->CI->email->message(nl2br($message));
		try{
			$this->CI->email->send();
		}catch(Exception $e){
			
		}
	}
	//การบิดสิ้นสุด สำเร็จ
	public function successBidding($product){

	}
	//หมดระยะเวลา Bid
	public function timeoutBidding($product){

	}
	
}
