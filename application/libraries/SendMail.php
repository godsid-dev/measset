<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
class SendMail
{
	
	private $CI;
	public $error;
	private $config;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('email',true);
		$this->CI->load->library('email');
		$this->config = $this->CI->config->item('email');
		
		$this->CI->email->initialize($this->config);
		$this->CI->email->from($this->config['from'],$this->config['from_name']);
	}

	public function verifyEmail($to,$data){
		$this->CI->email->to($to);
		$this->CI->email->subject('ตรวจสอบยืนยันที่อยู่อีเมลของคุณ');
		$message = $this->CI->load->view('email/verifyEmail',$data,true);
		$this->CI->email->message(nl2br($message));
		return $this->CI->email->send();
	}

	
}
