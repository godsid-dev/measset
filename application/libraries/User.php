<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
	class User
	{	
		private $public = array(
								'admin'=>1,
								'webmaster'=>2,
								'assessor'=>4
								);

		private $CI;
		private $encryption_key;
		private $COOKIE_KEY = 'memberId';
		private $COOKIE_LOGINED = 'logined';
		private $member;
		public function __construct()
		{
			$this->CI =& get_instance();
			$this->CI->load->model('member_model','mMember');
			$this->encryption_key = $this->CI->config->item('encryption_key');
		}

		public function login($email,$password){
			$this->member = $this->fetchUser($email);
			$hashPassword = $this->hashPassword($password);
			$loginSuccess = $this->checkPassword($this->member['password'],$this->member['salt'],$hashPassword);
			if($loginSuccess){
				$this->setCookie($this->member);
			}
			return $loginSuccess;
		}

		public function isLogin(){
			if($memberId = $this->CI->session->userdata($this->COOKIE_KEY)){
				$this->member = $this->CI->mMember
				->where(array('_id'=>$memberId))
				->pop(array('salt','password'))
				->find_one();
				$this->member['message'] = 0;
				$this->member['notification'] = 0;
				return $this->member;
			}elseif($rememberCode = get_cookie($this->COOKIE_LOGINED)){
				return $this->autoLogin();
			}
			return false;
		}

		public function acl($restrict){
			return 	(isset($this->member['acl']) && ( $this->member['acl'] & $restrict));
		}

		public function getUser(){
			
			return array();
		}

		public function logout(){
			$this->CI->session->unset_userdata($this->COOKIE_KEY);
			delete_cookie($this->COOKIE_LOGINED);
		}

		private function fetchUser($email){
			$this->member = $this->CI->mMember
				->where(array('email'=>$email))
				->find_one();
			return $this->member;
		}

		public function genSalt(){
			return random_string('alnum',10);
		}

		public function hashPassword($password){
			return md5($password.$this->encryption_key);
		}

		public function checkPassword($password,$salt,$hashPassword){
			$encryptionPassword = $this->getEncryptionPassword($salt,$hashPassword);
			return ( $password == $encryptionPassword );
		}

		public function getEncryptionPassword($salt,$hashPassword){
			return md5($hashPassword.$salt);
		}

		public function setCookie($member){
			$this->CI->session->set_userdata($this->COOKIE_KEY,$member['_id']);
		}

		public function isEmailVerify(){
			if(isset($this->member['emailConfirm'])){
				return $this->member['emailConfirm'];
			}
			return false;
		}

		public function setEmailVerify($code,$status = false){
			$member = $this->CI->mMember->where(array('emailConfirmCode'=>$code))->find_one();
			if(!$member){
				return false;
			}
			return $this->CI->mMember->where(array('emailConfirmCode'=>$code))
					->set('emailConfirm',$status)
					->update();
		}

		public function rememberPassword(){
			$rememberCode = md5($this->member['_id'].time());
			$this->CI->mMember
					->where(array('_id'=>$this->member['_id']))
					->set('rememberCode',$rememberCode)
					->update();
			set_cookie($this->COOKIE_LOGINED,$rememberCode,strtotime("+100 Year"));
		}

		public function autoLogin(){
			if($rememberCode = get_cookie($this->COOKIE_LOGINED)){
				$this->member = $this->CI->mMember
					->where(array('rememberCode'=>$rememberCode))
					->pop(array('salt','password'))
					->find_one();
				if($this->member){
					$this->member['message'] = 0;
					$this->member['notification'] = 0;
					$this->setCookie($this->member);	
				}
				return $this->member;	
			}			
		}

		public function changePassword($password){
			$salt = $this->genSalt();
			$hashPassword = $this->hashPassword($password);
			$encyptPassword = $this->getEncryptionPassword($salt,$hashPassword);
			
			return $this->CI->mMember
					->where(array('_id'=>$this->member['_id']))
					->set(array('password'=>$encyptPassword,'salt'=>$salt))
					->update();		
		}
	}

?>