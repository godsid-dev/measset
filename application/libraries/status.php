<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
	class Status
	{
		public function __construct(){
			$this->CI =& get_instance();
			$this->CI->load->model('product_model','mProduct');				
		}

		public function id($id){
			return new MongoId($id);
		}

		private function setStatus($status = null, $product_id = null){
			$product = $this->CI->mProduct
					->where(array('_id'=>$this->id($product_id)))
					->set(array('status'=>array('status'=>$status, 'updateDate'=>date("Y-m-d H:i:s"))))
					->update();
			return $product;
		}

		public function setApproved($product_id = null){
			$this->setStatus("Approved", $product_id);
		}

		public function setInvest($product_id = null){
			$this->setStatus("Invest", $product_id);
		}

		public function setSell($product_id = null){
			$this->setStatus("Sell", $product_id);
		}

		public function setClose($product_id = null){
			$this->setStatus("Close", $product_id);
		}

		public function setDelete($product_id = null){
			$this->setStatus("Delete", $product_id);
		}		
	}
?>