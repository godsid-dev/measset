<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	*  
	*/
	class Cost
	{
		var $type;
		var $area;
		var $areaThai;
		var $assessment = array();
		var $investerFee = 0.01;
		var $sellerFee = 0.03;
		var $transferFee = 0.02;
		var $stampDutyFee = 0.005;
		var $interestTax = 0;
		var $additionalSale = 0.01;
		public function __construct(){
				
		}
		

		public function getInterestTax($price,$interest,$period){
			return ($price*$interest*$period)/12;
		}
		public function getInterest($price,$interest,$period){
			return ($price/100*($interest/12)*$period);
		}
		//ค่าอากรณ์แสตมป์ (จากราคาประเมินหรือขายอย่างใดอย่างนึงที่สูง)
		public function getStampDutyFee($price){
			return ($price/$this->stampDutyFee)/100;
		}
		//ค่าโอนที่
		public function getTransferFee($price){
			return ($price/$this->transferFee)/100;
		}

		public function getInvesterFee($price){
			return ($price/$this->investerFee)/100;
		}

		public function getSellerFee($price){
			return ($price/$this->sellerFee)/100;
		}
		public function getAssessment($type,$area){
			$areaThai = toThaiMatric($area);
			$this->assessment[] = $this->company1($type,$areaThai);
		}

		public function getAdditionalSale($price){
			return ($price*$this->additionalSale);
		}


		public function getInvesterPrice($price,$interest,$period){
			return array(
					'interestPrice'=>$this->getInterest($price,$interest,$period),
					'additionalSale'=>$this->getAdditionalSale($price),
					//'additionalSale'=>$this->getAdditionalSale($price)

					);
		}

		private function company1($type,$area){

			switch ($this->type) {
				case 'ที่ดิน':
					if($this->areaThai['rai']<5){
						$price = 4500;
					}elseif($this->areaThai['rai']< 20){
						$price = 6000;
					}
					elseif($this->areaThai['rai']< 50){
						$price = 8000;
					}
					else{//if($this->areaThai['rai']< 99)
						$price = 10000;
					}
					break;
				case 'บ้านเดี่ยว':
				case 'ทาวน์เฮ้าส์':
				case 'ห้องชุด':
				case 'อาคารพาณิชย์':
					$preice = 4500;
					break;
			}
			return array(
					'name'=>'บริษัทที่1',
					'price'=>$price,
				);
		}
	}