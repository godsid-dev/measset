<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mongo_Model extends CI_Model{
	
	protected $collection;

	/**
     * If using MongoDB, this may contain the collection fields with
     * their default values. If set the model will check new documents
     * against this array.
     */
    protected $_fields = array();

	protected $primary_key = '_id';

	/**
     * Optionally skip the validation. Used in conjunction with
     * skip_validation() to skip data validation for any future calls.
     */
    protected $skip_validation = FALSE;

	/**
     * An array of validation rules. This needs to be the same format
     * as validation rules passed to the Form_validation library.
     */
    protected $validate = array();


	public function __construct(){
		parent::__construct();

		if(empty($this->collection)){
			$this->collection = str_replace('_model','s',get_class($this));	
		}
		
		if(!isset($this->mongo_db)){
			$this->load->library('mongo_db');	
		}
	}
	
	public function select($includes = array(), $excludes = array()){
		$this->mongo_db->select($includes,$excludes);
		return ($this);
	}

	public function where($wheres, $value = null){
		$this->mongo_db->where($wheres, $value = null);
		return ($this);
	}
	
	public function where_or($wheres = array()){
		$this->mongo_db->where_or($wheres);
		return ($this);
	}
	public function where_in($field = "", $in = array()){
		$this->mongo_db->where_in($field, $in);
		return ($this);	
	}
	public function where_in_all($field = "", $in = array()){
		$this->mongo_db->where_in_all($field, $in);
		return ($this);
	}
	public function where_not_in($field = "", $in = array()){
		$this->mongo_db->where_not_in($field, $in);
		return ($this);
	}
	public function where_gt($field = "", $x){
		$this->mongo_db->where_gt($field, $x);
		return ($this);
	}
	public function where_gte($field = "", $x){
		$this->mongo_db->where_gte($field, $x);
		return ($this);
	}
	public function where_lt($field = "", $x){
		$this->mongo_db->where_lt($field, $x);
		return ($this);
	}
	public function where_lte($field = "", $x){
		$this->mongo_db->where_lte($field, $in);
		return ($this);
	}
	public function where_between($field = "", $x, $y){
		$this->mongo_db->where_between($field, $x,$y);
		return ($this);
	}
	public function where_between_ne($field = "", $x, $y){
		$this->mongo_db->where_between_ne($field, $x,$y);
		return ($this);	
	}
	public function where_ne($field = '', $x){
		$this->mongo_db->where_ne($field, $x);
		return ($this);
	}
	public function like($field = "", $value = "", $flags = "i", $enable_start_wildcard = TRUE, $enable_end_wildcard = TRUE){
		$this->mongo_db->like($field , $value , $flags , $enable_start_wildcard , $enable_end_wildcard );
		return ($this);
	}
	
	public function insert($insert = array()){
		return $this->mongo_db->insert($this->collection,$insert);
	}

	public function batch_insert($insert = array()){
		return $this->mongo_db->batch_insert($this->collection,$insert);
	}

	public function get(){
		return $this->mongo_db->get($this->collection);	
	}

	public function get_where($where = array()){
		return $this->mongo_db->get_where($this->collection,$where);
	}

	public function find_one(){
		return $this->mongo_db->find_one($this->collection);
	}

	public function count(){
		return $this->mongo_db->count($this->collection);	
	}

	public function set($fields, $value = NULL){
		$this->mongo_db->set($fields, $value);
		return ($this);
	}
	public function unset_field($fields){
		$this->mongo_db->unset_field($fields);
		return ($this);
	}
	public function addtoset($fields, $values){
		$this->mongo_db->addtoset($fields,$values);
		return ($this);
	}
	public function push($fields, $value = array()){
		$this->mongo_db->push($fields, $value);
		return ($this);
	}
	public function pop($fields){
		$this->mongo_db->pop($fields);
		return ($this);
	}
	public function pull($field = "", $value = array()){
		$this->mongo_db->pull($fields,$value);
		return ($this);
	}
	public function rename_field($old, $new){
		$this->mongo_db->rename_field($old, $new);
		return ($this);
	}
	public function inc($fields = array(), $value = 0){
		$this->mongo_db->inc($fields,$value);
		return ($this);
	}
	public function mul($fields = array(), $value = 0){
		$this->mongo_db->mul($fields,$value);
		return ($this);
	}
	public function max($fields = array(), $value = 0){
		$this->mongo_db->max($fields,$value);
		return ($this);
	}
	public function min($fields = array(), $value = 0){
		$this->mongo_db->min($fields,$value);
		return ($this);
	}
	

	public function distinct($field=""){
		return $this->mongo_db->distinct($this->collection, $field);
	}

	public function update($options = array()){
		return $this->mongo_db->update($this->collection, $options);
	}

	public function update_all($data = array(), $options = array()){
		return $this->mongo_db->update_all($this->collection,$data, $options);
	}

	public function delete(){
		return $this->mongo_db->delete($this->collection);
	}

	public function delete_all(){
		return $this->mongo_db->delete_all($this->collection);
	}

	public function aggregate($operation){
		return $this->mongo_db->aggregate($this->collection, $operation);
	}

	public function order_by($fields = array()){
		$this->mongo_db->order_by($fields);
		return ($this);
	}
	public function date($stamp = FALSE){
		$this->mongo_db->date($stamp);
		return ($this);
	}
	public function limit($x = 99999){
		$this->mongo_db->limit($x);
		return ($this);
	}

	public function offset($x = 0){
		$this->mongo_db->offset($x);
		return ($this);
	}

	public function id($id){
		return new MongoId($id);
	}

	/**
     * Run validation on the passed data
     */
    private function _run_validation($data)
    {
        if($this->skip_validation)
        {
            return TRUE;
        }
        if(!empty($this->validate))
        {
            foreach($data as $key => $val)
            {
                $_POST[$key] = $val;
            }
            $this->load->library('form_validation');
            if(is_array($this->validate))
            {
                $this->form_validation->set_rules($this->validate);
                return $this->form_validation->run();
            }
            else
            {
                return $this->form_validation->run($this->validate);
            }
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * Prepares a document for MongoDB insert/update operation
     * using the pre-filled $_fields schema dictionary.
     *
     * This will save us from Null-byte and SQL-like injection attacks,
     * Also ensures that we got default values of unset fields in the
     * passing document.
     *
     * Since MongoDB is a schema-less database, we better do this on the
     * application side!
     */
    public function _prep_fields(&$fields, $update = FALSE)
    {
        if ( ! $this->mongo_db OR empty($this->_fields))
        {
            return $fields;
        }
        // Remove extra fields from the passing document
        foreach ($fields as $key => $value)
        {
            // Null-byte injection?
            if (!isset($this->_fields[$key]))
            {
                unset($fields[$key]);
            }
            // SQL-like injection?
            // else
            // {
            //     $fields[$key] = (string) $value;
            // }
        }
        // Ensure default values
        $update OR $fields = array_merge($this->_fields, $fields);
        return ! empty($fields);
    }

}
