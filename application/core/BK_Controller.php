<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BK_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->setScript();
	}

	protected function setScript(){

		$this->load->css('assets/themes/engines/bower_components/bootstrap/dist/css/bootstrap.min.css');
		$this->load->css('assets/themes/engines/bower_components/metisMenu/dist/metisMenu.min.css');
		$this->load->css('assets/themes/engines/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');
		$this->load->css('assets/themes/engines/bower_components/datatables-responsive/css/dataTables.responsive.css');
		$this->load->css('assets/themes/engines/dist/css/timeline.css');
		$this->load->css('assets/themes/engines/dist/css/sb-admin-2.css');
		//$this->load->css('assets/themes/engines/bower_components/morrisjs/morris.css');
		$this->load->css('assets/themes/engines/bower_components/font-awesome/css/font-awesome.min.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox-buttons.css');
		

		$this->load->js('assets/themes/engines/bower_components/jquery/dist/jquery.min.js');
		$this->load->js('assets/themes/engines/bower_components/bootstrap/dist/js/bootstrap.min.js');
		$this->load->js('assets/themes/engines/bower_components/metisMenu/dist/metisMenu.min.js');
		$this->load->js('assets/themes/engines/bower_components/raphael/raphael-min.js');
		//$this->load->js('assets/themes/engines/bower_components/morrisjs/morris.min.js');
		//$this->load->js('assets/themes/engines/js/morris-data.js');
		$this->load->js('assets/themes/engines/dist/js/sb-admin-2.js');
		$this->load->js('assets/themes/default/js/source/jquery.fancybox.js');
		

	}

	
	
}