<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $page = 1;
	var $limit = 20;
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		if($this->input->get('page')){
			$this->page = $this->input->get('page');
		}
	}

	protected function getOffset(){
		return ((($this->page-1)*$this->limit));
	}

	protected function getPageing($count,$url){

		return array(
				'page'=>$this->page,
				'item'=>$count,
				'limit'=>$this->limit,
				'url'=>$url
				);
	}

	public function render_home($profile=null)
	{
		$this->output->set_template('default');
		if(is_array($profile)){
			$nav['profile'] = $profile;
			$this->load->section('nav_profile', 'web/header/profile',$nav);
		}

		$this->load->section('navigation', 'web/header/navigation_home');
		$this->load->section('header', 'web/header/header');
		
		$this->load->css('assets/themes/default/css/ui-v4.1.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox-buttons.css');
		$this->load->css('assets/themes/default/css/flexslider.css');
		$this->load->css('assets/themes/default/fonts/fontface.css');
		$this->load->css('assets/themes/default/css/theme.css');

		$this->load->js('assets/themes/default/js/jquery-1.10.2.js');
		$this->load->js('assets/themes/default/js/jquery.tools.min.js');
		$this->load->js('assets/themes/default/js/jquery.flexslider-min.js');
		$this->load->js('assets/themes/default/js/source/jquery.fancybox.js');
		$this->load->js('assets/themes/default/js/source/helpers/jquery.fancybox-buttons.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-transition.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js');
		$this->load->js('assets/themes/default/js/theme.js');		

		$this->load->section('footer', 'web/footer');
	}

	public function render_page($profile=null)
	{

		$this->output->set_template('simple');

		if(is_array($profile)){
			$nav['profile'] = $profile;
			$this->load->section('nav_profile', 'web/header/profile',$nav);
		}
		$this->load->section('navigation', 'web/header/navigation');
		$this->load->section('header', 'web/header/header');
		
		$this->load->css('assets/themes/default/css/ui-v4.1.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox.css');
		$this->load->css('assets/themes/default/css/jquery.fancybox-buttons.css');
		$this->load->css('assets/themes/default/css/flexslider.css');
		$this->load->css('assets/themes/default/fonts/fontface.css');
		$this->load->css('assets/themes/default/css/theme.css');
		$this->load->css('assets/themes/default/css/royalslider.css');
		$this->load->css('assets/themes/default/css/rs-universal.css');
		$this->load->css('assets/themes/default/css/bootstrap-datepicker.min.css');

		$this->load->js('assets/themes/default/js/jquery-1.10.2.js');
		$this->load->js('assets/themes/default/js/jquery.tools.min.js');

		$this->load->js('assets/themes/default/hero_files/bootstrap-transition.js');
		$this->load->js('assets/themes/default/hero_files/bootstrap-collapse.js');
		$this->load->js('assets/themes/default/js/jquery.royalslider.min.js');
		$this->load->js('assets/themes/default/js/bootstrap-datepicker.min.js');
		$this->load->js('assets/themes/default/js/locales/bootstrap-datepicker.th.js');
		$this->load->js('assets/themes/default/js/jquery.flexslider-min.js');
		$this->load->js('assets/themes/default/js/source/jquery.fancybox.js');
		$this->load->js('assets/themes/default/js/source/helpers/jquery.fancybox-buttons.js');
		

		$this->load->section('footer', 'web/footer');
	}

}