<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Valuation_model extends Mongo_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function getValuation($product=null)
	{
		if(is_null($product)){
			return false;
		}

		return parent::get_where(array('product'=>$product));

	}

	public function setValuation($acction="insert", $data=null, $ID=null)
	{

		$data['product']=$this->id($data['product']);
		switch ($acction) {
			case 'insert':
				# code...
				break;
			case 'update':
				parent::where(array('product'=>$this->id($ID)))->set($data)->update(array('upsert'=>TRUE));
				return true;
				break;
			
			default:
				# code...
				break;
		}

	}

}