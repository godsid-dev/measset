<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends Mongo_Model
{
	private $_table='Products';
	public function __construct(){
		parent::__construct();
	}

	public function getInvestList(){
		//$query = $this->where('estimates' != "")->get();
		$query = $this->where(array(
							 'status'=>'กำลังขาย'
							 //'investor'=>array('$exists'=>false)
							 ))
					  ->get();
		return($query);
	}

	public function getDetailInvest($id){

		return $this->where(
							array('_id'=>$this->id($id)))
					->get();
		//$query = $this->select()->where(array('_id'=>$this->id($id)))->get();
		//return($query);
	}

	public function id($id){
		return new MongoId($id);
	}

	public function getAll($page=1,$limit=10)
	{
		$offset=($page*$limit)-$limit;
		$products = $this->where(array('seller'=>array('$exists'=>true)))
						->limit($limit)
						->offset($offset)
						->get();
		$count = $this->where(array('seller'=>array('$exists'=>true)))->count();
		return array(
			'result'=>$products,
			'totalrecord'=>$count,
			'totalpage'=>ceil($count/$limit),
			'currentpage'=>$page,
			'limitperpage'=>$limit,
		);
	}

	public function getProduct($ID=null)
	{
		if(is_null($ID)){
			return;
		}
		return parent::get_where(array('_id'=>$this->id($ID)));
	}



	public function getInvList($page=1,$limit=10)
	{
		$page=$page<=0?1:$page;
		$offset=($page*$limit)-$limit;

		$tmpdata=array(
			'result'=>parent::limit($limit)->offset($offset)->where(array('investor'=>array('$exists'=>true)))->get(),
			'totalrecord'=>parent::where(array('investor'=>array('$exists'=>true)))->count(),
			'totalpage'=>ceil(parent::where(array('investor'=>array('$exists'=>true)))->count()/$limit),
			'currentpage'=>$page,
			'limitperpage'=>$limit,
		);

		return $tmpdata;
	}


	public function setProduct($acction="insert", $data=null, $ID=null)
	{
		switch ($acction) {
			case 'insert':
				# code...
				break;
			case 'update':
				parent::where(array('_id'=>$this->id($ID)))->set($data)->update();
				break;
			
			default:
				# code...
				break;
		}
	}

	public function updateInvestor($product_id,$investor, $price,$interest)
	{	
		$data = array_merge($investor,array(
						'price'=>$price,
						'interest'=>$interest,
						'create_date'=>date('Y-m-d H:i:s')
					));
		return $this->where(array('_id'=>$this->id($product_id)))
					//->set('status','สำเร็จ')
					->addtoset('investor',array($data))
					->update();
	}
}