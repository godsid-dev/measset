<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends Mongo_Model
{
	public $_fields = array(
							'email'=>'',
							'salt'=>'',
							'password'=>'',
							'emailConfirm'=>false,
							'emailConfirmCode'=>'',
							'createDate'=>'',
							'updateDate'=>'',
							'default'=>array(
										'individual'=>array(
												'memberType'=>'',
												'title'=>'',
												'status'=>'',
												'firstname'=>'',
												'lastname'=>'',
												'birth_date'=>'',
												'gender'=>'',
												'id_card'=>'',
												'id_card_issue'=>'',
												'address'=>'',
												'province'=>'',
												'district'=>'',
												'tambon'=>'',
												'postcode'=>'',
												'id_card_image'=>'',
												'id_card_issue'=>'',
												'home_document_image'=>'',
												'occupation'=>'',
												'telephone'=>'',
											),
										'corporation'=>array(
												'memberType'=>'',
												'objective'=>'NO',
												'company_name'=>'',
												'registration_tax'=>'',
												'business_type'=>'',
												'contact_name'=>'',
												'contact_phone'=>'',
												'corporation_image'=>''
											),
									),
							'investor'=>array(
										'individual'=> array(
													'memberType'=>'',
													'title'=>'',
													'status'=>'',
													'firstname'=>'',
													'lastname'=>'',
													'birth_date'=>'',
													'gender'=>'',
													'id_card'=>'',
													'id_card_issue'=>'',
													'address'=>'',
													'province'=>'',
													'district'=>'',
													'tambon'=>'',
													'postcode'=>'',
													'id_card_image'=>'',
													'id_card_issue'=>'',
													'home_document_image'=>'',
													'occupation'=>'',
													'telephone'=>''
													),
										'corporation'=> array(
													'memberType'=>'',
													'objective'=>'NO',
													'company_name'=>'',
													'registration_tax'=>'',
													'business_type'=>'',
													'contact_name'=>'',
													'contact_phone'=>'',
													'corporation_image'=>''
													),
										'interested'=>[],
										'budget'=>'0',
										'contract'=>'12'
										),
							'seller'=>array(
											'individual' => array(),
											'corporation' => array(),
										)
						);
	

	public function __construct(){
		parent::__construct();
	}

	public function getAll($page=1,$limit=10)
	{
		$page=$page<=0?1:$page;
		$offset=($page*$limit)-$limit;

		$tmpdata=array(
			'result'=>parent::limit($limit)->offset($offset)->get(),
			'totalrecord'=>parent::count(),
			'totalpage'=>ceil(parent::count()/$limit),
			'currentpage'=>$page,
			'limitperpage'=>$limit,
		);

		return $tmpdata;
	}

	public function getwhere($id=null)
	{
		
	}

	public function getInvestor($page=1,$limit=10)
	{
		$offset=($page*$limit)-$limit;

		$investors = $this->where(array(
								'investor'=>array('$exists'=>true)
								))
					->limit($limit)
					->offset($offset)
					->get();
		$count = parent::where(array('seller'=>array('$exists'=>true)))->count();
		return array(
			'result'=>$investors,
			'totalrecord'=>$count,
			'totalpage'=>ceil($count/$limit),
			'currentpage'=>$page,
			'limitperpage'=>$limit,
		);
	}

	public function setMember($acction="insert", $data=null, $ID=null)
	{
		switch ($acction) {
			case 'insert':
				# code...
				break;
			case 'update':
				parent::where(array('_id'=>$this->id($ID)))->set($data)->update();
				break;
			
			default:
				# code...
				break;
		}
	}

}
