<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->_init();
	}

	private function _init()
	{
		$this->render_page($this->user->isLogin());
		
	}

	public function index()
	{
		$this->question();
		$this->load->view('web/question/index');
	}

	private function question()
	{
		$this->load->section('question', 'web/question/question');
	}	
}