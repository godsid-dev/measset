<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends BK_Controller {

	var $loginMember;
	var $restrict;//LEVEL_WEBMASTER | LEVEL_ADMIN | LEVEL_WEBMASTER | LEVEL_ASSESSOR;
	function __construct()
	{
		parent::__construct();
		$this->restrict = LEVEL_ADMIN | LEVEL_WEBMASTER | LEVEL_ASSESSOR;
		$this->_init();

	}

	private function _init()
	{
		$this->load->library('user');
	}

	public function index(){
		$data["title"] = "เข้าสู่ระบบ";
		$this->user->logout();
		redirect(engines_url('/login'));
	}

}