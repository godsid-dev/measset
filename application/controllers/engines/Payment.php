<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends BK_Controller {

	var $loginMember;
	var $limit = 20;
	var $page  = 1;

	function __construct()
	{
		parent::__construct();
		$this->_init();
		if($page = $this->input->get('page')){
			$this->page = $page;
		}
	}
	private function _init(){
		$this->load->library('paging');
		$this->load->library('user');

		$this->load->model('member_model','mMember');
		$this->load->model('payment_model','mPayment');
		
		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		//$this->output->set_meta('description','Lorem Ipsum');
		//$this->output->set_meta('author','Lorem Ipsum');		
		$this->output->set_template('engines');
		$this->loginMember = $this->user->isLogin();
	}

	public function index(){
		$payments = $this->mPayment
						->where(array(
								//'checked'=>false
							))
						->order_by(array('create_date'=>'DESC'))
						->get();
		$data=array(
			'title'=>'Confirm Payment',
			'payments'=>$payments,
			'paging'=>''//$this->paging->myRender(),
		);

		$this->load->view('engines/layout/payment',$data);
	}

	public function approve($id){
		if($id){
			$payment = $this->mPayment
							->where(array('_id'=>$this->mPayment->id($id)))
							->find_one();

			$this->mPayment
					->where(array(
						'_id'=>$this->mPayment->id($id)
						))
					->set(array('checked'=>true,
								'approve_date'=>date('Y-m-d H:i:s'),
								'approve_by'=>$this->loginMember['_id']
						))
					->update();
			$this->mMember
					->where(array('_id'=>$this->mMember->id($payment['investor']['_id'])))
					->set(array('investor.canbid'=>true))
					->update();
		}
		redirect(engines_url('payment'));
	}

}