<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invester extends BK_Controller {

	var $loginMember;
	var $limit = 20;
	var $page  = 1;
	function __construct()
	{
		parent::__construct();
		$this->_init();
		if($page = $this->input->get('page')){
			$this->page = $page;
		}
	}

	private function _init()
	{
		$this->load->library('paging');
		$this->load->library('user');

		$this->load->model('member_model','mMember');
		
		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		//$this->output->set_meta('description','Lorem Ipsum');
		//$this->output->set_meta('author','Lorem Ipsum');

		$this->output->set_template('engines');
		$this->loginMember = $this->user->isLogin();
	}

	//dashboard
	public function index()
	{
		
		$investors = $this->mMember->getInvestor($this->page,$this->limit);

		$this->paging->set($investors['totalrecord'],$this->limit,5,$this->page);

		$data=array(
			'title'=>'Investor List',
			'investors'=>$investors['result'],
			'paging'=>$this->paging->myRender(),
		);

		$this->load->view('engines/layout/invest_list',$data);
	}

	public function invdetail($member=null)
	{

	}

	public function approve($member=null)
	{
		//var_dump($approve=($this->input->post('approve')));
		//die();	
		if(!is_null($member)){
			$approve=($this->input->post('approve'));
			$this->mMember->setMember('update',array('investor.status'=>$approve),$member);
		}

		redirect(site_url('engines/invester'), 'auto', 301);
	}
}