<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consignment extends BK_Controller {

	var $page = 1;
	var $limit = 20;
	function __construct()
	{
		parent::__construct();
		$this->_init();
		if($page = $this->input->get('p')){
			$this->page = $page;
		}
	}

	private function _init()
	{
		$this->load->library('paging');
		$this->load->library('notification');

		$this->load->model('product_model','mProduct');
		$this->load->model('member_model','mMember');
		$this->load->model('valuation_model','mValuation');
		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->output->set_meta('description','Lorem Ipsum');
		$this->output->set_meta('author','Lorem Ipsum');

		$this->output->set_template('engines');
	}

	//dashboard
	public function index($id="")
	{
		if($id){
			return $this->detail($id);
		}
		$products = $this->mProduct
						->order_by(array('create_date','DESC'))
						->getAll($this->page,$this->limit);

		$this->paging->set($products['totalrecord'],$this->limit,5,$this->page);

		$data=array(
			'title'=>'ข้อมูลรายการขายฝาก',
			'products'=>$products['result'],
			'paging'=>$this->paging->myRender(),
		);

		$this->load->view('engines/layout/cons_inv_list',$data);
	}

	public function datalist()
	{	
		$page=$this->input->get('p')<=1?1:$this->input->get('p');
		$limit = 20;
		$tmpResult=$this->mProduct->getAll($page,$limit);
		$this->paging->set($tmpResult['totalrecord'],$limit,5,$page);

		$data=array(
			'title'=>'Product List',
			'result'=>$tmpResult['result'],
			'paging'=>$this->paging->myRender(),
		);

		$this->load->view('engines/layout/cons_list',$data);
	}

	public function invlist()
	{
		
	}

	public function detail($id=null)
	{
		$product = $this->mProduct
						->where(array('_id'=>$this->mProduct->id($id)))
						->find_one();
		$member = $this->mMember
						->where(array('_id'=>$product['seller']['_id']))
						->find_one();
		$valuations = $this->mValuation->getValuation($product);
		$member['seller']['email'] = $member['email'];
		$data=array(
			'title'=>'Product Detail',
			'product'=>$product,
			'seller'=>$member['seller'],
			'valuation'=>$valuations,
		);
		$this->load->section('google_map_js', 'web/googleMapJs',array('lat'=>$product['location'][0],'lng'=>$product['location'][1]));
		$this->load->section('box_googlemap','engines/component/box_googlemap',array('height'=>300,"width"=>"100%"));
		$this->load->view('engines/layout/cons_detail',$data);

	}

	public function assessor($status,$id){
		if(!is_null($id)){
			$this->mProduct
					->setProduct('update',array(
						'status'=>'กำลังขาย')
					,$id);
			$product = $this->mProduct
							->where(array(
										'_id'=>$this->mProduct->id($id)))
							->find_one();
			$seller = $this->mMember
							->where(array('_id'=>$this->mMember->id($product['seller']['_id'])))
							->find_one();
			$this->notification->adminConfirmAssessment($product,$seller);

		}
	}
	public function approve($product_id=null){
		if(!is_null($product_id)){
			$approve=($this->input->post('approve'));
			$bidtype = $this->input->post('bidtype');
			$this->mProduct
					->setProduct('update',array(
						'approve'=>$approve,
						'status'=>'รอการประเมิน',
						'bidtype'=>$bidtype)
					,$product_id);

			if($approve){
				$product = $this->mProduct
								->where(array(
											'_id'=>$this->mProduct->id($product_id)))
								->find_one();
				$this->notification->adminConfirmProduct($product);
			}
		}

		redirect(site_url('engines/consignment/'.$product_id), 'auto', 301);

	}

	public function estimates($product=null)
	{

		if(!is_null($product)){

			$market_price=str_replace(',', '', $this->input->post("market_price"));
			$force_sale_price=str_replace(',', '', $this->input->post("force_sale_price"));

			if(!is_numeric($market_price) || !is_numeric($force_sale_price)){
				echo "ข้อมูลไม่ถูกต้อง";
				exit();
			}
			//die();
			$data=array(
				'estimates'=>array(
						'market_price'=>$market_price,
						'force_sale_price'=>$force_sale_price,
						'create_date'=>date('Y-m-d')
					),
				'land_office'=>$this->input->post('land_office')
			);
			$this->mProduct->setProduct('update',$data,$product);
		}

		redirect(site_url('engines/consignment/datadetail/'.$product), 'auto', 301);
	} 

}
