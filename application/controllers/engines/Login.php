<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends BK_Controller {

	var $loginMember;
	var $restrict;//LEVEL_WEBMASTER | LEVEL_ADMIN | LEVEL_WEBMASTER | LEVEL_ASSESSOR;
	function __construct()
	{
		parent::__construct();
		$this->restrict = LEVEL_ADMIN | LEVEL_WEBMASTER | LEVEL_ASSESSOR;
		$this->_init();

	}

	private function _init()
	{
		$this->load->model('member_model','mMember');
		$this->load->library('user');
		$this->load->section('sidebar', 'engines/sidebar');
		//$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->loginMember = $this->user->isLogin();
		if($this->loginMember && $this->user->acl($this->restrict)){
			redirect();
		}
		if($this->loginMember && $this->user->acl(LEVEL_ADMIN|LEVEL_WEBMASTER)){
			redirect(engines_url('/home'));
		}else if($this->loginMember && $this->user->acl(LEVEL_ASSESSOR)){
			redirect(engines_url('/assessor'));
		}else if($this->loginMember){
			redirect('/');
		}
		$this->output->set_template('blank');
	}

	public function index(){
		$data["title"] = "เข้าสู่ระบบ";
		$this->load->view('engines/login',$data);
	}

	public function submit(){
		$data["title"] = "เข้าสู่ระบบ";
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$this->mMember->find_one(array());
		
		if($this->user->login($email,$password)){
			redirect(engines_url('/home'));
		}

		$data['alert'] = array('message'=>'Missing Password','type'=>'danger');	
		$this->load->view('engines/login',$data);
	}


}