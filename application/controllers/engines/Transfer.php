<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends BK_Controller {
	var $loginMember;
	var $page = 1;
	var $limit = 20;
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	private function _init()
	{
		$this->load->library('user');
		$this->load->model('product_model','mProduct');

		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->output->set_meta('description','Lorem Ipsum');
		$this->output->set_meta('author','Lorem Ipsum');

		$this->output->set_template('engines');

		$this->page = $this->input->get('page');
		if(!$this->page){
			$this->page = 1;
		}

		$this->loginMember = $this->user->isLogin();
		
		if(!$this->loginMember){
			redirect(engines_url('/login'));
		}elseif($this->user->acl(LEVEL_ADMIN | LEVEL_ASSESSOR)){
			//redirect(engines_url('/home'));
		}
	}

	
	public function index(){
		$data['title'] = "ข้อมูลสินทรัพย์";
		$data['products'] = $this->mProduct->getAll($this->page,$this->limit);
		$this->load->section('table_product','engines/layout/assessor',$data);
		$this->load->view('engines/layout/assessor',$data);
	}
}