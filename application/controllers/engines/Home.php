<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends BK_Controller {
	var $loginMember;
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	private function _init()
	{
		$this->load->library('user');

		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->output->set_meta('description','Lorem Ipsum');
		$this->output->set_meta('author','Lorem Ipsum');

		$this->output->set_template('engines');

		$this->loginMember = $this->user->isLogin();
		if(!$this->loginMember){
			redirect(engines_url('/login'));
		}
	}

	//dashboard
	public function index()
	{
		if(!$this->user->acl(LEVEL_ADMIN | LEVEL_WEBMASTER | LEVEL_ASSESSOR)){
			redirect('/');
		}
	}
}
