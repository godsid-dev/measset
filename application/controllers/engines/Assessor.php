<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assessor extends BK_Controller {
	var $loginMember;
	var $page = 1;
	var $limit = 20;
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	private function _init()
	{
		$this->load->library('user');
		$this->load->library('paging');
		$this->load->library('notification');
		$this->load->model('product_model','mProduct');
		$this->load->model('valuation_model','mValuation');

		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		
		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->output->set_template('engines');

		$this->page = $this->input->get('page');
		if(!$this->page){
			$this->page = 1;
		}

		$this->loginMember = $this->user->isLogin();
		
		if(!$this->loginMember){
			redirect(engines_url('/login'));
		}elseif($this->user->acl(LEVEL_ADMIN | LEVEL_ASSESSOR)){
			//redirect(engines_url('/home'));
		}
	}

	
	public function index($id=""){
		if($id){
			 $this->detail($id);
		}

		$data['title'] = "ข้อมูลสินทรัพย์";
		$products = $this->mProduct
									->where_in('status',array('รอการประเมิน','กำลังประเมิน'))
									->order_by(array('create_date'=>'desc'))
									->getAll($this->page,$this->limit);
		$data['products'] = $products['result'];

		$this->paging
			->linkto(engines_url('/assessor'))
			->set($products['totalrecord'],$this->limit,5,$this->page);

		$data['paging'] = $this->paging->myRender();
		$this->load->section('table_product','engines/component/table_product',$data);
		$this->load->view('engines/layout/assessor',$data);
	}

	public function detail($id){
		$data['title'] = "ข้อมูลสินทรัพย์";
		$product = $this->mProduct
						->where(array('_id'=>$this->mProduct->id($id)))
						//->where_in('status',array('รอการประเมิน','กำลังประเมิน'))
						->find_one();

		$data['valuation'] = $this->mValuation
							->where(array('product_id'=>$this->mValuation->id($product['_id'])))
							->find_one();
		
	

		$this->load->section('box_seller','engines/component/box_seller',$product);
		$this->load->section('box_product','engines/component/box_product',$product);
		$this->load->section('box_product','engines/component/box_valuation',$data);
		$this->load->view('engines/layout/assessor_detail',$data);
	}

	public function edit($id){
		$data['title'] = "รายงานการประเมินราคาทรัพย์สิน";
		$product = $this->mProduct
									->where(array('_id'=>$this->mProduct->id($id)))
									->where_in('status',array('รอการประเมิน','กำลังประเมิน'))
									->find_one();

		$this->load->section('box_valuation','engines/component/box_valuation',array('_id'=>$id,'product'=>$product));
		$this->load->view('engines/layout/assessor_edit',$data);
	}

	public function submit($id){
		$data = $this->input->post();
		/*$valuation = array(
				    "product" => $this->mProduct->id($id),
				    "type" => $data['type'],
				    "address" => $data['address'],
				    "document_type" => $data['document_type'],
				    "obligation" => $data['obligation'],
				    "method" => $data['method'],
				    "marget_price" => $data['marget_price'],
				    "forcesale_price" => $data['forcesale_price'],
				    "department_price" => $data['department_price'],
				    "create_date" => $data['create_date'],
				    "holder" => $data['holder'],
				    "obligation" => $data['obligation'],
				    "location" => array($data['lat'],$data['lon']), 
				    "document" => $data['document'],
				    "sheet" => $data['title_deed_number'],
				    "parcel_no" =>$data['title_deed_number'],
				    "title_deed_number"=>$data['title_deed_number']
		    	);
		 */
		 $valuation = $data;
		 $valuation['product_id'] = $this->mProduct->id($id);
		if($data['type'] == 'คอนโดมิเนียม/อพาร์ทเม้นท์'){
			$valuation["specified"] = array('meter'=>$data['meter']);
		}else{
			$valuation["specified"] = array(
										"rai"=>$data['rai'],
							    		"ngan"=>$data['ngan'],
							    		"wah"=>$data['wah']
							    		);
		}
		$valuation['create_date'] = date('Y-m-d H-i-s');

		$valuation_id = $this->mValuation
			->insert($valuation);
		//Update product 
		if($valuation_id){
			$this->mProduct
				->where(array('_id'=>$this->mProduct->id($id)))
				->set(array(
							'location'=>array($data['location_lat'],$data['location_lon']),
							'specified'=>$valuation["specified"],
							'valuation'=>array(
												"_id"=>$valuation_id,
												"price"=>$data['cost_estimate']
												),
							'status'=>'รอตรวจสอบการประเมิน'
							))						
				->update();
		}

		$product = $this->mProduct
				->where(array('_id'=>$this->mProduct->id($id)))
				->find_one();

		$this->notification->assessmentSuccess($product);

		//redirect(engines_url('/assessor/'.$id));
	}

}