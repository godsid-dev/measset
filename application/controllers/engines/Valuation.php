<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Valuation extends BK_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	private $page=array();

	private function _init()
	{
		$this->load->library('paging');
		$this->load->library('File');
		$this->load->model('product_model','mProduct');
		$this->load->model('member_model','mMember');
		$this->load->model('valuation_model','mValuation');
		$this->load->section('sidebar', 'engines/sidebar');
		$this->load->section('header', 'engines/header');
		$this->load->js(assets_url('themes/engines/js/valuation.js'));


		$this->output->set_meta('viewport','width=device-width, initial-scale=1');
		$this->output->set_meta('description','Lorem Ipsum');
		$this->output->set_meta('author','Lorem Ipsum');

		$this->output->set_template('engines');
	}
	public function index(){

	}

	public function saveform()
	{
		$formdata=$this->input->post();

		//var_dump($this->mValuation->id($formdata['valuation_product']));
		//die();

		$saveData=array(
			'product'=>$formdata['valuation_product'],
			'valuation_type'=>$formdata['valuation_type'],
			'valuation_address'=>$formdata['valuation_address'],
			'valuation_doc'=>$formdata['valuation_doc'],
			'valuation_obligation'=>$formdata['valuation_obligation'],
			'valuation_method'=>$formdata['valuation_method'],
			'valuation_value'=>$formdata['valuation_value'],
			'valuation_force_value'=>$formdata['valuation_force'],
			'valuation_niti'=>$formdata['valuation_niti'],
			'valuation_date'=>date('Y-m-d'),
			'hold'=>$formdata['hold'],
			'obligation'=>$formdata['obligation'],
			'lat'=>$formdata['lat'],
			'lon'=>$formdata['lon'],
		);

		if($_FILES['valuation_file']['size']>0){
			$saveData['valuation_file'] = $this->file->uploadFileTemp('valuation_file');
		}

		$saveData['product']=$formdata['valuation_product'];
		if(isset($formdata['deedid'])){
			foreach ($formdata['deedid'] as $dkey => $dvalue){

				if(!empty($formdata['deedid'][$dkey]) || !empty($formdata['landid'][$dkey]) ||!empty($formdata['survey'][$dkey]) || !empty($formdata['acres'][$dkey]) || !empty($formdata['quarte'][$dkey]) || !empty($formdata['varh'][$dkey])){
					$saveData['landData'][$dkey]=array(
						'deedid'=>$formdata['deedid'][$dkey],
						'landid'=>$formdata['landid'][$dkey],
						'survey'=>$formdata['survey'][$dkey],
						'acres'=>$formdata['acres'][$dkey],
						'quarte'=>$formdata['quarte'][$dkey],
						'varh'=>$formdata['varh'][$dkey],
					);
				}
			}
		}

		if($this->mValuation->setValuation('update',$saveData,$formdata['valuation_product'])){
			$this->mProduct->setProduct('update',array('status'=>'ประเมินราคาแล้ว'),$formdata['valuation_product']);
			redirect(site_url('engines/valuation/defaults/'.$formdata['valuation_product']), 'location');
		}

	}

	public function defaults($product=null)
	{

		$tmpResult=$this->mValuation->getValuation($this->mValuation->id($product));
		$tmpSeller=''; //$this->mMember->get_where(array('_id'=>$tmpResult[0]['seller']));

		$data=array(
			'title'=>'แบบประเมินราคา',
			'product'=>$product,
			'result'=>isset($tmpResult[0])?$tmpResult[0]:'',
			'seller'=>isset($tmpSeller[0])?$tmpSeller[0]:'',
		);

		$this->load->view('engines/layout/Valua_default',$data);

	}

	public function details($product=null)
	{
		//echo"asdf";
	}


}