<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {

	var $loginMember;
	function __construct()
	{

		parent::__construct();

		$this->render_page();
		$this->load->library('User');
		$this->load->library('SendMail');
		$this->loginMember = $this->user->isLogin();
		$this->render_page($this->loginMember);
	}

	public function index()
	{
		if($this->user->isLogin()){
			if($this->user->isEmailVerify()){
				redirect("/home");
			}else{
				redirect("/verifyEmail");
			}
		}
		$this->sectionLogin();
		$this->sectionRegister();
		$this->load->view('web/auth/index');
	}

	public function login(){
		$this->index();
	}

	public function register(){
		$this->index();
	}

	public function logout(){
		$this->user->logout();
		redirect('/home');
	}

	public function verifyEmail(){
		$data = array();
		$code = $this->input->get('code');
		if($this->loginMember['emailConfirm']){
			redirect('/my/profile/edit');
			return;
		}
		if($code){
			if($this->user->setEmailVerify($code,true)){
				redirect('/login');
			}else{
				$data['error'] = "รหัสยืนยันไม่ถูกต้อง";		
			}
		}
		$this->load->section('sec_verify','web/auth/verifyEmail',$data);
		$this->load->view('web/auth/index');
	}

	public function reverify(){
		
		if($this->loginMember){
			$this->sendmail->verifyEmail($this->loginMember['email'],$this->loginMember);
		}else{
			redirect('/login');
		}

		$this->load->section('sec_verify','web/auth/verifyEmail');
		$this->load->view('web/auth/index');
	}

	private function checkRef(){
		$ref = $this->input->post('ref');
		if($ref){
			if($ref=='invest'){
				redirect('/invest');
			}else{
				redirect('/my/sell/form');
			}
		}else{
			redirect('/my/profile/edit');
		}
	}
	private function sectionLogin()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$isremember = $this->input->post('isremember');
		$data = array();
		if($this->input->post('login')){
			if($this->validateLogin()){
				if(!$this->user->login($email,$password)){
					$data['error'] = "Email หรือ Password ไม่ถูกต้อง";
				}else{
					if($isremember){
						$this->user->rememberPassword();
					}
					if($this->user->isEmailVerify()){
						$this->checkRef();
					}else{
						$ref = $this->input->post('ref');
						redirect('/verifyEmail?ref='.$ref);
					}
				}	
			}else{
				$data['error'] = "Email หรือ Password ไม่ถูกต้อง";	
			}
		}
		$this->load->section('sec_login','web/auth/login',$data);
	}

	private function sectionRegister()
	{
		$data = array();
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');

		$data = $this->input->post();
		if($this->input->post('register')){
			if(!$this->validateRegister()){
				$data['error'] =  "รหัสผ่านไม่เหมือนกัน";
			}
			if($this->checkEmailDuplicate($email)){
				$data['error'] =  "Email นี้ถูกใช้งานแล้ว";
			}
			if(!isset($data['error'])){
				if($memberid = $this->saveMember($email,$password)){
					$member['_id'] = $memberid;
					$this->user->setCookie($member);
					redirect('/verifyEmail');
					//$this->checkRef();
				}
			}
		}
		
		$this->load->section('sec_register','web/auth/register',$data);
	}

	private function validateRegister(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(!empty($password) && $password == $confirm_password){
			return true;
		}
		return false;
	}
	private function checkEmailDuplicate($email){
		return ($this->mMember->where(array('email'=>$email))->count()>0);
	}

	private function validateLogin(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(!empty($email) && !empty($password)){
			return true;
		}
		return false;
	}

	private function saveMember($email,$password){
		$hashPassword = $this->user->hashPassword($password);
		$salt = $this->user->genSalt();
		$data = array(
				'email'=>$email,
				'password'=>$this->user->getEncryptionPassword($salt,$hashPassword),
				'salt'=>$salt,	
				'emailConfirm'=>false,
				'emailConfirmCode'=>md5($salt.$email),
				'createDate'=>date('Y-m-d H:i:s'),
				'updateDate'=>date('Y-m-d H:i:s')
			);
		$memberId = $this->mMember->insert($data);
		$this->sendmail->verifyEmail($email,$data);
		return $memberId;
	}

}