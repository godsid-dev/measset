<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('province_model','mProvince');
		$this->load->model('district_model','mDistrict');
		$this->load->model('tambon_model','mTambon');
		header('Content-Type: application/json');
	}

	public function province(){
		echo json_encode($this->mProvince->get());
	}

	public function district($province){
		if(!is_numeric($province)){
			$provinceQuery = $this->mProvince->where(array('name'=>urldecode($province)))->find_one();	
			$province_id = $provinceQuery['id'];
		}else{
			$province_id = $province;
		}
		echo json_encode($this->mDistrict->get_where(array('province_id'=>$province_id)));	
	}

	public function tambon($district){
		if(!is_numeric($district)){
			$districtQuery = $this->mDistrict->where(array('name'=>urldecode($district)))->find_one();
			$district_id = $districtQuery['id'];
		}else{
			$district_id = $district;
		}
		echo json_encode($this->mTambon->get_where(array('district_id'=>$district_id)));
	}
}