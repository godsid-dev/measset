<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sell extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->_init();
	}

	private function _init()
	{
		$this->render_page($this->user->isLogin());
		
	}

	public function index()
	{
		$this->sell();		
		$this->load->view('web/sell/index');
	}

	public function info(){
		$this->load->view('web/sell/info');	
	}

	private function sell()
	{
		$this->load->section('sell', 'web/sell/sell');
	}

	/*personal*/
	public function personal()
	{
		$data['breadcrumb'] = 'แก้ไขข้อมูลส่วนตัว';
		$this->load->section('sell_post', 'web/sell/sell_post', $data);
		$this->index();
	}

	public function message()
	{
		$data['breadcrumb'] = 'กล่องข้อความ';
		$this->load->section('message', 'web/sell/sell_my', $data);
		$this->index();
	}

	public function notification()
	{
		$data['breadcrumb'] = 'การแจ้งเตือน';
		$this->load->section('notification', 'web/sell/sell_history', $data);
		$this->index();
	}	

	/*sell*/
	public function sell_post()
	{
		$data['breadcrumb'] = 'ขายฝากใหม่';
		$this->load->section('sell_post', 'web/sell/sell_post', $data);
		$this->index();
	}

	public function sell_my()
	{
		$data['breadcrumb'] = 'รายการขายฝากของฉัน';
		$this->load->section('sell_my', 'web/sell/sell_my', $data);
		$this->index();
	}

	public function sell_history()
	{
		$data['breadcrumb'] = 'ประวัติการขายฝาก';
		$this->load->section('sell_history', 'web/sell/sell_history', $data);
		$this->index();
	}	

	/*invest*/
	public function invest_post()
	{
		$data['breadcrumb'] = 'ลงทุนใหม่';
		$this->load->section('invest_post', 'web/invest/invest_post', $data);
		$this->index();
	}

	public function invest_my()
	{
		$data['breadcrumb'] = 'รายการลงทุนของฉัน';
		$this->load->section('invest_my', 'web/invest/invest_my', $data);
		$this->index();
	}

	public function invest_history()
	{
		$data['breadcrumb'] = 'ประวัติการลงทุน';
		$this->load->section('invest_history', 'web/invest/invest_history', $data);
		$this->index();
	}	
}