<?php 
class Sell extends MY_Controller {

	var $loginMember;
	

	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->load->library('File');
		$this->load->model('product_model','mProduct');
		$this->_init();
	}

	private function _init()
	{
		$this->loginMember = $this->user->isLogin();
		if(!$this->loginMember){
			redirect('/login?ref=sell');
		}elseif(!$this->user->isEmailVerify()){
			redirect('/verifyEmail');
		}
		$this->render_page($this->loginMember);
		$this->load->section('sec_sidebar', 'web/my/sec_sidebar');
	}

	public function index(){
		$data['breadcrumb'] = 'ขายฝากใหม่';
		
		$where = array(
						'seller._id'=>$this->mProduct->id($this->loginMember['_id'])
						);

		$count = $this->mProduct->where($where)->count();
		$data['products'] = $this->mProduct
								->limit($this->limit)
								->offset($this->getOffset())
								->get_where($where);

		for($i =0,$j = sizeof($data['products']);$i<$j;$i++){
			//$data['products'][]
		}

		$pageing = $this->getPageing($count,base_url('/my/sell/'));
		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('form', 'web/my/sell/index',$data);
		$this->load->view('web/my/index',$data);
	}

	public function history(){
		$data['breadcrumb'] = 'ประวัติการขายฝากของฉัน';
		$where = array(
						'seller._id'=>$this->mProduct->id($this->loginMember['_id'])
						);

		$count = $this->mProduct->where($where)->count();
		$data['products'] = $this->mProduct
								->limit($this->limit)
								->offset($this->getOffset())
								->get_where($where);
		
		$pageing = $this->getPageing($count,base_url('/my/sell/history'));
		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('history', 'web/my/sell/history',$data);
		$this->load->view('web/my/index',$data);
	}

	public function product($id=null){
		if(!is_null($id)){
			return $this->product_detail($id);
		}
	}
	
	private function product_detail($id){

		$data['product'] = $this->mProduct->find_one(array('_id'=>$id));
		$this->load->view('web/my/sell/product',$data);
	}

	public function form(){
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		
		$data['breadcrumb'] = 'ขายฝากใหม่';
		$member = $this->loginMember;


		if(isset($member['seller'])){
			$seller = $member['seller'];
			if($seller['memberType'] == 'individual'){
				$this->load->section('occupation', 'web/component/occupation',$seller);
				$this->load->section('bank','web/component/bank',$seller['bank']);
				$this->load->section('form_individual', 'web/component/form_seller_individual',$seller);
			}
			if($seller['memberType'] == 'corporation'){
				$this->load->section('business_type', 'web/component/business_type',$seller);
				$this->load->section('bank','web/component/bank',$seller['bank']);
				$this->load->section('form_corporation', 'web/component/form_seller_corporation',$seller);
			}
		}elseif(isset($this->loginMember['default'])){
			$seller = $this->loginMember['default'];
			if($seller['memberType'] == 'individual'){
				
				$this->load->section('occupation', 'web/component/occupation',$seller);
				$this->load->section('bank','web/component/bank',$seller['bank']);
				$this->load->section('form_individual', 'web/component/form_seller_individual',$seller);
			}else{
				$this->load->section('business_type', 'web/component/business_type', $seller);
				$this->load->section('bank','web/component/bank',$seller['bank']);
				$this->load->section('form_corporation', 'web/component/form_seller_corporation',$seller);
			}
		}else{
			$this->load->section('business_type', 'web/component/business_type');
			$this->load->section('occupation', 'web/component/occupation');
			$this->load->section('bank','web/component/bank');
			$this->load->section('address','web/component/address');
			$this->load->section('form_individual', 'web/component/form_seller_individual');
			$this->load->section('form_corporation', 'web/component/form_seller_corporation');
		}

		$this->load->section('form', 'web/my/sell/form');
		$this->load->view('web/my/index',$data);
	}

	private function validateForm(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'คำนำหน้าชื่อ', 'required',
		 							array(
		 									'required' => 'คุณยังไม่ได้เลือก %s'
		 								)
		);
		return $this->form_validation->run();
	}


	public function form2($type){
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		$post = $this->input->post();
		$post['type'] = $type;
		if($post){
			$post['id_card_image'] = $this->file->uploadImageTemp('individual_id_card_image');
			$post['bank_image'] = $this->file->uploadImageTemp('bank_image');
			$post['home_document_image'] = $this->file->uploadImageTemp('home_document_image');
			$post['corporation_image'] = $this->file->uploadImageTemp('corporation_image');
			$data['form']['step1'] = base64_encode(json_encode($post));
		}

		$data['breadcrumb'] = 'ขายฝากใหม่';
		$this->load->section('address', 'web/component/address_land',$data);
		$this->load->section('google_map_js', 'web/googleMapJs');
		$this->load->section('form_step2', 'web/my/sell/form_step2',$data);
		$this->load->view('web/my/index',$data);
	}
	public function form3(){
		if($this->input->post()){
			$post = $this->input->post();
			$post['title_deed_image'] = $this->file->uploadImageTemp('title_deed_image');
			$data['form']['step1'] = $post['step1'];
			unset($post['step1']);
			$data['form']['step2'] = base64_encode(json_encode($post));
		}		
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		$data['breadcrumb'] = 'ขายฝากใหม่';
		
		$this->load->section('contract','web/component/contract');
		$this->load->section('form_step3', 'web/my/sell/form_step3',$data);
		$this->load->view('web/my/index',$data);
	}

	public function step4(){
		if($this->input->post()){
			$post = $this->input->post();
			$post['title_deed_image'] = $this->file->uploadImageTemp('title_deed_image');
			$data['form']['step1'] = $post['step1'];
			unset($post['step1']);
			$data['form']['step2'] = base64_encode(json_encode($post));
		}		
		
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		$data['breadcrumb'] = 'ขายฝากใหม่';
		
		$this->load->section('contract','web/component/contract');
		$this->load->section('form_step3', 'web/my/sell/form_step4',$data);
		$this->load->view('web/my/index',$data);
	}

	public function formsave(){
		$tmpFile = $_FILES['images'];
		$_FILES = array();
		$limit = (sizeof($tmpFile['name'])>5)?5:sizeof($tmpFile['name']);
		for($i=0;$i<$limit;$i++){
			$_FILES['images_'.$i] = array(
						'name'=>$tmpFile['name'][$i],
						'type'=>$tmpFile['type'][$i],
						'tmp_name'=>$tmpFile['tmp_name'][$i],
						'error'=>$tmpFile['error'][$i],
						'size'=>$tmpFile['size'][$i]
						);
			$file = $this->file->uploadImageTemp('images_'.$i);
			$images[] = $this->file->moveProductsImage($file);
		}		
		
		if($this->input->post()){
			$step3 = $this->input->post();
			$step1 = json_decode(base64_decode($step3['step1']),true);
			$step2 = json_decode(base64_decode($step3['step2']),true);

			unset($step3['step1']);
			unset($step3['step2']);

			$location = explode(',',$step2['address']['location']);

			if(isset($step1['id_card_image']) && $step1['id_card_image']){
				$step1['id_card_image'] = $this->file->moveMembersImage($step1['id_card_image']);
			}

			if(isset($step1['home_document_image']) && $step1['home_document_image']){
				$step1['home_document_image'] = $this->file->moveMembersImage($step1['home_document_image']);
			}

			if(isset($step1['corporation_image']) && $step1['corporation_image']){
				$step1['corporation_image'] = $this->file->moveMembersImage($step1['corporation_image']);
			}

			if(isset($step2['title_deed_image']) && $step2['title_deed_image']){
				$step2['title_deed_image'] = $this->file->moveMembersImage($step2['title_deed_image']);
			}
			if(isset($step1['bank_image']) && $step1['bank_image']){
				$step1['bank_image'] = $this->file->moveMembersImage($step1['bank_image']);
			}
			
			$product = array(
						'title'=>$step2['title'],
						'description'=>$step2['description'],
						'type'=>$step2['type'],
						'sheet'=>$step2['sheet'],
						'parcel_no'=>$step2['parcel_no'],
						'title_deed_number'=>$step2['title_deed_number'],
						'title_deed_image'=>$step2['title_deed_image'],

						'address'=>$step1['address'],
						'tambon'=>$step1['tambon'],
						'district'=>$step1['district'],
						'province'=>$step1['province'],
						'location'=>explode(',',$step2['address']['location']),
						'images'=>$images,
						'seller'=>array(
								'_id'=>$this->mProduct->id($this->loginMember['_id']),
								'memberType'=>$step1["type"],
								'title' => $step1["title"],
								'firstname' => $step1["firstname"],
								'lastname' => $step1["lastname"],
								'telephone' => $step1["telephone"],
								'bank'=>array(
									'id'=>$step1['bank_id'],
									'bank'=>$step1['bank'],
									'name'=>$step1['bank_name'],
									'image'=>$step1['bank_image']
									)
								),
						'specified'=>$step2['specified'],

						'price'=>preg_replace("#,#","",$step3['price']),
						'contract'=>$step3['contract'],
						'interest'=>$step3['interest']+$step3['interest_sub'],
						
						'approve'=>false,
						'approve_date'=>'',
						'create_date'=>date('Y-m-d H:i:s'),
						'status'=>'รอดำเนินการ',
						);
			$this->mProduct->insert($product);

			if(!isset($this->loginMember['default'])){
				$this->updateDefaultProfile($step1);
			}

			if(!isset($this->loginMember['seller'])){
				$this->updateSellerProfile($step1);
			}

			redirect('/my/sell');
		}
	}

	private function updateDefaultProfile($data){
		
		if($data['type'] == 'individual'){
			$default["memberType"] = $data['type'];
			$default["title"] = $data["title"];
			$default["firstname"] = $data["firstname"];
			$default["lastname"] = $data["lastname"];
			$default["birth_date"] = $data["birth_date"];
			$default["gender"] = ($data['title']=="นาย")?"ชาย":"หญิง";
			$default["id_card"] = $data["id_card"];
			$default["id_card_image"] = $data["id_card_image"];
			$default["home_document_image"] = $data["home_document_image"];
			$default["status"] = $data["status"];
			$default["occupation"] = $data["occupation"];
			$default["telephone"] = $data["telephone"];
			$default['address'] = $data['address'];
			$default['province'] = $data['province'];
			$default['district'] = $data['district'];
			$default['tambon'] = $data['tambon'];
			$default['postcode'] = $data['postcode'];
			
		}else{
			$default["memberType"] = $data["type"];
			$default["objective"] = isset($data["objective"])?$data["objective"]:"NO";
			$default["company_name"] = $data["company_name"];
			$default["registration_tax"] = $data["registration_tax"];
			$default["business_type"] = $data["business_type"];
			$default["contact_name"] = $data["contact_name"];
			$default["contact_phone"] = $data["contact_phone"];
			$default["corporation_image"] = $data["corporation_image"];
		}

		$default['bank'] = array(
										'id'=>$data['bank_id'],
										'bank'=>$data['bank'],
										'name'=>$data['bank_name'],
										'image'=>$data['bank_image']
									);
		
		$this->mMember
			->where(array('_id'=>$this->mMember->id($this->loginMember['_id'])));
		
		if(!isset($this->loginMember['default'])){
			$this->mMember->set('default',$default);
		}
		
		return $this->mMember->update();
	}
	private function updateSellerProfile($data){

		if($data['type'] == 'individual'){
			$seller["memberType"] = $data['type'];
			$seller["title"] = $data["title"];
			$seller["firstname"] = $data["firstname"];
			$seller["lastname"] = $data["lastname"];
			$seller["birth_date"] = $data["birth_date"];
			$seller["gender"] = ($data['title']=="นาย")?"ชาย":"หญิง";
			$seller["id_card"] = $data["id_card"];
			$seller["id_card_image"] = $data["id_card_image"];
			$seller["home_document_image"] = $data["home_document_image"];
			$seller["status"] = $data["status"];
			$seller["occupation"] = $data["occupation"];
			$seller["telephone"] = $data["telephone"];
			$seller['address'] = $data['address'];
			$seller['province'] = $data['province'];
			$seller['district'] = $data['district'];
			$seller['tambon'] = $data['tambon'];
			$seller['postcode'] = $data['postcode'];
			
		}else{
			$seller["memberType"] = $data["type"];
			$seller["objective"] = isset($data["objective"])?$data["objective"]:"NO";
			$seller["company_name"] = $data["company_name"];
			$seller["registration_tax"] = $data["registration_tax"];
			$seller["business_type"] = $data["business_type"];
			$seller["contact_name"] = $data["contact_name"];
			$seller["contact_phone"] = $data["contact_phone"];
			$seller["corporation_image"] = $data["corporation_image"];
		}
		$seller['bank'] = array(
							'id'=>$data['bank_id'],
							'bank'=>$data['bank'],
							'name'=>$data['bank_name'],
							'image'=>$data['bank_image']
						);

		$this->mMember
			->where(array('_id'=>$this->mMember->id($this->loginMember['_id'])));
		if(!isset($this->loginMember['seller'])){
			$this->mMember->set('seller',$seller);
		
		}	
		return $this->mMember->update();
	}	
		
}


?>