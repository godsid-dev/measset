<?php 
class Profile extends MY_Controller {

	var $loginMember;
	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->load->library('File');
		$this->_init();
	}

	private function _init()
	{
		$this->loginMember = $this->user->isLogin();
		if(!$this->loginMember){
			redirect('/login');
		}elseif(!$this->user->isEmailVerify()){
			
		}
		$this->render_page($this->loginMember);
		$this->load->section('sec_sidebar', 'web/my/sec_sidebar');
	}

	public function index(){
		$this->dashboard();
	}

	public function dashboard(){
		$data['breadcrumb'] = 'ข้อมูลส่วนตัว';
		$this->load->section('dashboard', 'web/my/profile/dashboard', $data);
		$this->load->view('web/my/index');
	}


	public function edit(){
		$data['breadcrumb'] = 'แก้ไขข้อมูลส่วนตัว';
		$this->load->css(assets_url('/themes/default/css/jquery-ui-1.10.2.css'));
		$this->load->js(assets_url('/themes/default/js/jquery-ui-1.10.2.js'));
		//$this->load->js('assets/themes/default/js/parsley.min.js');

		if(isset($this->loginMember['default'])){
			$member = $this->loginMember['default'];
		}else{
			$member = null;
		}
		//Load Form Individual & Corporate
		$this->load->section('address', 'web/component/address',$member);
		if($member == null){
			$this->load->section('occupation', 'web/component/occupation');
			$this->load->section('form_individual', 'web/component/form_individual');
			$this->load->section('business_type', 'web/component/business_type');
			$this->load->section('form_corporation', 'web/component/form_corporation');
		}else{
			//Load Form Individual
			if($member['memberType'] == 'individual' ){
				$this->load->section('occupation', 'web/component/occupation',$member);
				$this->load->section('form_individual', 'web/component/form_individual',$member);
			}

			//Load Form Corporate
			if($member['memberType'] == 'corporation' ){
				$this->load->section('business_type', 'web/component/business_type', $member);
				$this->load->section('form_corporation', 'web/component/form_corporation', $member);
			}	
		}
		$data['member'] = $member;
		$this->load->section('edit', 'web/my/profile/edit', $data);
		$this->load->view('web/my/index');

	}

	public function submit($type){
		if($type == 'individual'){
			$this->editIndividual();
		}elseif($type =='corporation'){
			$this->editCorporation();
		}
		
		redirect('/my/profile/edit');
		
	}

	private function editIndividual(){
		$post = $this->input->post();
		$member = $this->loginMember;

		if(isset($member['default'])){
			$default = $member['default'];
		}else{
			$default = $this->mMember->_fields['default']['individual'];
		}

		$default['memberType'] = 'individual';
		$default['title'] = $post['title'];
		$default['status'] = $post['status'];
		$default['firstname'] = $post['firstname'];
		$default['lastname'] = $post['lastname'];
		$default['birth_date'] = $post['birth_date'];
		$default['gender'] = ($post['title']=="นาย")?"ชาย":"หญิง";
		$default['id_card'] = $post['id_card'];
		$default['id_card_issue'] = $post['id_card_issue'];
		$default['address'] = $post['address'];
		$default['province'] = $post['province'];
		$default['district'] = $post['district'];
		$default['tambon'] = $post['tambon'];
		$default['postcode'] = $post['postcode'];
		$default['id_card_issue'] = $post['id_card_issue'];
		$default['occupation'] = $post['occupation'];
		$default['telephone'] = $post['telephone'];		

		if(isset($_FILES['id_card_image'])&& !$_FILES['id_card_image']['error']){
			$default['id_card_image'] = $this->file->uploadMemberImage('id_card_image');
		}

		if(isset($_FILES['home_document_image']) && !$_FILES['home_document_image']['error']){
			$default['home_document_image'] = $this->file->uploadMemberImage('home_document_image');
		}

		$status = $this->mMember->where(array('_id'=>$this->mMember->id($member['_id'])))
					->set('updateDate',date('Y-m-d H:i:s'))
					->set('default',$default)
					->update();
		return $status;

	}
	private function editCorporation(){
		$post = $this->input->post();
		$member = $this->loginMember;

		if(isset($member['default'])){
			$default = $member['default'];
		}else{
			$default = $this->mMember->_fields['default']['corporation'];
		}

		$default['memberType'] = 'corporation';
		$default['objective'] = $post['objective'];
		$default['company_name'] = $post['company_name'];
		$default['business_type'] = $post['business_type'];
		$default['registration_tax'] = $post['registration_tax'];
		$default['contact_name'] = $post['contact_name'];
		$default['contact_phone'] = $post['contact_phone'];

		if(isset($_FILES['corporation_image'])&& !$_FILES['corporation_image']['error']){
			$default['corporation_image'] = $this->file->uploadMemberImage('corporation_image');
		}

		$status = $this->mMember->where(array('_id'=>$this->mMember->id($member['_id'])))
					->set('updateDate',date('Y-m-d H:i:s'))
					->set('default',$default)
					->update();
		return $status;
	}

	public function message(){
		$this->load->model('Message_model','mMessage');
		$data['breadcrumb'] = 'กล่องข้อความ';
		$pageing = $this->getPageing(1,base_url());
		$messages = $this->mMessage
								->where(array(
									"member._id"=>$this->loginMember['_id'],
									))
								->order_by(array('create_date'=>'DESC'))
								->get();
		
		for($i=0,$j=sizeof($messages);$i<$j;$i++) {
			if($messages[$i]['type'] == 'adminConfirmProduct'){
				$messages[$i]['link'] = site_url('/my/sell/product/'.$messages[$i]['data']['product']['_id']);
			}elseif($messages[$i]['type'] == 'assessmentSuccess'){
				$messages[$i]['link'] = site_url('/my/sell/product/'.$messages[$i]['data']['product']['_id']);
			}else{
				$messages[$i]['link'] = site_url('/my/sell/product/'.$messages[$i]['data']['product']['_id']);
			}
		}
		$data['messages'] = $messages;

		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('message', 'web/my/profile/message', $data);
		$this->load->view('web/my/index');		

	}

	public function notification(){
		$data['breadcrumb'] = 'การแจ้งเตือน';
		$pageing = $this->getPageing(1,base_url());
		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('notification', 'web/my/profile/notification', $data);
		$this->load->view('web/my/index');		
	}

	public function changePassword(){
		$data['breadcrumb'] = 'แก้ไขรหัสผ่าน';
		$this->load->section('sec_changepassword','web/my/profile/changepassword');
		$this->load->view('web/my/index',$data);

	}

	public function submitChangePassword(){
		$data['breadcrumb'] = 'แก้ไขรหัสผ่าน';
		$lastpassword = $this->input->post('lastpassword');
		$password = $this->input->post('password');
		$repassword = $this->input->post('repassword');

		$error = $this->validateChangePassword();
		if($error === true){
			$data['error'] = "แก้ไขรหัสผ่านเรียบร้อย";
		}else{
			$data['error'] = $error;
		}

		$this->load->section('sec_changepassword','web/my/profile/changepassword',$data);
		$this->load->view('web/my/index');
	}

	private function validateChangePassword(){
		$lastpassword = $this->input->post('lastpassword');
		$password = $this->input->post('password');
		$repassword = $this->input->post('repassword');

		if($password != $repassword ){
			return "ยืนยันรหัสผ่านไม่ถูกต้อง";
		}
		$member = $this->mMember
			->where(array('_id'=>$this->loginMember['_id']))
			->find_one();

		if($this->user->checkPassword($member['password'],$member['salt'],$this->user->hashPassword($lastpassword))){
			return $this->user->changePassword($password);	
		}else{
			return "รหัสผ่านเดิมไม่ถูกต้อง";
		}
	}
	
}


?>