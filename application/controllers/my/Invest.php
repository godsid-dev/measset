<?php 
class Invest extends MY_Controller {

	var $loginMember;
	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->load->library('File');
		$this->load->model('product_model','mProduct');
		$this->_init();
	}

	private function _init()
	{
		$this->loginMember = $this->user->isLogin();
		if(!$this->loginMember){
			redirect('/login');
		}elseif(!$this->user->isEmailVerify()){
			redirect('/verifyEmail');
		}
		$this->render_page($this->loginMember);
		$this->load->section('sec_sidebar', 'web/my/sec_sidebar');
	}	

	public function index(){
		$data['breadcrumb'] = 'รายการลงทุนของฉัน';
		$user = $this->user->isLogin();
		/*if($user){
			$data['title'] = $user['default']['title'];
			$data['firstname'] = $user['default']['firstname'];
			$data['lastname'] = $user['default']['lastname'];
			$data['birth_date'] = $user['default']['birth_date'];
			$data['gender'] = $user['default']['gender'];
			$data['id_card'] = $user['default']['id_card'];
			$data['status'] = $user['default']['status'];
			$data['occupation'] = $user['default']['occupation'];
			$data['id_card_image'] = $user['default']['id_card_image'];
			$data['home_document_image'] = $user['default']['home_document_image'];
		}*/

		$where = array('investor._id'=>$this->mProduct->id($this->loginMember['_id']));

		$count = $this->mProduct->where($where)->count();
		$data['products'] = $this->mProduct
								->limit($this->limit)
								->offset($this->getOffset())
								->get_where($where);

		$pageing = $this->getPageing($count, base_url('/my/invest/'));
		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('invest', 'web/invest/invest_my', $data);
		$this->load->view('web/my/index');	

	}

	public function form(){
		$this->load->css(assets_url('/themes/default/css/jquery-ui-1.10.2.css'));
		$this->load->js(assets_url('/themes/default/js/jquery-ui-1.10.2.js'));
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		$data['breadcrumb'] = 'เพิ่มข้อมูลลงทุนใหม่';
		
		if(isset($this->loginMember['investor'])){
			$investor = $this->loginMember['investor'];
			if($this->loginMember['default']['memberType'] == 'individual'){
				$this->load->section('bank', 'web/component/bank', $investor['bank']);
				$this->load->section('address', 'web/component/address',$investor);
				$this->load->section('occupation', 'web/component/occupation',$investor);
				$this->load->section('form_individual', 'web/component/form_invest_individual',$investor);
			}elseif($this->loginMember['default']['memberType'] == 'corporation'){
				$this->load->section('bank', 'web/component/bank', $investor['bank']);
				$this->load->section('business_type', 'web/component/business_type', $investor);
				$this->load->section('form_corporation', 'web/component/form_invest_corporation',$investor);
			}
		}elseif(isset($this->loginMember['default'])){
			$investor = $this->loginMember['default'];
			if($investor['memberType'] == 'individual'){
				$this->load->section('bank', 'web/component/bank', $investor['bank']);
				$this->load->section('address', 'web/component/address',$investor);
				$this->load->section('occupation', 'web/component/occupation',$investor);
				$this->load->section('form_individual', 'web/component/form_invest_individual',$investor);
			}elseif($investor['memberType'] == 'corporation'){
				$this->load->section('bank', 'web/component/bank', $investor['bank']);
				$this->load->section('business_type', 'web/component/business_type', $investor);
				$this->load->section('form_corporation', 'web/component/form_invest_corporation',$investor);
			}
		}else{
			$investor = null;
			$this->load->section('address', 'web/component/address');
			$this->load->section('occupation', 'web/component/occupation');
			$this->load->section('bank', 'web/component/bank');
			$this->load->section('form_individual', 'web/component/form_invest_individual');
			$this->load->section('business_type', 'web/component/business_type');
			$this->load->section('form_corporation', 'web/component/form_invest_corporation');
		}
		
		$this->load->section('form', 'web/my/invest/form', $data);
		$this->load->view('web/my/index');
	}

	public function form2($type){
		//$this->load->js('assets/themes/default/js/parsley.min.js');
		$post = $this->input->post();
		$post['type'] = $type;
		if(isset($_FILES['id_card_image']) && !$_FILES['id_card_image']['error'] ){
			$post['id_card_image'] = $this->file->uploadMemberImage('id_card_image');
		}
		if(isset($_FILES['home_document_image']) && !$_FILES['home_document_image']['error'] ){
			$post['home_document_image'] = $this->file->uploadMemberImage('home_document_image');
		}
		if(isset($_FILES['corporation_image']) && !$_FILES['corporation_image']['error'] ){
			$post['corporation_image'] = $this->file->uploadMemberImage('corporation_image');
		}
		if($post){
			$data['form']['step1'] = base64_encode(json_encode($post));
		}

		$data['breadcrumb'] = 'เพิ่มข้อมูลลงทุนใหม่';

		$this->load->section('contract','web/component/contract');
		$this->load->section('form_step2', 'web/my/invest/form_step2',$data);
		$this->load->view('web/my/index',$data);
	}



	public function formsave(){
		$data['breadcrumb'] = 'เพิ่มข้อมูลลงทุนใหม่';
		if($this->input->post()){
			$member = $this->loginMember;
			$step2 = $this->input->post();
			$step1 = json_decode(base64_decode($step2['step1']),true);
			unset($step2['step1']);

			if(isset($this->loginMember['investor'])){
				$investor = $this->loginMember['investor'];
			}elseif(isset($this->loginMember['default'])){
				$investor = $this->loginMember['default'];
			}else{
				$investor = $this->mMember->_fields['investor'];
			}
			if($step1['type'] == 'individual'){
				$investor["title"] = $step1['title'];
				$investor["firstname"] = $step1['firstname'];
	            $investor["lastname"] = $step1['lastname'];
	            $investor["birth_date"] = $step1['birth_date'];
	            $investor["gender"] = ($step1['title']=="นาย")?"ชาย":"หญิง";
	            $investor["id_card"] = $step1['id_card'];
	            $investor['id_card_issue'] = $step1['id_card_issue'];
	            $investor["status"] = $step1['status'];
	            $investor["occupation"] = $step1['occupation'];
	            $investor["telephone"] = $step1['telephone'];
	            $investor['address'] = $step1['address'];
				$investor['province'] = $step1['province'];
				$investor['district'] = $step1['district'];
				$investor['tambon'] = $step1['tambon'];
				$investor['postcode'] = $step1['postcode'];
				if(!empty($step1['id_card_image'])){
					$investor['id_card_image']  = $step1['id_card_image'];
				}
				if(!empty($step1['home_document_image'])){
					$investor["home_document_image"] = $step1['home_document_image'];
				}

			}elseif($step1['type'] == 'corporation'){
				
				$investor["objective"] = $step1['objective'];
				$investor["company_name"] = $step1['company_name'];
	            $investor["registration_tax"] = $step1['registration_tax'];
	            $investor["business_type"] = $step1['business_type'];
	            $investor["contact_name"] = $step1['contact_name'];
	            $investor["contact_phone"] = $step1['contact_phone'];
	            if(!empty($step1['corporation_image'])){
					$investor["corporation_image"] = $step1['corporation_image'];
				}
			}

			$investor['interested'] = $step2['interested'];
			$investor['budget'] = preg_replace("#,#","",$step2['budget']);
			$investor['contract'] = $step2['contract'];
			
			unset($investor['corporation']);
			unset($investor['individual']);  
			
			$status = $this->mMember->where(array('_id'=>$this->mMember->id($member['_id'])))
						  ->set('investor',$investor)
						  ->update();
			
			if(!isset($member['default'])){
				$this->updateDefaultProfile($step1);
			}

			
		}
		$this->load->section('invest_thankyou','web/my/invest/thankyou');
		$this->load->view('web/my/index',$data);

	}

	public function confirmpayment($action=""){
		$data['breadcrumb'] = "แจ้งชำระเงิน";
		if($action == "submit"){
			return $this->confirmpayment_submit();
		}
		$this->load->css(assets_url('/themes/default/css/jquery-ui-1.10.2.css'));
		$this->load->js(assets_url('/themes/default/js/jquery-ui-1.10.2.js'));
		$this->load->section('confirm_payment_form','web/my/invest/confirm_payment_form');
		$this->load->view('web/my/index',$data);		
	}

	private function confirmpayment_submit(){
		$data['breadcrumb'] = "แจ้งชำระเงิน";
		$this->load->model('payment_model','mPayment');
		$post = $this->input->post();
		$payment = array(
				'transfer_date'=>$post['transfer_date'],
				'bank'=>$post['bank'],
				'transfer_id'=>$post['transfer_id'],
				'transfer_amount'=>$post['transfer_amount'],
				'investor'=>array(
								'_id'=>$this->loginMember['_id'],
								'name'=>$this->loginMember['investor']['firstname'].' '.$this->loginMember['investor']['lastname']
								),
				'checked'=>false,
				'create_date'=>date('Y-m-d H:i:s')
			);
		if(isset($_FILES['transfer_document']) && !$_FILES['transfer_document']['error'] ){
			$payment['transfer_document'] = $this->file->uploadMemberImage('transfer_document');
		}
		$this->mPayment->insert($payment);
		$this->load->section('confirm_payment_form','web/my/invest/confirm_payment_thankyou');
		$this->load->view('web/my/index',$data);		
	}

	public function history(){
		$data['breadcrumb'] = 'ประวัติการลงทุน';

		$where = array(
						'investor'=>$this->mProduct->id($this->loginMember['_id']), 
						);
		$count = $this->mProduct->where($where)->count();
		$data['products'] = $this->mProduct
								->limit($this->limit)
								->offset($this->getOffset())
								->get_where($where);

		$pageing = $this->getPageing($count, base_url('/my/invest/history'));
		$this->load->section('pageing', 'web/common/pageing',$pageing);
		$this->load->section('history', 'web/my/invest/history', $data);
		$this->load->view('web/my/index');
	}

	private function updateDefaultProfile($data){
		
		if($data['type'] == 'individual'){
			$default["memberType"] = $data['type'];
			$default["title"] = $data["title"];
			$default["firstname"] = $data["firstname"];
			$default["lastname"] = $data["lastname"];
			$default["birth_date"] = $data["birth_date"];
			$default["gender"] = ($data['title']=="นาย")?"ชาย":"หญิง";
			$default["id_card"] = $data["id_card"];
			$default['id_card_issue'] = $data['id_card_issue'];
			$default["id_card_image"] = $data["id_card_image"];
			$default["home_document_image"] = $data["home_document_image"];
			$default["status"] = $data["status"];
			$default["occupation"] = $data["occupation"];
			$default["telephone"] = $data["telephone"];
			$default['address'] = $data['address'];
			$default['province'] = $data['province'];
			$default['district'] = $data['district'];
			$default['tambon'] = $data['tambon'];
			$default['postcode'] = $data['postcode'];

		}else{
			$default["memberType"] = $data["type"];
			$default["objective"] = $data["objective"];
			$default["company_name"] = $data["company_name"];
			$default["registration_tax"] = $data["registration_tax"];
			$default["business_type"] = $data["business_type"];
			$default["contact_name"] = $data["contact_name"];
			$default["contact_phone"] = $data["contact_phone"];
			$default["corporation_image"] = $data["corporation_image"];

		}

		return $this->mMember
			->where(array('_id'=>$this->mMember->id($this->loginMember['_id'])))
			->set('default',$default)
			->update();

	}

}


?>