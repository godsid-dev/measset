<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invest extends MY_Controller {

	var $loginMember;
	function __construct()
	{
		parent::__construct();
		$this->load->model('member_model','mMember');
		$this->load->model('product_model','mProduct');
		$this->load->model('valuation_model','mValuation');

		$this->load->library('notification');
		$this->load->library('User');
		$this->_init();
	}

	private function _init()
	{
		$this->loginMember = $this->user->isLogin();
		$this->render_page($this->loginMember);
	}
	private function checkPermission(){
		if(!$this->loginMember){
			redirect('/login?ref=invest');
		}elseif(!isset($this->loginMember['investor'])){
			redirect('/my/invest/form');
		}
	}

	public function index()
	{		
		$this->checkPermission();
		$this->invest_list();
		//$this->loadmore_list();

		$this->load->view('web/invest/index');
	}

	public function detail($id)
	{
		$this->load->css('assets/themes/default/css/asRange.min.css');
		$this->load->js('assets/themes/default/js/jquery-asRange.min.js');
		
		$this->checkPermission();
		$data['detail'] = $this->mProduct->getDetailInvest($id);
		$data['user'] = $this->user->isLogin();
		$data['detail'][0]['estimates'] = $this->mValuation->where(array('product_id'=>$data['detail'][0]['_id']))->find_one();

		$this->invest_detail($data);
		if(isset($this->loginMember['investor']['status'])&&$this->loginMember['investor']['status']) { 
			$this->invest_price($id);
			$this->invest_info($data['detail'][0]['estimates']);
			//$this->invest_building();
			//$this->invest_law();
			//$this->invest_summary();
			//$this->invest_compare();
			//$this->invest_shortmap();
			//$this->invest_map();
			//$this->invest_scheme();
			//$this->invest_gallary();
		}
		$this->load->view('web/invest/detail');
	}

	public function info(){
		$this->load->view('web/invest/info');
	}

	private function invest_list()
	{		
		
		$data['list'] = $this->mProduct->getInvestList();
		for($i=0,$j=sizeof($data['list']);$i<$j;$i++){
			$data['list'][$i]['estimates'] = $this->mValuation->where(array('product_id'=>$data['list'][$i]['_id']))->find_one();
		}
		$this->load->section('invest_list', 'web/invest/invest_list', $data);
		
	}

	private function loadmore_list()
	{
		$this->load->section('loadmore_list', 'web/invest/loadmore_list');
	}

	private function invest_detail($data)
	{		
		/*ประเภททรัพย์สิน*/
		
		
		$this->load->section('invest_detail', 'web/invest/invest_detail', $data);
	}

	public function invest_update()
	{
		$this->load->library('Status');
		$this->checkPermission();

		$investor = array(
						'_id'=>$this->loginMember['_id'],
						'name'=>$this->loginMember['investor']['firstname'].' '.$this->loginMember['investor']['lastname'],
						'telephone'=>$this->loginMember['investor']['telephone']
					);


		$product_id = $_POST['product_id'];
		$price = $_POST['price'];
		$interest = $_POST['interest'];


		$product = $this->mProduct->updateInvestor($product_id,$investor,$price,$interest);


		$product = $this->mProduct
						->where(array('_id'=>$this->mProduct->id($product_id)))
						->find_one();

		$seller = $this->mMember
						->where(array('_id'=>$this->mMember->id($product['seller']['_id'])))
						->find_one();

		$this->notification->newBiding($product,$seller);
		
		//if($product){
			//$this->status->setInvest($product_id);
		//}
	}

	private function invest_price($id)
	{
		$this->load->library('Cost');

		$data['product'] = $this->mProduct
								->where(array('_id'=>$this->mProduct->id($id)))
								->find_one();
		$data['price'] = $this->cost->getInvesterPrice($data['product']['price'],$data['product']['interest'],$data['product']['contract']);
		/*ข้อมูลการลงทุน*/
		$this->load->section('invest_price', 'web/invest/invest_price',$data);
	}

	private function invest_info($data)
	{
		/*รายละเอียดที่ดิน*/
		$this->load->section('invest_info', 'web/invest/invest_info',$data);
	}

	private function invest_building()
	{
		/*รายละเอียดสิ่งปลูกสร้าง*/
		$this->load->section('invest_building', 'web/invest/invest_building');
	}

	private function invest_law()
	{
		/*ข้อจำกัดทางกฎหมายที่มีผลกระทบต่อทรัพย์สิน*/
		$this->load->section('invest_law', 'web/invest/invest_law');
	}

	private function invest_summary()
	{
		/*รายละเอียดสิ่งปลูกสร้าง*/
		$this->load->section('invest_summary', 'web/invest/invest_summary');
	}

	private function invest_compare()
	{
		/*ข้อมูลเปรียบเทียบ*/
		$this->load->section('invest_compare', 'web/invest/invest_compare');
	}

	private function invest_shortmap()
	{
		/*แผนที่สังเขปแสดงที่ตั้งทรัพย์สิน*/
		$this->load->section('invest_shortmap', 'web/invest/invest_shortmap');
	}

	private function invest_map()
	{
		/*แผนที่ตั้งทรัพย์สิน*/
		$this->load->section('invest_map', 'web/invest/invest_map');
	}

	private function invest_scheme()
	{
		/*ผังที่ดินและผังอาคาร*/
		$this->load->section('invest_scheme', 'web/invest/invest_scheme');
	}

	private function invest_gallary()
	{
		/*ภาพถ่ายสภาพทรัพย์สิน*/
		$this->load->section('invest_gallary', 'web/invest/invest_gallary');
	}
}