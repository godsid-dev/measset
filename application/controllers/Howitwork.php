<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HowItWork extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->_init();
	}

	public function _init(){
		$this->render_page($this->user->isLogin());
		
	}

	public function index(){
		$this->load->view('web/howitwork/index');
	}
}
