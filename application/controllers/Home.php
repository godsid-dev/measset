<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('User');
		$this->_init();
	}

	private function _init()
	{
		$this->render_home($this->user->isLogin());
		$this->load->helper('unitConvert');
	}

	public function index()
	{
		$this->load->section('sec_slide', 'web/home/sec-slide');
		$this->load->section('sec_recommend', 'web/home/sec-recommend');
		$this->load->section('sec_work', 'web/home/sec-work');
		$this->sec_sell();
		$this->sec_invest();
		$this->sec_contact();

		$this->load->view('web/home/index');
	}

	private function sec_regis()
	{
		$this->load->section('sec_regis', 'web/home/sec-register');
	}

	private function sec_sell()
	{
		$this->load->section('sec_sell', 'web/home/sec-sell');
	}

	private function sec_invest()
	{
		$this->load->section('sec_invest', 'web/home/sec-invest');
	}

	private function sec_contact()
	{
		$this->load->section('sec_contact', 'web/home/sec-contact');
	}
}