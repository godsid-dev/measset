<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Example extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	private function _init()
	{
		$this->render_page();
	}

	public function index()
	{
		$this->load->view('web/welcome');
	}

	public function example_1()
	{
		$this->load->view('web/welcome');
	}

	public function example_2()
	{
		$this->output->set_template('simple');
		$this->load->view('web/welcome');
	}

	public function example_3()
	{
		$this->load->section('sidebar', 'web/sidebar');
		$this->load->view('web/welcome');
	}

	public function example_4()
	{
		$this->output->unset_template();
		$this->load->view('web/welcome');
	}
}
