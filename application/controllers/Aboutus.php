<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutus extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('User');
		$this->_init();
	}

	private function _init()
	{
		$this->render_page($this->user->isLogin());
	}

	public function index()
	{
		$this->aboutus();
		$this->contactus();

		$this->load->view('web/aboutus/index');
	}

	private function aboutus()
	{
		$this->load->section('aboutus', 'web/aboutus/aboutus');
	}

	private function contactus()
	{
		$this->load->section('contactus', 'web/aboutus/contactus');
	}	
}