<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				รายการนักลงทุน
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
						<div class="row">
							<div class="col-lg-12">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ID-Code</th>
										<th>ชื่อสินทรัพย์</th>
										<th>ผู้ขาย</th>
										<th>นักลงทุนที่สนใจ</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										foreach ($products as $product) {
											
									?>
									<tr>
										<td><?php echo $product['_id']; ?></td>
										<td><?php echo $product['title']; ?></td>
										<td>
										<?php 
											
											if($product['seller']['memberType'] =='individual'){
												echo $product['seller']['title'],$product['seller']['firstname'],' ',$product['seller']['lastname'];
											}else{
												echo $product['seller']['company_name'];
											}
										?>	
										</td>
										<td>
											<?php
												echo (isset($product['investor']))?count($product['investor']):"-";
											?>
										</td>
										<td>
											<?php
												echo $product['status'];
											?>
										</td>
										<td>
											<a class="btn btn-primary btn-circle" href="<?php echo site_url('engines/consignment/'.$product['_id']); ?>">
											<i class="fa fa-search"></i>
											</a>
										</td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
							</div>
						</div>
						<?php echo $paging; ?>
					</div>
				</div>
				<!-- /.table-responsive -->
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>

