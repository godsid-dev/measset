<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				รายการนักลงทุน
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
						<div class="row">
							<div class="col-lg-12">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ID-Code</th>
										<th>Name</th>
										<th>invesment</th>
										<th>Status</th>
										<th>Tool</th>
									</tr>
								</thead>
								<tbody>
									<?php
										foreach ($investors as $investor) {
									?>
									<tr>
										<td><a href="<?php echo engines_url('/invester/'.$investor['_id'])?>" title="รายละเอียด"><?php echo $investor['_id']; ?></a></td>
										<td><a href="<?php echo engines_url('/invester/'.$investor['_id'])?>" title="รายละเอียด">
											<?php 
											if($investor['default']['memberType'] == 'individual'){
												echo $investor['investor']['title'],$investor['investor']['firstname']," ",$investor['investor']['lastname'];
											}else{
												echo $investor['investor']['company_name'];
											}
											?>
											</a>
										</td>
										<td>
											<?php
												echo number_format($investor['investor']['budget']);
											?>
										</td>
										<td>
											<?php 
											if(isset($investor['investor']['status']) && $investor['investor']['status']=='true'){
												//if($investor['investor']['status']=='true'){
													echo "<p class='text-success'>Approved</p>";
												//}else{
												//	echo "<p class='text-danger'>Waiting</p>";
												//}
											}else{
												echo"<p class='text-danger'>Waiting</p>";
											}
											?>
										</td>
										<td>
											<a href="<?php echo engines_url('/invester/'.$investor['_id'].'/edit')?>" title="แก้ไข">
												<button type="submit" name="approve" value="false" class="btn btn-warning btn-circle" ><i class="fa fa-edit"></i></button>
											</a>
											
											<form role="form" method="post" action="<?php echo site_url('engines/invester/approve/'.$investor['_id']);?>">
											<?php
												if(isset($investor['investor']['status']) && $investor['investor']['status']=='true'){
											?>
												<button type="submit" name="approve" value="false" class="btn btn-danger btn-circle" title="UnApprove"><i class="fa fa-times"></i></button>
											<?php
												}else{
											?>
												<button type="submit" name="approve" value="true" class="btn btn-success btn-circle" title="Approve"><i class="fa fa-check"></i></button>
											<?php
												}
											?>
											</form>
										</td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
							</div>
						</div>
						<?php echo $paging; ?>
					</div>
				</div>
				<!-- /.table-responsive -->
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>

