<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive">
			<form name="default" method="POST" action="<?php echo site_url('engines/valuation/saveform');?>" enctype="multipart/form-data">
			<p>1. ข้อมูลทั่วไป </p> 
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>ห้วข้อ</th>
						<th>รายละเอียด</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>1</td>
						<td>ประเภททรัพย์สิน</td>
						<td><input type="text" name="valuation_type" class="form-control" value="<?php echo isset($result['valuation_type'])?$result['valuation_type']:'';?>" required /></td>
					</tr>
					<tr>
						<td>2</td>
						<td>ที่ตั้งของทรัพย์สิน</td>
						<td><input type="text" name="valuation_address" class="form-control" value="<?php echo isset($result['valuation_address'])?$result['valuation_address']:'';?>" required /></td>
					</tr>
					<tr>
						<td>3</td>
						<td>เอกสารสิทธิ์ที่ดิน</td>
						<td><input type="text" name="valuation_doc" class="form-control" value="<?php echo isset($result['valuation_doc'])?$result['valuation_doc']:'';?>" required /></td>
					</tr>
					<tr>
						<td>4</td>
						<td>ภาระผูกพัน</td>
						<td><input type="text" name="valuation_obligation" class="form-control" value="<?php echo isset($result['valuation_obligation'])?$result['valuation_obligation']:'';?>" required /></td>
					</tr>
					<tr>
						<td>5</td>
						<td>วิธีการประเมินมูลค่า</td>
						<td><input type="text" name="valuation_method" class="form-control" value="<?php echo isset($result['valuation_method'])?$result['valuation_method']:'';?>" required /></td>
					</tr>
					<tr>
						<td>6</td>
						<td>วันที่ประเมิน</td>
						<td><input type="text" disabled name="valuation_date" class="form-control" value="<?php echo isset($result['valuation_date'])?$result['valuation_date']:date('Y-m-d');?>" required /></td>
					</tr>
					<tr>
						<td>7</td>
						<td>มูลค่าตลาดของทรัพย์สิน</td>
						<td><input type="number" name="valuation_value" class="form-control" value="<?php echo isset($result['valuation_value'])?$result['valuation_value']:'';?>" required /></td>
					</tr>
					<tr>
						<td>8</td>
						<td>มูลค่าบังคับขาย</td>
						<td><input type="number" name="valuation_force" class="form-control" value="<?php echo isset($result['valuation_value'])?$result['valuation_value']:'';?>" required /></td>
					</tr>
				</tbody>
			</table>


			<p>2. รายละเอียดที่ดิน</p>
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>โฉนดที่ดินเลขที่</th>
						<th>เลขที่ดิน</th>
						<th>หน้าสำรวจ</th>
						<th>ไร่</th>
						<th>งาน</th>
						<th>ตารางวา</th>
						<th>เพิ่ม</th>
					</tr>
				</thead>
				<tbody class="landlist" id="landlist">
					<?php
						if(isset($result['landData'])){
							foreach ($result['landData'] as $key => $value) {
					?>
					<tr>
						<td><input type="text" class="form-control" name="deedid[]" value="<?php echo isset($value['deedid'])?$value['deedid']:''; ?>"></td>
						<td><input type="text" class="form-control" name="landid[]" value="<?php echo isset($value['landid'])?$value['landid']:''; ?>"></td>
						<td><input type="text" class="form-control" name="survey[]" value="<?php echo isset($value['survey'])?$value['survey']:''; ?>"></td>
						<td><input type="number" class="form-control" name="acres[]" value="<?php echo isset($value['acres'])?$value['acres']:''; ?>"></td>
						<td><input type="number" class="form-control" name="quarte[]" value="<?php echo isset($value['quarte'])?$value['quarte']:''; ?>"></td>
						<td><input type="number" class="form-control" name="varh[]" value="<?php echo isset($value['varh'])?$value['varh']:''; ?>"></td>
						<td><p onclick='delBtn($(this))' class='btn btn-danger'> - </p></td>
					</tr>
					<?php
							}
						}
					?>
					<tr>
						<td><input type="text" class="form-control" value="" name="deedid[]"></td>
						<td><input type="text" class="form-control" value="" name="landid[]"></td>
						<td><input type="text" class="form-control" value="" name="survey[]"></td>
						<td><input type="number" class="form-control" value="" name="acres[]"></td>
						<td><input type="number" class="form-control" value="" name="quarte[]"></td>
						<td><input type="number" class="form-control" value="" name="varh[]"></td>
						<td><a id='addBtn' onclick='addBtn()' class='btn btn-success'>+</a></td>
					</tr>
				</tbody>
			</table>

			<p>3. สิทธิตามกฎหมาย</p>
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<tr>
						<td>
							<div class="radio">
                                <label>
                                    <input type="radio" name="hold" value="กรรมสิทธิ์สมบูรณ์" <?php echo isset($result['hold'])&&$result['hold']=='กรรมสิทธิ์สมบูรณ์'?'checked':''; ?> >
                                    กรรมสิทธิ์สมบูรณ์
                                </label>
                            </div>
						</td>
						<td>
							<div class="radio">
                                <label>
                                    <input type="radio" name="hold" value="สิทธิครอบครอง" <?php echo isset($result['hold'])&&$result['hold']=='สิทธิครอบครอง'?'checked':''; ?> >
                                    สิทธิครอบครอง
                                </label>
                            </div>
						</td>
						<td>
							<div class="radio">
                                <label>
                                    <input type="radio" name="hold" value="สิทธิการเช่า" <?php echo isset($result['hold'])&&$result['hold']=='สิทธิการเช่า'?'checked':''; ?> >
                                    สิทธิการเช่า
                                </label>
                            </div>
						</td>
					</tr>
				</tbody>
			</table>

			<p>4. ภาระผูกพัน</p>
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<tr>
						<td>
							<div class="radio">
                                <label>
                                    <input type="radio" name="obligation" value="ไม่มี" <?php echo isset($result['obligation'])&&$result['obligation']=='ไม่มี'?'checked':''; ?> >
                                    ไม่มี
                                </label>
                            </div>
						</td>
						<td>
							<div class="radio">
                                <label>
                                    <input type="radio" name="obligation" value="มีภาระผูกพัน" <?php echo isset($result['obligation'])&&$result['obligation']=='มีภาระผูกพัน'?'checked':''; ?> >
                                    มี
                                </label>
                            </div>
						</td>
					</tr>
				</tbody>
			</table>

			<p>5. ราคาประเมินทุนทรัพย์ในการจดทะเบียนสิทธิ และนิติกรรม</p>
			<div class="form-group">
				<input type="number" name="valuation_niti" class="form-control" value="<?php echo isset($result['valuation_niti'])?$result['valuation_niti']:''; ?>" >
				<p class="help-block">ตารางวาละ/บาท</p>
			</div>

			<p>6. เอกสารการประเมินราคา</p>
			<div class="form-group">
				<input type="file" name="valuation_file" id="valuation_file">
				<p class="help-block">pdf, doc, excel</p>
			</div>

			<p>7. การตรวจสอบตำแหน่งที่ตั้งทรัพย์สิน</p>
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<tr>
						<td>
							ค่าพิกัด GPS
						</td>
						<td>
							LAT
						</td>
						<td>
							<input type="text" name="lat" class="form-control" value="<?php echo isset($result['lat'])?$result['lat']:'';?>" required="" />
						</td>
						<td>
							LONG
						</td>
						<td>
							<input type="text" name="lon" class="form-control" value="<?php echo isset($result['lon'])?$result['lon']:'';?>" required="" />
						</td>
					</tr>
				</tbody>

				<tbody>
					<tr>
						<td colspan="5">
							<!--<input type="hidden" name="valuation_group" value="<?php echo $fieldgroup; ?>">-->
							<input type="hidden" name="valuation_product" value="<?php echo $product; ?>">
							<!--<input type="hidden" name="valuation_callback" class="form-control" value="<?php echo $callback;?>" />-->
							<input type="submit" class="btn btn-success" value="บันทึกข้อมูล" />
						</td>
					</tr>
				</tbody>
			</table>

            </form>
		</div>
	</div>
</div>
