<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Striped Rows
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
						<div class="row">
							<div class="col-lg-12">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ID-Code</th>
										<th>Title</th>
										<th>Type</th>
										<th>Seller</th>
										<th>Investor</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										foreach ($result as $key => $value) {
											//var_dump($value);
											//die();
									?>
									<tr>
										<td><?php echo $value['_id']; ?></td>
										<td><?php echo $value['title']; ?></td>
										<td><?php echo $value['type']; ?></td>
										<td>
										<?php 
											$tmpSeller=$this->mMember->where(array('_id'=>$value['seller']))->get();

											if(isset($tmpSeller[0]['seller'])){
												echo $tmpSeller[0]['seller']['individual']['title'],$tmpSeller[0]['seller']['individual']['firstname'],' ',$tmpSeller[0]['seller']['individual']['lastname'];
											}else{
												echo"-";
											}
										?>	
										</td>
										<td>
										<?php 
											if(isset($value['investor'][0])){
											$tmpInvestor=$this->mMember->where(array('_id'=>$value['investor'][0]))->get();

											//$tmpInvestor=$this->mMember->get_where($value['investor'][0]);
												if(isset($tmpInvestor[0]['investor'])){
													echo $tmpInvestor[0]['investor']['individual']['title'],$tmpInvestor[0]['investor']['individual']['firstname'],' ',$tmpInvestor[0]['investor']['individual']['lastname'];
												}else{
													echo"ไม่มีข้อมูล";
												}
											}else{
												echo"-";
											}
										?>	
										</td>
										<td>
											<?php 
												echo isset($value['product_status'])?$value['product_status']:'รอดำเนินการ';
											?>
										</td>
										<td>
											<a class="btn btn-primary btn-circle" href="<?php echo site_url('engines/consignment/datadetail/'.$value['_id']); ?>">
											<i class="fa fa-edit"></i>
											</a>
										</td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
							</div>
						</div>
						<?php echo $paging; ?>
					</div>
				</div>
				<!-- /.table-responsive -->
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>

