<?php 
	//$seller = $product['seller'];
	//

?>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>
<div class="row">
		<div class="col-lg-7">

			<div class="panel panel-green">
				<div class="panel-heading">
					<i class="fa fa-user fa-fw"></i> ข้อมูลผู้ขาย
				</div>
				<div class="panel-body">
					<strong>
						<?php 
							if($seller['memberType']=='individual'){
								echo $seller['title'],$seller['firstname']," ",$seller['lastname'];
							}else{
								echo $seller['campany_name'];
							}
						?>
					</strong>
					<address>
						<abbr title="Phone"> Tel. </abbr> <?php echo $seller['telephone'] ?>
					</address>
					<address>
						<abbr title="Email"> Email. </abbr> <?php echo $seller['email'] ?>
					</address>
					<address>
						<b title="บัตรประชาชน"> บัตรประชาชน. </b> <?php echo $seller['id_card'] ?>
					</address>
					<strong>เอกสารสำคัญ</strong>
					<ul>
						<li>
							<a href="<?php echo static_url($seller['id_card_image']) ?>">สำเนาบัตรประชาชน</a>
						</li>
						<li>
							<a href="<?php echo static_url($seller['home_document_image']) ?>">สำเนาทะเบียนบ้าน</a>
						</li>
						<li>
							<a href="<?php echo static_url($seller['bank']['image']) ?>">สำเนาหน้าบัญชีธนาคาร</a>
						</li>
					</ul>
					
				</div>
			</div>

			<div class="panel panel-primary">
				<div class="panel-heading">
					<i class="fa fa-user fa-fw"></i> สินทรัพย์ <?php echo "เลขที่ : ",$product['_id']?>
				</div>
				<div class="panel-body">
				<form role="form">
					<fieldset disabled="">
					<div class="form-group">
						<label for="txtTitle">ชื่อสินทรัพย์</label>
						<input id="txtTitle" class="form-control" type="text" value="<?php echo $product['title']; ?>" >
					</div>
					<div class="form-group">
						<label for="txtTitle">รายละเอียด</label>
						<textarea id="txtTitle" class="form-control"><?php echo $product['description']; ?></textarea>
					</div>
					<div class="form-group">
						<label for="disabledSelect">ประเภทสินทรัพย์</label>
						<select id="disabledSelect" class="form-control">
							<option><?php echo $product['type']; ?></option>
						</select>
					</div>
					<div class="form-group">
						<label>โฉนดเลขที่</label>
						<input type="text" class="form-control" name="title_deed_number" value="<?php echo $product['title_deed_number']; ?>">
					</div>
					<div class="form-group">
						<label for="tstSheet">หมายเลขระวา</label>
						<input id="tstSheet" class="form-control" type="text" value="<?php echo $product['sheet']; ?>" >
					</div>
					<div class="form-group">
						<label for="txtParchel">หมายเลขที่ดิน</label>
						<input id="txtParchel" class="form-control" type="text" value="<?php echo $product['parcel_no']; ?>" >
					</div>
					<!--
					<div class="form-group">
						<label for="txtParchel">ขนาด</label>
						<input id="txtParchel" class="form-control" type="text" value="<?php echo $product['scale']; ?>" >
					</div>
					-->
					<div class="form-group">
						<label for="txtParchel">ขนาดพื้นที่</label>
						<input id="txtParchel" class="form-control" type="text" value="<?php echo isset($product['specified']['square_wah'])?$product['specified']['square_wah']:0; ?>" >
					</div>
					<div class="form-group">
						<label for="txtParchel">Address</label>
						<textarea class="form-control" rows="3"><?php echo $product['address']," ต.", $product['tambon'],"\nอ.",$product['district'],"\nจ.",$product['province'] ?>
						</textarea>
					</div>
					
					</fieldset>
					</form>
					<form method="post" action="<?php echo site_url('engines/consignment/approve/'.$product['_id']); ?>">
					<fieldset>
						<div class="form-group">
							<label for="txtParchel">รูปแบบการประมูล</label>
							<select name="bidtype" class="form-control">
								<option value="">-- เลือกรูปแบบการประมูล --</option>
								<option value="price" <?php echo (isset($product['bidtype'])&&$product['bidtype']=='price')?'selected':'' ?>>ราคา</option>
								<option value="interest" <?php echo (isset($product['bidtype'])&&$product['bidtype']=='interest')?'selected':'' ?>>ดอกเบี้ย</option>
								<option value="both" <?php echo (isset($product['bidtype'])&&$product['bidtype']=='both')?'selected':'' ?>>ราคา/ดอกเบี้ย</option>
							</select>
						</div>
						<div class="form-group">
							<button type="submit" name="approve" value="true" class="btn btn-success <?php echo $product['approve']=='true'?'disabled':''; ?>">
								ยืนยันสินทรัพย์
							</button>
							<button type="submit" name="approve" value="false" class="btn btn btn-danger <?php echo $product['approve']=='true'?'':'disabled'; ?>">
								ไม่ยืนยันสินทรัพย์
							</button>
						</div>
					</fieldset>
					</form>
					<?php if($product['status'] == 'รอตรวจสอบการประเมิน'){?>
					<form method="post" action="<?php echo engines_url('/consignment/assessor/approve/'.$product['_id'])?>">
						<fieldset>
							<div class="form-group">
								<label></label>
								<button type="submit" name="approve" value="true" class="btn btn-warning">
										ยืนยันการประเมิน
								</button>
							</div>
						</fieldset>
					</form>
					<?php }elseif($product['status'] == 'กำลังขาย'){ ?>
							<a href="<?php echo engines_url('/assessor/'.$product['_id'])?>" class="btn btn-warning">
										ข้อมูลการประเมิน
							</a>
					<?php }?>
				</div>
			</div>

			<div class="panel panel-primary">
				<div class="panel-heading">
					<i class="fa fa-user fa-fw"></i> แผนที่
				</div>
				<div class="panel-body">
					<?php 
						echo $this->load->get_section('box_googlemap');
					?>
				</div>
			</div>
		</div>


		<div class="col-lg-5">

			<div class="panel panel-red">
				<div class="panel-heading">
					<i class="fa fa-user fa-fw"></i> ข้อมูลการลงทุน
				</div>
				<div class="panel-body table-responsive">
					<table class="table table-striped table-bordered table-hover"  style="cursor: pointer;">
						<tr>
							<th>Name</th>
							<th>Price</th>
							<th>Interest</th>
						</tr>
				<?php if(isset($product['investor'])){
						foreach ($product['investor'] as $investor) { ?>
						<tr onclick="window.location='<?php echo engines_url('/invester/'.$investor['_id'])?>'">
							<td><?php echo $investor['name'];?><br/>
							<?php echo $investor['telephone']; ?><br/>
							<?php //echo $investor['email']; ?></td>
							<td><?php echo number_format($investor['price']); ?></td>
							<td><?php echo number_format($investor['interest'],2); ?>%</td>
						</tr>
				<?php }
					}else{ ?>
						<td colspan="3">ยังไม่มีนักลงทุน</td>
				<?php }?>
				</table>
				</div>
			</div>
		</div>
</div>