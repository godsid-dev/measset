<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<?php echo $this->load->get_section('box_seller');?>
		<?php echo $this->load->get_section('box_product');?>
		<?php //echo $this->load->get_section('box_valuation');?>
	</div>
</div>