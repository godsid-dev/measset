<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $title; ?></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Striped Rows
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
						<div class="row">
							<div class="col-lg-12">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Name</th>
										<th>Information</th>
										<th>Slip</th>

										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$count = 1;
										foreach ($payments as $payment) {
											
									?>
									<tr>
										<td><a href="<?php echo engines_url('/invester/'.$payment['investor']['_id'])?>"><?php echo $count++; ?></a></td>
										<td><a href="<?php echo engines_url('/invester/'.$payment['investor']['_id'])?>"><?php echo $payment['investor']['name']; ?></a>
										</td>
										<td>
											<span>ธนาคาร: <?php echo $payment['bank']?></span><br/>
											<span>บัญชี: <?php echo $payment['transfer_id']?></span><br/>
											<span>จำนวน: <?php echo $payment['transfer_amount']?></span><br/>
											<span>วันที่: <?php echo $payment['transfer_date']?></span>
										</td>
										<td><a href="<?php echo static_url('/'.$payment['transfer_document'])?>" target="_blank" class="fancybox"><img src="<?php echo static_url('/'.$payment['transfer_document'])?>" width="100"></a></td>
										<td>
											<?php 
												echo $payment['checked']?"ตรวจสอบแล้ว":"รอตรวจสอบ";
											?>
										</td>
										<td>
											<?php if(!$payment['checked']){?>
											<a class="btn btn-success btn-circle" href="<?php echo engines_url('/payment/approve/'.$payment['_id']); ?>" title="อนุมัติ">
											<i class="fa fa-check"></i>
											</a>
											<?php }?>
										</td>
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
							</div>
						</div>
						<?php echo $paging; ?>
					</div>
				</div>
				<!-- /.table-responsive -->
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.fancybox').fancybox();	
	});
</script>
