            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <?php 
                            $this->user->isLogin();
                            if($this->user->acl(LEVEL_ADMIN | LEVEL_WEBMASTER)){?>
                        <!--li>
                            <a href="<?php echo site_url('/engines/home'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li-->
                        <li>
                            <a href="<?php echo site_url('/engines/invester'); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> ข้อมูลนักลงทุน</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/engines/consignment'); ?>"><i class="fa fa-table fa-fw"></i> ข้อมูลรายการขายฝาก</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/engines/payment'); ?>"><i class="fa fa-table fa-fw"></i> แจ้งชำระเงิน</a>
                        </li>
                        <!--li>
                            <a href="<?php echo site_url('/engines/consignment/investor'); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> สินทรัพย์ที่นักลงทุนสนใจ </a>
                        </li-->
                        
                        <?php }?>
                        <!-- สำหรับผู้ประเมิน -->
                        <li>
                            <a href="<?php echo engines_url('/assessor'); ?>"><i class="fa fa-table fa-fw"></i> รายการรอประเมิน</a>
                        </li>
                    </ul>
                </div>
            </div>
