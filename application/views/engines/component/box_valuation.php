<div class="panel panel-primary">
	<div class="panel-heading">
		<i class="fa fa-user fa-fw"></i> ข้อมูลการประเมิน
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<form id="valuationForm" name="default" method="POST" action="<?php echo engines_url('/assessor/submit/'.$_id);?>" enctype="multipart/form-data">

			<div class="panel panel-default">
		        <div class="panel-body">
		        	<div class="form-group ">
			        	<div class="form-group col-xs-4">
			                <label>หน่วยงาน/สาขา</label>
			                <input class="form-control" name="brance" id="brance" required="" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>วันที่รับเรื่อง</label>
			                <input class="form-control" name="open_date" id="open_date" required="" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>วันที่ประเมิน</label>
			                <input class="form-control" name="valuation_date"  id="valuation_date" required="" />
			            </div>
		            </div>
		            <div class="form-group">
		            	<div class="form-group col-xs-4">
			                <label>หน่วยงานที่ประเมิน</label>
			                <input class="form-control" name="rating_agencie" id="rating_agencie" required="" />
			                
			            </div>
			            <div class="form-group col-xs-4">
			                <label>เลขที่รับงาน</label>
			                <input class="form-control" name="job_referance" id="job_referance" required="" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>วัตถุประสงค์</label>
			                <input class="form-control" name="objective" id="objective" />
			            </div>
		            </div>
		        
		        	<div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>ชื่อลูกค้า</label>
			                <input class="form-control" name="customer" id="customer" value="<?php echo $product['seller']['firstname'],' ',$product['seller']['lastname']?>" />
			                
			            </div>
			            <div class="form-group col-xs-6">
			                <label>ประเภททรัพย์สิน</label>
			                <select id="type" class="select-box form-control" name="type" required="" onchange="changeType(this)">
		                        <option value="">- เลือกประเภทสินทรัพย์ -</option>
		                        <option value="ที่ดินเปล่า" <?php echo ($product['type']=='ที่ดินเปล่า')?'selected':''?>>ที่ดินเปล่า</option>
		                        <option value="ที่การเกษตร" <?php echo ($product['type']=='ที่การเกษตร')?'selected':''?>>ที่การเกษตร</option>
		                        <option value="คอนโดมิเนียม/อพาร์ทเม้นท์" <?php echo ($product['type']=='คอนโดมิเนียม/อพาร์ทเม้นท์')?'selected':''?>>คอนโดมิเนียม/อพาร์ทเม้นท์</option>
		                        <option value="บ้านเดี่ยว" <?php echo ($product['type']=='ที่ดินเปล่า')?'selected':''?>>บ้านเดี่ยว</option>
		                        <option value="ทาวน์เฮ้าส์/ทาวน์โฮม" <?php echo ($product['type']=='ทาวน์เฮ้าส์/ทาวน์โฮม')?'selected':''?>>ทาวน์เฮ้าส์/ ทาวน์โฮม</option>
		                        <option value="ตึกแถว/อาคารพาณิชย์" <?php echo ($product['type']=='ตึกแถว/อาคารพาณิชย์')?'selected':''?>>ตึกแถว/ อาคารพาณิชย์</option>
		                        <option value="โรงแรม/รีสอร์ท/โรงงาน"<?php echo ($product['type']=='โรงแรม/รีสอร์ท/โรงงาน')?'selected':''?>>โรงแรม/ รีสอร์ท/ โรงงาน</option>
		                    </select>
		                    <script type="text/javascript">
		                    	function changeType(obj){
		                    		console.log(obj.value);

		                    		if(obj.value == "คอนโดมิเนียม/อพาร์ทเม้นท์"){
		                    			$("#rai,#ngan,#wah").attr("disabled","disabled");
		                    			$("#meter").removeAttr("disabled");
		                    		}else{
		                    			$("#rai,#ngan,#wah").removeAttr("disabled");
		                    			$("#meter").attr("disabled","disabled");
		                    		}
		                    	}
		                    </script>
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-4">
			                <label>เอกสารสิทธิ</label>
			                <input class="form-control" id="title_deed" name="title_deed" />
			            </div>
			            <div class="form-group col-xs-2">
			                <label>เนื้อที่</label>
			                <input class="form-control"  name="rai" id="rai" />
			                <span class="input-group-addon">ไร่</span>
			            </div>
			            <div class="form-group col-xs-2">
			            	<label>&nbsp;</label>
			                <input class="form-control" name="ngan" id="ngan" />
			                <span class="input-group-addon">งาน</span>
			            </div>
			            <div class="form-group col-xs-2">
			            	<label>&nbsp;</label>
			                <input class="form-control"  name="wah" id="wah" />
			                <span class="input-group-addon">ตร.ว.</span>
			            </div>
			            <div class="form-group  col-xs-2">
			                <label>หรือ</label>
			                <input class="form-control" name="meter" id="meter" />
			                <span class="input-group-addon">ตร.ม.</span>
			            </div>
		            </div>
		            <!--div class="form-group">
			        	<div class="form-group col-xs-3">
			                <label>ลักษณะของที่ดิน</label>
			                <input class="form-control" name="nature_of_land" id="nature_of_land" />
			            </div>
			            <div class="form-group input-group col-xs-3">
			                <label>รูปแปลงที่ดินเป็น</label>
			                <input class="form-control" name="land_shape" id="land_shape" />
			            </div>
			            <div class="form-group input-group col-xs-3">
			                <label>กว้าง</label>
			                <input class="form-control" name="land_wide" id="land_wide" />
			                <span class="input-group-addon">เมตร</span>
			            </div>
			            <div class="form-group input-group col-xs-3">
			                <label>ยาว</label>
			                <input class="form-control" name="land_long" id="land_long" />
			                <span class="input-group-addon">เมตร</span>
			            </div>
		            </div-->
		            <div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>ชื่อผู้ถือกรรมสิทธิ์</label>
			                <input class="form-control" name="owner_name" id="owner_name" />
			            </div>
			            <div class="form-group input-group col-xs-6">
			                <label>ภาระติดพัน</label>
			                <input class="form-control" name="responsibility" id="responsibility" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-4">
			                <label>ประเภทสิ่งปลูกสร้าง</label>
			                <input class="form-control" name="building_type" id="building_type" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>การก่อสร้างอาคาร</label>
			                <input class="form-control" name="building_status" id="building_status" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>การใช้ประโยชน์ปัจจุบัน</label>
			                <input class="form-control" name="building_use" id="building_use" />
			            </div>
			            
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-4">
			                <label>สภาพการดูแลรักษา</label>
			                <input class="form-control" name="maintenance" id="maintenance" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>พื้นที่ใช้สอยรวม</label>
			                <input class="form-control" name="total_area" id="total_area" />
			                <span class="input-group-addon">ตร.ม.</span>
			            </div>
			            <div class="form-group col-xs-4">
			                <label>อายุอาคาร</label>
			                <input class="form-control" name="building_age" id="building_age" />
			                <span class="input-group-addon">ปี</span>
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-3">
			                <label>กรรมสิทธิ์สิ่งปลูกสร้าง </label>
			                <select name="building_own" id="building_own" class="form-control">
			                	<option value="มี">มี</option>
			                	<option value="ไม่มี">ไม่มี</option>
			                </select>
			            </div>
			            <div class="form-group col-xs-5">
			                <label>ตรวจสอบจาก</label>
			                <input class="form-control" name="building_own_by" id="building_own_by" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>ชื่อผู้ถือกรรมสิทธิ์</label>
			                <input class="form-control" name="building_own_name" id="building_own_name" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-4">
			                <label>ที่ตั้ง บ้านเลขที่</label>
			                <input class="form-control" name="address" id="address" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>หมู่บ้าน</label>
			                <input class="form-control" name="village" id="village" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>ซอย</label>
			                <input class="form-control" name="soi" id="soi" />
			            </div>
		            </div>

		            <div class="form-group">
			        	<div class="form-group col-xs-4">
			                <label>ถนน</label>
			                <input class="form-control" name="street" id="street" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>ตำบล/แขวง</label>
			                <input class="form-control" name="tambon" id="tambon" />
			            </div>
			            <div class="form-group col-xs-4">
			                <label>อำเภอ/เขต</label>
			                <input class="form-control" name="district" id="district" />
			            </div>
		            </div> 
					<div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>จังหวัด</label>
			                <input class="form-control" name="province" id="province" />
			            </div>
			            <div class="form-group col-xs-6">
			                <label>ระยะห่างจากสาขา</label>
			                <input class="form-control" name="distance" id="distance" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>พิกัด GPS LAT</label>
			                <input class="form-control" name="location_lat" id="location_lat" />
			            </div>
			            <div class="form-group col-xs-6">
			                <label>LONG</label>
			                <input class="form-control" name="location_lon" id="location_lon" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-12">
			                <label>สภาพแวดล้อม</label>
			                <input class="form-control" name="environment" id="environment" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>การจัดสรรที่ดิน</label>
			                <input class="form-control" name="land_allotment" id="land_allotment" />
			            </div>
			            <div class="form-group col-xs-6">
			                <label>สภาพคล่องในการซื้อขาย</label>
			                <input class="form-control" name="liquidity" id="liquidity" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-2">
			                <label>สภาพทางเข้า-ออก</label>
			                <select name="gate" id="gate" class="form-control">
			                	<option value="ไม่มีปัญหา">ไม่มีปัญหา</option>
			                	<option value="มีปัญหา">มีปัญหา</option>
			                </select>
			                
			            </div>
			            <div class="form-group col-xs-10">
			            	<label>เนื่องจาก</label>
				            <input class="form-control" name="gate_detail" id="gate_detail" />
			            </div>

			            
		            </div>

		            <div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>องค์การปกครองส่วนท้องถิ่น</label>
			                <input class="form-control" name="local_government" id="local_government" />
			            </div>
			            <div class="form-group col-xs-6">
			                <label>สำนักงานเขตที่ดิน</label>
			                <input class="form-control" name="office_property" id="office_property" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-6">
			                <label>ผังเมือง</label>
			                <input class="form-control" name="city_plan" id="city_plan" />
			            </div>
			            <div class="form-group col-xs-6">
			                <label>ประเภท</label>
			                <input class="form-control" name="city_plan_type" id="city_plan_type" />
			            </div>
		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-3">
			                <label>ตรวจสอบแนวเวนคืน</label>
			                <select name="expropriation" id="expropriation" class="form-control">
			                	<option value="อยู่ในแนวเวนคืนของ">อยู่ ในแนวเวนคืนของ</option>
			                	<option value="ไม่อยู่ในแนวเวนคืน">ไม่อยู่ ในแนวเวนคืน</option>
			                </select>
			                
			            </div>
			            <div class="form-group col-xs-9">
			            	<label>&nbsp;</label>
			            	<input class="form-control" name="expropriation_detail" id="expropriation" />
			            </div>

		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-3">
			                <label>ปัญหามลพิษ</label>
			                <select name="pollution" id="pollution" class="form-control">
			                	<option value="พบ">พบ</option>
			                	<option value="ไม่พบ">ไม่ พบ</option>
			                </select>
			            </div>
			            <div class="form-group col-xs-9">
			            	<label>&nbsp;</label>
			            	<input class="form-control" name="pollution_detail" id="pollution_detail" />
			            </div>

		            </div>
		            <div class="form-group">
			        	<div class="form-group col-xs-4" >
			                <label>ราคาประเมินราชการ ตร.ว. ละ</label>
			                <input class="form-control" name="appraisal_service" id="appraisal_service" />
			                <span class="input-group-addon">บาท</span>
			            </div>
			            <div class="form-group col-xs-4">
			                <label>มูลค่ารวม</label>
			                <input class="form-control input-group" name="total_value" id="total_value" />
			                <span class="input-group-addon">บาท</span>
			            </div>
			            <div class="form-group col-xs-4">
			                <label>ราคาประเมิน</label>
			                <input class="form-control" name="cost_estimate" id="cost_estimate" />
			            </div>
		            </div>

		            <div class="form-group col-xs-12">
		            	<input type="hidden" name="product_id" id="<?php echo $_id; ?>" />
						<input type="submit" class="btn btn-success" id="บันทึกข้อมูล" />
					</div>
		        </div>
			</div>
		    </form>
		</div>
	</div>
</div>
<?php if(isset($isDisable) && $isDisable){?>
<!-- Disable input  -->
<script type="text/javascript">
	/*setTimeout(function(){
		$(document).ready(function(){
			var valuation = <?php echo json_encode($valuation)?>;
			for (var key in valuation){
				
				$('#'+key).val(valuation[key]);
			}
			console.log(valuation);
			$("#valuationForm input,#valuationForm  select").attr('disabled','disabled');	
		});
	},2000);*/
	
</script>
<?php }?>