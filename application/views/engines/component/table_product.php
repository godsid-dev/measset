
<div class="panel panel-default">
	<div class="panel-heading">
		Striped Rows
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
				<div class="row">
					<div class="col-lg-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Title</th>
								<th>Type</th>
								<th>Location</th>
								<th>Seller</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($products as $product) { ?>
							<tr>
								<td><a href="<?php echo engines_url('/assessor/detail/'.$product['_id']) ?>" title="รายละเอียด"><?php echo $product['title']; ?></a></td>
								<td><?php echo $product['type']; ?></td>
								<td>
									<?php echo $product['province'],'/',$product['district']?>
								</td>
								<td>
								<?php 
									if($product['seller']['memberType']=='individual'){
										echo $product['seller']['firstname'],' ',$product['seller']['lastname'];	
									}else{
										echo $product['seller']['company_name'];
									}
								?>	
								</td>
								
								<td>
									<?php 
										echo $product['status'];
									?>
								</td>
								<td>
									<a class="btn btn-primary btn-circle" title="" href="<?php echo engines_url('/assessor/edit/'.$product['_id']); ?>">
									<i class="fa fa-edit"></i>
									</a>
								</td>
							</tr>
							<?php
								}
							?>
						</tbody>
					</table>
					</div>
				</div>
				<?php echo $paging; ?>
			</div>
		</div>
		<!-- /.table-responsive -->
		
	</div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->
