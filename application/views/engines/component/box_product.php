<div class="panel panel-primary">
	<div class="panel-heading">
		<i class="fa fa-user fa-fw"></i> สินทรัพย์ <?php echo "เลขที่ : ",$_id?>
	</div>
	<div class="panel-body">
	<form role="form">
		<fieldset disabled="">
		<div class="form-group">
			<label for="txtTitle">ชื่อสินทรัพย์</label>
			<input id="txtTitle" class="form-control" type="text" value="<?php echo $title; ?>" >
		</div>
		<div class="form-group">
			<label for="disabledSelect">ประเภทสินทรัพย์</label>
			<select id="disabledSelect" class="form-control">
				<option><?php echo $type; ?></option>
			</select>
		</div>
		<div class="form-group">
			<label>โฉนดเลขที่</label>
			<input type="text" class="form-control" name="title_deed_number" value="<?php echo $title_deed_number; ?>">
		</div>
		<div class="form-group">
			<label for="tstSheet">หมายเลขระวา</label>
			<input id="tstSheet" class="form-control" type="text" value="<?php echo $sheet; ?>" >
		</div>
		<div class="form-group">
			<label for="txtParchel">หมายเลขที่ดิน</label>
			<input id="txtParchel" class="form-control" type="text" value="<?php echo $parcel_no; ?>" >
		</div>
		<div class="form-group">
			<label for="txtParchel">ขนาด</label>
			<?php if(isset($specified['rai'])){?>
				<input id="	" class="form-control" type="text" value="<?php echo $specified['rai'],' ไร่ ',$specified['ngan'],' งาน ',$specified['wah'],' วา '?>" />
			<?php }else{ ?>
				<input id="txtParchel" class="form-control" type="text" value="<?php echo isset($specified['meter'])?$specified['meter']:0; ?>" />
			<?php }?>
		</div>
		<div class="form-group">
			<label for="txtParchel">Address</label>
			<textarea class="form-control" rows="3"><?php echo $province,"\n", $district; ?></textarea>
		</div>
		</fieldset>
		<div class="form-group">
			<label for="txtParchel">โฉนดที่ดิน</label><br/>
			<a href="<?php echo static_url($title_deed_image)?>" title="ดูรูปขนาดใหญื" target="_bank" ><img src="<?php echo static_url($title_deed_image)?>" width="100" /></a>
		</div>
		<div class="form-group">
			<label for="txtLocation">พิกัด (คลิกเพื่อดูแผนที่)</label>
			<br/>
			<a href="https://www.google.co.th/maps/place/@<?php echo str_replace(' ','',implode(',',$location))?>,17z" title="ดูรูปขนาดใหญื" target="_bank" ><?php echo implode(',',$location); ?></a>
		</div>
		</form>


		<a href="<?php echo engines_url('/assessor/edit/'.$_id)?>" class="btn btn-success" >เพิ่มข้อมูลประเมิน</a>
	</div>
</div>