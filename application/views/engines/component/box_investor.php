<div class="panel panel-red">
	<div class="panel-heading">
		<i class="fa fa-user fa-fw"></i> นักลงทุน
	</div>
	<div class="panel-body">
		<?php
			if(isset($result['investor'])){
				foreach ($result['investor'] as $ivalue) {
					$investor=$this->mMember->get_where(array('_id'=>$ivalue));
					//var_dump($investor);
					//echo"<br/>";
					//var_dump(isset($investor[0]));

					if(isset($investor[0])){
						$tmpInvestor=$investor[0];

		?>
		<address>
			<ul>
				<li> 
					<strong>ชื่อนักลงทุน : </strong>
					<?php 
						echo $tmpInvestor['default']['title'],$tmpInvestor['default']['firstname']," ",$tmpInvestor['default']['lastname'];
					?>
				</li>
				<li> 
					<strong>Tel. : </strong><?php echo $tmpInvestor['default']['telephone']; ?>
				</li>
				<li> 
					<strong>Email. : </strong><?php echo $tmpInvestor['email']; ?>
				</li>
				<li> 
					<strong>เงินลงทุน : </strong><?php echo $tmpInvestor['investor']['investment']['min'],"-",$tmpInvestor['investor']['investment']['max']; ?>
				</li>
				<li> 
					<strong>สนใจลงทุนกับ : </strong>
						<?php 
							if(is_array($tmpInvestor['investor']['investment_type'])){
								echo implode(', ', $tmpInvestor['investor']['investment_type']);
							}else{
								echo $tmpInvestor['investor']['investment_type'];
							}
						?>
				</li>
			</ul>
		</address>
		<?php								
				}else{
					echo"ไม่พบข้อมูลของนักลงทุน รหัส : ", $ivalue, "<br/>";
				}



				}
			}else{
				echo"ยังไม่มีนักลงทุนสนใจ";
			}
		?>
	</div>
</div>