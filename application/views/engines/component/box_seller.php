<div class="panel panel-green">
	<div class="panel-heading">
		<i class="fa fa-user fa-fw"></i> ข้อมูลผู้ขาย
	</div>
	<div class="panel-body">
		<?php 
			if(!empty($seller)){
		?>
		<strong>
			<?php 
				echo $seller['title'],$seller['firstname']," ",$seller['lastname'];
				//echo "[ ",$seller['id_card']," ]";
			?>
		</strong>
		<address>
			<abbr title="Phone"> Tel. </abbr> <?php echo $seller['telephone'] ?>
		</address>
		<?php
			}else{
				echo"ไม่มีข้อมูลผู้ขาย!";
			}
		?>
	</div>
</div>