<!-- Header -->
<header id="header">
    <div class="container">
        <div itemscope itemtype="http://schema.org/Organization" class="logo">
            <a href="<?php echo base_url('/')?>" itemprop="url">Q Asset</a>
            <img src=<?php echo assets_url('/themes/default/img/logo.png'); ?> itemprop="logo" alt="Q Asset">
        </div>
        <!-- menu header -->
        <?php echo $this->load->get_section('navigation');?>
        <!-- /menu header -->
    </div>
</header>
<!-- /Header -->