<nav id="navigation"  class="top-nav">
    <ul id="nav-drop">
    	<li><a href="#sec-register"  title="ขายฝาก">ขายฝาก</a></li>
    	<li><a href="#sec-sell"  title="ผู้ขายฝาก">ผู้ขายฝาก</a></li>
        <li><a href="#sec-invest"  title="นักลงทุน">นักลงทุน</a></li>
        <li><a href="#sec-contact"  title="บริษัท">บริษัท</a></li>
        <li><a href="<?php echo base_url('/question')?>" title="คำถามที่พบบ่อย">คำถามที่พบบ่อย</a></li>
        <?php  if($nav_profile = $this->load->get_section('nav_profile')){
                    echo $nav_profile;
                }else{ ?>
        <li><a href="<?php echo base_url('/login')?>" title="เข้าสู่ระบบ">เข้าสู่ระบบ</a></li>
        <?php   
                }
            
        ?>
        
        
    </ul>
</nav>
<span class="btn-nv-m"><a title="Expand" href="javascript:;" class="b-ex">Expand Nav</a></span>