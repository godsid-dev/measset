<li>
	<a id="btn-profile" href="javascript:;" title="Username"><img src="<?php echo assets_url("themes/default/img/thumb/user.png")?>" alt="Username"> <i class="fa-angle-down"></i></a>
    <ul class="pop-member" style="overflow: hidden; display: none;">
    	<li><a href="<?php echo base_url('/my/profile/edit')?>" title="แก้ไขข้อมูลส่วนตัว"><i class="fa-user"></i> แก้ไขข้อมูลส่วนตัว</a></li>
        <li><a href="<?php echo base_url('/my/profile/message');?>" title="กล่องข้อความ"><i class="fa-envelope"></i> กล่องข้อความ <?php if($profile['message']){ ?><em><?php echo $profile['message']?></em><?php }?></a></li>
        <li><a href="<?php echo base_url('/my/profile/notification');?>" title="การแจ้งเตือน"><i class="fa-bell"></i> การแจ้งเตือน <?php if($profile['notification']){ ?><em><?php echo $profile['notification']?></em><?php }?></a></li>
        <li><a href="<?php echo base_url('/logout');?>" title="ออกจากระบบ"><i class="fa-sign-out"></i> ออกจากระบบ</a></li>
    </ul>
</li>