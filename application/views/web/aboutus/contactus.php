        <li>
        	<h2 class="h-acc active">CONTACT US<i class="fa-chevron-down"></i></h2>
            <div class="pane txt-c" style="display:block">
                <div class="img-map img-resp">
                    <a class="lb-fancy fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14205.706155154905!2d100.5270171665247!3d13.907433286464903!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x36af40a75ccce9b0!2z4Lia4Lij4Li04Lip4Lix4LiX4Liq4Liy4Lih4Liy4Lij4LiW4Lih4Lix4Lil4LiV4Li04Lih4Li14LmA4LiU4Li14LiiIOC4iOC4s-C4geC4seC4lA!5e0!3m2!1sen!2sth!4v1469434001826">
                    <img src="<?php echo base_url('assets/themes/default/img/maps.jpg'); ?>" alt="Map"></a>
                    <address class="_self-mt20 txt-l">
                        <b>บริษัท คิว แอสเสท จำกัด (มหาชน)</b>
                        <big>
                            Software Park 26 Fl., Chaengwattana Rd., Klong Gluar, Pak-Kred,<br>
                            Nonthaburi 11120 Thailand Tel: 
                        </big>
            			<p class="call">
                            <a href="tel:025026000"><i class="fa-phone"></i> 02-502-6000</a> 
                        </p>
                    </address>
                </div>
                <div class="row _chd-cl-xs-12-sm-06">
                    <div class="_chd-cl-xs-12-mb10">
                        <input type="text" class="txt-box" id="yourname" name="yourname" placeholder="ชื่อ - นามสกุล">
                        <input type="text" class="txt-box" id="your-email" name="your-email" placeholder="E-mail">
                        <input type="text" class="txt-box" id="your-phone" name="your-phone" placeholder="โทรศัพท์">
                    </div>
                    <div class="_chd-cl-xs-12">
                        <textarea class="txt-area" id="msg-contact" rows="4" placeholder="รายละเอียด"></textarea>
                    </div>
                    <div class="_self-cl-xs-12-sm-12-mt15 txt-c">
                        <button type="submit" id="sedn-contact" class="ui-btn-green">SEND</button>
                    </div>
                </div>                
            </div>
        </li>        
    </ul>