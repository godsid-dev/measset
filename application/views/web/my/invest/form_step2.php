<!-- box -->
<div class="bx-model">
    <h3 class="hd-box h-acc">เพิ่มข้อมูลลงทุนใหม่ <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
    	<!-- step -->
        <div class="wp-step">
        	<span class="step1 active">
            	<a href="#" title="Step1">
                <b>Step 1</b>
                <small>ข้อมูลทั่วไป</small>
                </a>
            </span>
            <span class="step3 active">
            	<a href="#" title="Step3">
            	<b>Step 2</b>
                <small>ข้อมูลการลงทุน</small>
                </a>
            </span>
        </div>
        <!-- /step -->
        <div class="z-post">
            <form id="sell-step1" name="sell" method="post" enctype="multipart/form-data" action="<?php echo base_url('/my/invest/formsave')?>">
                <input type="hidden" name="step1" value="<?php echo $form['step1']?>" />
            <fieldset>
            	<legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                <div class="contentTab">
                	<!-- บุคคลธรรมดา -->
                	<div id="user1" class="bx-tab">
                    	
                    <div>
                        <label for="budget">จำนวนเงินที่ต้องการลงทุน <small>(ระบุเงินลงทุนสูงสุด)</small></label>
                        <input id="budget" class="txt-box money" type="text"  name="budget" required="" />
                        
                    </div>
                    
                    <div>
                        <label for="contract">ระยะเวลาที่จะทำสัญญา <small>(เดือน)</small></label>
                        <span class="cv-select" for="contract">
                        <select name="contract" id="contract" required="" pattern="[0-9]+" class="select-box">
                            <option >- เลือกระยะเวลา -</option>
                            <option value="3"> 3  เดือน</option>
                            <option value="6"> 6  เดือน</option>
                            <option value="12">12 เดือน (1 ปี)</option>
                            <option value="18">18 เดือน (1.5 ปี)</option>
                            <option value="24">24 เดือน (2 ปี)</option>
                            <option value="30">30 เดือน (2.5 ปี)</option>
                            <option value="36">36 เดือน (3 ปี)</option>
                            <option value="42">42 เดือน (3.5 ปี)</option>
                            <option value="48">48 เดือน (4 ปี)</option>
                            <option value="54">54 เดือน (4.5 ปี)</option>
                            <option value="60">60 เดือน (5 ปี)</option>
                        </select>
                        </span>
                    </div>
                    
                    <div>
                        <label for="interested">ประเภทอสังหาริมทรัพย์ที่ต้องการลงทุน</label><small>(สามารถเลือกได้หลายประเภท)</small>
                        <span class="cv-select" for="interested">
                        <select id="interested" class="select-box" name="interested[]" multiple="" required="">
                            <option value="ที่ดินเปล่า">ที่ดินเปล่า</option>
                            <option value="ที่การเกษตร">ที่การเกษตร</option>
                            <option value="คอนโดมิเนียม/อพาร์ทเม้นท์">คอนโดมิเนียม/อพาร์ทเม้นท์</option>
                            <option value="บ้านเดี่ยว">บ้านเดี่ยว</option>
                            <option value="ทาวน์เฮ้าส์/ทาวน์โฮม">ทาวน์เฮ้าส์/ ทาวน์โฮม</option>
                            <option value="ตึกแถว/อาคารพาณิชย์">ตึกแถว/ อาคารพาณิชย์</option>
                            <option value="โรงแรม/รีสอร์ท/โรงงาน ">โรงแรม/ รีสอร์ท/ โรงงาน</option>
                        </select>
                        </span>
                    </div>                        
                    </div>
                </div>
                
                <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
                    <input type="submit" value="ยืนยันข้อมูล" name="btn-step" class="ui-btn-yellow-post" id="btn-next-step">
                </div>
            </fieldset>
            </form>
        </div>
        
    </div>
</div>
<!-- /box -->