<div class="bx-model">
	<h3 class="hd-box h-acc">แจ้งชำระเงินประกันการประมูล <i class="fa-angle-down"></i></h3>
	<div class="bx-body pane">
		<div class="z-post">
			<fieldset>
                	<legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                    <div class="contentTab">
						<form id="editprofile" name="editprofile" method="post" action="<?php echo site_url('/my/invest/confirmpayment/submit')?>" enctype="multipart/form-data">
							<div  class="bx-tab">
								
								    <div class="row">
								    	<p class="_self-cl-xs-12-sm-07-md-08">
									        <label for="bank">ชื่อธนาคาร</label>
									        <select name="bank" id="bank" class="select-box" required="">
									            <option value="">- เลือกชื่อธนาคาร -</option>
									            <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
									            <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
									            <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
									            <option value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
									            <option value="ธนาคารซีไอเอ็มบีไทย">ธนาคารซีไอเอ็มบีไทย</option>
									            <option value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
									            <option value="ธนาคารทิสโก้">ธนาคารทิสโก้</option>
									            <option value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
									            <option value="ธนาคารธนชาต">ธนาคารธนชาต</option>
									            <option value="ธนาคารยูโอบี">ธนาคารยูโอบี</option>
									            <option value="ธนาคารแลนด์แอนด์เฮาส์">ธนาคารแลนด์แอนด์เฮาส์</option>
									            <option value="ธนาคารสแตนดาร์ดชาร์เตอร์ด">ธนาคารสแตนดาร์ดชาร์เตอร์ด</option>
									            <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
									            <option value="ธนาคารออมสิน">ธนาคารออมสิน</option>
									            <option value="ธนาคารอาคารสงเคราะห์">ธนาคารอาคารสงเคราะห์</option>
									            <option value="ธนาคารอิสลามแห่งประเทศไทย">ธนาคารอิสลามแห่งประเทศไทย</option>
									            <option value="ธนาคารไอซีบีซี">ธนาคารไอซีบีซี</option>
									            <option value="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร">ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร</option>
									            <option value="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย">ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย</option>
									            <option value="ธนาคารไทยเครดิตเพื่อรายย่อย">ธนาคารไทยเครดิตเพื่อรายย่อย</option>
									        </select>
								        </p>
								    </div>
							    	<div class="row">
							            <p class="_self-cl-xs-12-sm-07-md-08">
								            <label for="transfer_id">เลขที่บัญชี</label>
								            <input id="transfer_id" class="txt-box" required="" type="text" name="transfer_id"  >
							            </p>
							        </div>
							        <div class="row">
							            <p class="_self-cl-xs-12-sm-07-md-08">
								            <label for="transfer_amount">จำนวนเงิน</label>
								            <input id="transfer_amount" class="txt-box" required="" type="text" name="transfer_amount"  >
							            </p>
							        </div>
							        <div class="row">
							            <p class="_self-cl-xs-12-sm-07-md-08">
								            <label for="transfer_date">วันที่และเวลา</label>
								            <input id="transfer_date" class="txt-box datepicker hasDatepicker" required="" type="text" name="transfer_date"  >
							            </p>
							        </div>
							    </div>
							    <div class="_self-mt30">
							         <label for="transfer_document">อัพโหลด เอกสารการโอนเงิน</label>
							        <div class="row">
							            <p class="_self-cl-xs-12-confirm_payment_document-07-md-08-pr0">
							                <input id="transfer_document" class="txt-box" type="file" name="transfer_document" required="">
							            </p>
							        </div>
							    </div>

							   <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
							        <input type="submit" value="บันทึกข้อมูล" class="ui-btn-tsmall-red-edit" id="btn-edit">
							    </div>
							</div>
						</form>
					</div>
            </fieldset>
		</div>
	</div>
</div>
<script>
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        endDate: "1d",
    });
</script>