<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-body">
				<fieldset>
					<p>
						ระบบได้บันทึกข้อมูลการลงทุนแล้ว และกำลังอยู่ในขั้นตอนตรวจสอบข้อมูล
					</p>

					<a href="<?php echo site_url('/invest')?>" class="ui-btn-tsmall-red-edit">ตกลง</a>
				</fieldset>
			</div>
		</div>
	</div>
</div>