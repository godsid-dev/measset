<!-- box -->
    <div class="bx-model">
        <h3 class="hd-box h-acc">เพิ่มข้อมูลลงทุนใหม่ <i class="fa-angle-down"></i></h3>
        <div class="bx-body pane">
            <!-- step -->
            <div class="wp-step">
                <span class="step1 active">
                    <a href="#" title="Step1">
                    <b>Step 1</b>
                    <small>ข้อมูลทั่วไป</small>
                    </a>
                </span>
                <span class="step3">
                    <a href="#" title="Step3">
                    <b>Step 2</b>
                    <small>ข้อมูลการลงทุน</small>
                    </a>
                </span>
            </div>
            <!-- /step -->
            <div class="z-post">
                <fieldset>
                    <legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                    <ul class="idTabs _chd-cl-xs-06">
                        <?php if( $this->load->get_section('form_individual')){ ?>
                        <li><a href="#individual" title="บุคคลธรรมดา" data="individual" onclick="($('#member_type').val($(this).attr('data')))" class="selected"><i class="fa-check"></i> บุคคลธรรมดา</a></li>
                        <?php }?>
                        
                        <?php if($this->load->get_section('form_corporation')){ ?>
                        <li><a href="#corporate" title="นิติบุคคล" data="corporation" onclick="($('#member_type').val($(this).attr('data')))" class=""><i class="fa-check"></i> นิติบุคคล</a></li>
                        <?php }?>
                    </ul>
                    
                    <div class="contentTab">
                        <?php echo $this->load->get_section('form_individual')?>

                        <?php echo $this->load->get_section('form_corporation')?>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <!-- /box -->
<script>
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        endDate: "1d",
    });
</script>