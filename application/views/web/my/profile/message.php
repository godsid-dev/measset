<!-- box -->
<div class="bx-model">
    <h3 class="hd-box h-acc">กล่องข้อความ <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
        <div class="mail-box full">
            <ul>
            	<?php foreach($messages as $message){ ?>
            	<li <?php if(!$message['readed']){?>class="hl"<?php }?>>
                	<a href="<?php echo $message['link']?>" title="">
                    <div class="wp">
                    	<i class="fa-circle"></i>
                    	<b><?php echo $message['title']?></b> <span><?php echo dateThai("d M y H:i",strtotime($message['create_date']))?></span>
                    	<p><?php echo $message['title']?></p>
                    </div>
                     </a>
                    <!--
                    <a href="#" title="reply"><i class="fa-reply"></i></a>
                    <a href="#" title="delete"><i class="fa-close"></i></a>
                    -->
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<!-- /box -->
<?php echo $this->load->get_section('pageing');?>