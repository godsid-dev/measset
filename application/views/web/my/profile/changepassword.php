
<!-- box -->
    <div class="bx-model">
        <h3 class="hd-box h-acc"> แก้ไขรหัสผ่าน <i class="fa-angle-down"></i></h3>
        <div class="bx-body pane">

            <div class="z-post">
                <form id="changepassword" name="changepassword" method="post" action="<?php echo base_url('/my/profile/submitChangePassword')?>">
                    <?php if(isset($error)){ ?><p><?php echo $error?></p><?php }?>
                <fieldset>
                	<legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                    <div class="contentTab">
                    	<!-- บุคคลธรรมดา -->
                    	<div id="user1" class="bx-tab">
                         
                            <div>
                                <label for="lastpassword">รหัสผ่านเดิม</label>
                                <input id="lastpassword" class="txt-box form-control" required="" minlength="5" maxlength="32" type="password" name="lastpassword" value="" />
                            </div>
                            <div>
                                <label for="password">รหัสผ่านใหม่</label>
                                <input id="password" class="txt-box form-control" required="" minlength="5" maxlength="32" type="password" name="password" value="" onchange="passwordValidate()" />
                            </div>
                            <div>
                                <label for="repassword">ยืนยันรหัสผ่านใหม่</label>
                                <input id="repassword" class="txt-box form-control" required="" minlength="5" maxlength="32" type="password" name="repassword" value="" onchange="passwordValidate()" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
                        <input type="submit" value="แก้ไข้ข้อมูลรหัสผ่าน" name="profile_edit" class="ui-btn-tsmall-red-edit" id="btn-edit">
                    </div>
                </fieldset>
                </form>
                <script type="text/javascript">
                    function passwordValidate(){
                        if($('#password').val().trim() != $('#repassword').val().trim()){
                            $('#repassword').get(0).setCustomValidity("ยืนยันรหัสผ่านใหม่ ไม่ถูกต้อง");
                        }else{
                            $('#repassword').get(0).setCustomValidity("");
                        }
                    }
                </script>
            </div>
            
        </div>
    </div>
    <!-- /box -->