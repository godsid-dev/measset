
<!-- box -->
    <div class="bx-model">
        <h3 class="hd-box h-acc"> แก้ไขข้อมูลส่วนตัว <i class="fa-angle-down"></i></h3>
        <div class="bx-body pane">

            <div class="z-post">

                <fieldset>
                	<legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                    <ul class="idTabs _chd-cl-xs-06">
                        <?php if( $this->load->get_section('form_individual')){ ?>
                        <li><a href="#individual" title="บุคคลธรรมดา" data="individual" onclick="($('#member_type').val($(this).attr('data')))" class="selected"><i class="fa-check"></i> บุคคลธรรมดา</a></li>
                        <?php }?>
                        
                        <?php if($this->load->get_section('form_corporation')){ ?>
                        <li><a href="#corporate" title="นิติบุคคล" data="corporation" onclick="($('#member_type').val($(this).attr('data')))" class=""><i class="fa-check"></i> นิติบุคคล</a></li>
                        <?php }?>
                    </ul>
                    
                    <div class="contentTab">
                        <?php echo $this->load->get_section('form_individual')?>

                        <?php echo $this->load->get_section('form_corporation')?>
                    </div>
                    <!-- Corporate --> 
                    
                </fieldset>
            </div>
            
        </div>
    </div>
    <!-- /box -->
<script type="text/javascript">

    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        autoclose:true,
        language: 'th'
    });
    $('.fancybox').fancybox();
</script>    