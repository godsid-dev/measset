<div id="toc" class="container page-member">
	<section id="sec-profile" class="row _chd-cl-xs-12-sm-08-lg-09-pd0">
		<?php echo $this->load->get_section('sec_sidebar');?>
		<?php echo $this->load->js(assets_url('/themes/default/js/jquery.idTabs.min.js'));?>
		<div class="z-profile">
        	<h2>
        		<div class="breadcrumb"><a href="<?php echo base_url('/my/profile/dashboard')?>" title="Home">Home</a> &gt; <?php echo isset($breadcrumb)?$breadcrumb:"";?></div>
            	<div class="add-post">
                    <?php if(!isset($profile['investor']) ){ ?>
                	<span>
	                	<a class="ui-btn-gray" href="<?php echo base_url('/my/invest/form')?>" title="ลงทุนใหม่">
	                		<i class="fa-plus"></i> ลงทุนใหม่
	                	</a>
                	</span>
                    <?php }?>
                	<span>
	                	<a class="ui-btn-green" href="<?php echo base_url('/my/sell/form')?>" title="ขายฝากใหม่">
	                		<i class="fa-plus"></i> ขายฝากใหม่
	                	</a>
                	</span>
                </div>
            </h2>
            <div class="inner sec-acc">
                <?php echo $this->load->get_section('dashboard');?>
                <?php echo $this->load->get_section('edit');?>
                <?php echo $this->load->get_section('message');?>
                <?php echo $this->load->get_section('notification');?>
            	
                <?php echo $this->load->get_section('sell');?>
            	<?php echo $this->load->get_section('invest');?>
                <?php echo $this->load->get_section('form');?>
                <?php echo $this->load->get_section('form_step2');?>
                <?php echo $this->load->get_section('form_step3');?>
            	<?php echo $this->load->get_section('history');?>

                <?php echo $this->load->get_section('invest_thankyou');?>
                <?php echo $this->load->get_section('confirm_payment_form');?>
                <?php echo $this->load->get_section('confirm_payment_thankyou');?>

                <?php echo $this->load->get_section('sec_changepassword');?>
                
            </div>
        </div>
	</section>
</div>