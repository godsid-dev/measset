<div class="bx-model">
    <h3 class="hd-box h-acc">ข้อมูลเจ้าของที่ตามโฉนด <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
        <!-- step -->
        <div class="wp-step">
            <span class="step1 active">
                <a href="#" title="Step1">
                <b>Step 1</b>
                <small>ข้อมูลทั่วไป</small>
                </a>
            </span>
            <span class="step2">
                <a href="#" title="Step2">
                <b>Step 2</b>
                <small>แผนที่</small>
                </a>
            </span>
            <span class="step4">
                <a href="#" title="Step4">
                <b>Step 3</b>
                <small>รูปภาพ</small>
                </a>
            </span>
            <span class="step3">
                <a href="#" title="Step3">
                <b>Step 4</b>
                <small>ยอมรับเงื่อนไข</small>
                </a>
            </span>
        </div>
        <!-- /step -->
        <div class="z-post">
            
                <fieldset>
                    <legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                    <ul class="idTabs _chd-cl-xs-06">
                        <?php if($this->load->get_section('form_individual')){ ?>
                        <li><a href="#individual" title="บุคคลธรรมดา" data="individual" ><i class="fa-check"></i> บุคคลธรรมดา</a></li>
                        <?php }?>
                        <?php if($this->load->get_section('form_corporation')){ ?>
                        <li><a href="#corporation" title="นิติบุคคล" data="corporation" ><i class="fa-check"></i> นิติบุคคล</a></li>
                        <?php }?>
                    </ul>
                    <div class="contentTab">
                        <!-- บุคคลธรรมดา -->
                            <?php echo $this->load->get_section('form_individual')?>
                        <!-- นิติบุคคล -->
                            <?php echo $this->load->get_section('form_corporation')?>
                    </div>
                </fieldset>
            
        </div>        
    </div>
</div>
<script type="text/javascript">


    //$(function () {
    //  $('#sell-step1').parsley().on('field:validated', function() {
        //var ok = $('.parsley-error').length === 0;
        //$('.bs-callout-info').toggleClass('hidden', !ok);
        //$('.bs-callout-warning').toggleClass('hidden', ok);
     // })
     // .on('form:submit', function() {
     //   return false; // Don't submit form for this demo
     // });
    //});

    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        //autoclose:true,
        //language: 'th'
    });
    $('.fancybox').fancybox();
</script>