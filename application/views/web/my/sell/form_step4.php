
<!-- box -->
<div class="bx-model">
    <h3 class="hd-box h-acc">เพิ่มข้อมูลขายฝากใหม่ <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
    	<!-- step -->
        <div class="wp-step">
        	<span class="step1 active">
            	<a href="#" title="Step1">
                <b>Step 1</b>
                <small>ข้อมูลทั่วไป</small>
                </a>
            </span>
            <span class="step2 active">
            	<a href="#" title="Step2">
            	<b>Step 2</b>
                <small>แผนที่</small>
                </a>
            </span>
            <span class="step4 active">
                <a href="#" title="Step4">
                <b>Step 3</b>
                <small>รูปภาพ</small>
                </a>
            </span>
            <span class="step3">
                <a href="#" title="Step3">
                <b>Step 4</b>
                <small>ยอมรับเงื่อนไข</small>
                </a>
            </span>
        </div>
        <!-- /step -->
        <div class="z-post">
            <form id="sell-step1" name="sell" method="post" enctype="multipart/form-data" action="<?php echo base_url('/my/sell/formsave')?>">
            <input type="hidden" name="step1" value="<?php echo $form['step1']?>" />
            <input type="hidden" name="step2" value="<?php echo $form['step2']?>" />
            <fieldset>
            	<legend class="hid">เพิ่มรูปสินทรัพย์</legend>
                <div class="contentTab">
                	<!-- บุคคลธรรมดา -->
                	<div id="user1" class="bx-tab">
                    
                    <!--div>
                        <label for="info-2">รูปภาพ</label>
                        <div class="list-pic row _chd-cl-xs-06-sm-03">
                        	<span><img alt="" src="<?php echo assets_url('/themes/default/img/thumb/photo1_full.jpg')?>"></span>
                            <span><img alt="" src="<?php echo assets_url('/themes/default/img/thumb/photo1_full.jpg')?>"></span>
                            <span><img alt="" src="<?php echo assets_url('/themes/default/img/thumb/photo1_full.jpg')?>"></span>
                            <span><img alt="" src="<?php echo assets_url('/themes/default/img/thumb/photo1_full.jpg')?>"></span>
                        </div>
                    </div-->

                    <div class="_self-mt30">
                        <label for="images">อัพโหลด รูปภาพ (สามารถเลือกได้ 5 ภาพ)</label>
                        <div class="row">
                        	<p class="_self-cl-xs-12-sm-07-md-08-pr0">
                            	<input id="images" class="txt-box" type="file" name="images[]" multiple="" accept="image/*" max="5" required="">
                            </p>
                        </div>
                    </div>

                    <div>
                        <label for="price">จำนวนเงินที่ต้องการ <small>(เป็นจำนวนเงินก่อนหักค่าธรรมเนียมต่างๆ)</small></label>
                        <input id="price" class="txt-box form-control" type="text" name="price" required="">
                        </span>
                    </div>

                    <?php echo $this->load->get_section('contract')?>
                    
                    <div>
                        <label for="interest">จำนวนดอกเบี้ยที่ต้องการ <small>%ต่อปี</small></label>
                        <select name="interest" id="interest" class="select-box" required="">
                            <option value="1">1%</option>
                            <option value="2">2%</option>
                            <option value="3">3%</option>
                            <option value="4">4%</option>
                            <option value="5">5%</option>
                            <option value="6">6%</option>
                            <option value="7">7%</option>
                            <option value="8">8%</option>
                            <option value="9">9%</option>
                            <option value="10">10%</option>
                            <option value="11">11%</option>
                            <option value="12">12%</option>
                            <option value="13">13%</option>
                            <option value="14">14%</option>
                            <option value="15">15%</option>
                        </select>
                        <select name="interest_sub" id="interest_sub" class="select-box" required="">
                            <option value="0">0%</option>
                            <option value="0.25">.25%</option>
                            <option value="0.50">.5%</option>
                            <option value="0.75">.75%</option>
                        </select>
                        </span>
                    </div>

                    <div class="_self-mt30">
                        <label for="estimate_price">คำนวนค่าใช้จ่าย</label>
                        <div class="row">
                            <table class="table" bordercolor="#b5b5b5" style="background:#efefef; border:2px solid #d4d4d4" width="100%" border="1" cellspacing="0" cellpadding="0">
                             <tr bgcolor="#149e97" style="color:#fff">
                                 <th align="center" >รายการ</th>
                                 <th align="center">ค่าใช้จ่าย</th>
                             </tr>
                             <tr>
                                 <td>จำนวนเงิน</td>
                                 <td><span id="table_money"></span></td>
                             </tr>  
                             <tr>
                                 <td>ระยะเวลา</td>
                                 <td><span id="table_period"></span></td>
                             </tr>
                             <tr>
                                 <td>ดอกเบี้ย <span id="table_interest_rate"></span> %</td>
                                 <td><span id="table_interest"></span></td>
                             </tr>   
                             <tr>
                                 <td>หักดอกเบี้ยก่อน 3 เดือน</td>
                                 <td><span id="table_sub_interest"></span></td>
                             </tr>
                             <tr>
                                 <td>ดอกเบี้ยที่ต้องจ่าย เมื่อครบกำหนดไถ่ถอน</td>
                                 <td><span id="table_pay_interest"></span></td>
                             </tr>
                             <tr>
                                 <td>ค่าธรรมเนียม 3%</td>
                                 <td><span id="table_fee"></span></td>
                             </tr>
                             <tr>
                                 <td>ค่าโอนที่ 2%</td>
                                 <td><span id="table_fee_transfer"></span></td>
                             </tr>
                             <tr>
                                 <td>ค่าอากรณ์แสตมป์ 0.5%</td>
                                 <td><span id="table_stamp_duty"></span></td>
                             </tr>
                             <tr>
                                 <td>ภาษีธุรกิจเฉพาะ 3.3%</td>
                                 <td><span id="table_business_tax"></span></td>
                             </tr>
                             <tr>
                                 <td>ภาษีดอกเบี้ยหัก ณ ที่จ่าย (การถือครองที่ 8 ปี)</td>
                                 <td><span id="table_tax_on_interest"></span></td>
                             </tr>
                             <tr>
                                 <td>ค่าธรรมและดอกเบี้ยมรวม</td>
                                 <td><span id="table_sum_fee_interest"></span></td>
                             </tr>
                             <tr>
                                 <td>รับ(จ่าย)เงิน ณ วันที่โอน</td>
                                 <td><span id="table_sum_pay"></span></td>
                             </tr>
                             <tr>
                                 <td>ค่าสินไถ่ เมื่อไถ่ถอนที่ดิน</td>
                                 <td><span id="table_redemption"></span></td>
                             </tr>                                    
                            </table>
                        </div>
                        <p>* ค่าภาษีอาจเปลี่ยนแปลงได้ขึ้นกับราคาประเมินกรมที่ดิน</p>
                        </div>
                    </div>
                </div>
                
                <div class="imageBased _self-cl-xs-12">
                    <input type="checkbox" name="accept1" id="accept1" value required="" /><label for="accept1">ถ้าโพสรายการขายฝากในเวปแล้ว ผู้ขายฝากยกเลิกการขายฝากหลังมีนักลงทุนเสนอbid ผู้ขายฝากต้องรับผิดชอบค่าเสียหายที่เกิดขึ้น โดยชำระค่าธรรมเนียมเป็นจำนวนX%ของมูลค่าที่</label>
                </div>
                <div class="imageBased _self-cl-xs-12">
                    <input type="checkbox" name="accept2" id="accept2" value required="" /><label for="accept2">หลังจากกรอกข้อมูลการขายฝากเสร็จสิ้นบริษัทจะส่งข้อมูล อสังหาริมทรัพย์ของท่านไปยังบริษัทประเมินที่ได้รับการรับรอง โดยกลต.ประเมินราคา ซึ่งมีค่าใช้จ่าย xxx บาท บริษัทประเมินจะติดต่อกลับผู้ขายฝาก ภายใน2วันทำการ โดยผู้ขายฝากต้องจ่ายเงินสดกับบริษัทประเมินโดยตรง</label>
                </div>

                <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
                    <input type="submit" value="ยืนยันข้อมูล" name="btn-step" class="ui-btn-yellow-post" id="btn-next-step">
                </div>
            </fieldset>
            </form>
        </div>
        
    </div>
</div>
<script type="text/javascript">
    function checkEstimatePrice(){
        var estimatePriceUrl = '<?php echo base_url('/') ?>'; 
        $.post(estimatePriceUrl,{type:'',area:2},function(resp){
            $('#estimate_price').val(resp.preice);
        });  
    };
    

    function estimatePrice(){
        money = parseInt($('#contract_money').val());
        interestRate = parseFloat($('#interest').val())+parseFloat($('#interest_sub').val());
        period = parseInt($('#contract_month').val());
        interest = (money/100)*(interestRate/12)*period;
        subInterest = (interest/period)*3;
        payInterest = interest-subInterest;
        fee = money * 0.03;
        feeTransfer = money * 0.02;
        stampDuty = money * 0.005;
        businessTax = money * 0.033;
        sumFeeInterest = fee + feeTransfer + stampDuty + businessTax;
        sumPay = money - subInterest - sumFeeInterest;
        redemption = money + payInterest;

        $('#table_money').text(numberFormat(money));
        $('#table_interest_rate').text(interestRate);
        $('#table_interest').text(numberFormat(interest));
        $('#table_period').text(period + " เดือน");
        $('#table_sub_interest').text(numberFormat(subInterest));
        $('#table_pay_interest').text(numberFormat(payInterest));
        $('#table_fee').text(numberFormat(fee));
        $('#table_fee_transfer').text(numberFormat(feeTransfer));
        $('#table_stamp_duty').text(numberFormat(stampDuty));
        $('#table_business_tax').text(numberFormat(businessTax));
        $('#table_sum_fee_interest').text(numberFormat(sumFeeInterest));
        $('#table_sum_pay').text(numberFormat(sumPay));
        $('#table_redemption').text(numberFormat(redemption));

    }
    function numberFormat(number){
        return number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    function changeType(obj){
        console.log(obj.value);   
        if(obj.value == "คอนโดมิเนียม/อพาร์ทเม้นท์"){
            $("#specified_square small").text("ตารางเมตร");
            $("#specified_square_wah").attr("name","specified_square_meter");
        }else{
            $("#specified_square small").text("ตารางวา");
            $("#specified_square_wah").attr("name","specified_square_wah");
        }
    }

    $('#contract_money').change(estimatePrice);
    $('#contract_month').change(estimatePrice);
    $('#interest').change(estimatePrice);
    $('#interest_sub').change(estimatePrice);
    
</script>
<!-- /box -->