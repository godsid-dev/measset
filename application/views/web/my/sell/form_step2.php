
<!-- box -->
    <div class="bx-model">
        <h3 class="hd-box h-acc">เพิ่มข้อมูลขายฝากใหม่ <i class="fa-angle-down"></i></h3>
        <div class="bx-body pane">
        	<!-- step -->
            <div class="wp-step">
            	<span class="step1 active">
                	<a href="#" title="Step1">
                    <b>Step 1</b>
                    <small>ข้อมูลทั่วไป</small>
                    </a>
                </span>
                <span class="step2 active">
                	<a href="#" title="Step2">
                	<b>Step 2</b>
                    <small>แผนที่</small>
                    </a>
                </span>
                <span class="step4">
                    <a href="#" title="Step4">
                    <b>Step 3</b>
                    <small>รูปภาพ</small>
                    </a>
                </span>
                <span class="step3">
                    <a href="#" title="Step3">
                    <b>Step 4</b>
                    <small>ยอมรับเงื่อนไข</small>
                    </a>
                </span>
            </div>
            <!-- /step -->
            <div class="z-post">
                <form id="sell-step1" name="sell" method="post" enctype="multipart/form-data" action="<?php echo base_url('/my/sell/form3')?>">
                    <input type="hidden" name="step1" value="<?php echo $form['step1']?>" />
                <fieldset>
                	<legend class="hid">รายละเอียดสินทรัพย์</legend>
                    <div class="contentTab">
                    	<!-- บุคคลธรรมดา -->
                    	<div id="user1" class="bx-tab">
                        <div>
                            <label for="title">ชื่อสินทรัพย์</label><small>ตัวอย่าง: บ้านเดี่ยว ย่านสุขุมวิท</small>
                            <input id="title" class="txt-box" type="text" name="title" required="">
                        </div>
                        <div>
                            <label for="title">รายละเอียดสินทรัพย์</label><small>ตัวอย่าง: คอนโดลุมพินี ชั้น28 พื้นที่64.5ตรม. 2ห้องนอน 2ห้องน้ำ 1ห้องรับแขก หน้าห้องหันไปทางทิศใต้ วิวถนนสุขุมวิท </small>
                            <input id="description" class="txt-box" type="text" name="description" required="">
                        </div>
                        
                        <div >
                            <label for="type">ประเภทสินทรัพย์</label>
                            <span class="cv-select" for="category">
                                <select id="type" class="select-box" name="type" required="" onchange="changeType(this)">
                                    <option value="">- เลือกประเภทสินทรัพย์ -</option>
                                    <option value="ที่ดินเปล่า">ที่ดินเปล่า</option>
                                    <option value="ที่การเกษตร">ที่การเกษตร</option>
                                    <option value="คอนโดมิเนียม/อพาร์ทเม้นท์">คอนโดมิเนียม/อพาร์ทเม้นท์</option>
                                    <option value="บ้านเดี่ยว">บ้านเดี่ยว</option>
                                    <option value="ทาวน์เฮ้าส์/ทาวน์โฮม">ทาวน์เฮ้าส์/ ทาวน์โฮม</option>
                                    <option value="ตึกแถว/อาคารพาณิชย์">ตึกแถว/ อาคารพาณิชย์</option>
                                    <option value="โรงแรม/รีสอร์ท/โรงงาน ">โรงแรม/ รีสอร์ท/ โรงงาน</option>
                                </select>
                            </span>
                        </div>
                        <div>
                            <label for="title_deed_number">เลขที่โฉนด</label> <small><a title="คลิกเพื่อดูตัวอย่าง" href="<?php echo assets_url('/themes/default/img/ex_land.png')?>" class="fancybox">ตัวอย่าง (1) <img src="<?php echo assets_url('/themes/default/img/ex_land.png')?>" width="30" /></a></small>
                            <input id="title_deed_number" class="txt-box" type="text" name="title_deed_number" required="">
                        </div>
                        <div>
                            <label for="sheet">หมายเลขระวาง</label> <small><a title="คลิกเพื่อดูตัวอย่าง" href="<?php echo assets_url('/themes/default/img/ex_land.png')?>" class="fancybox">ตัวอย่าง (2) <img src="<?php echo assets_url('/themes/default/img/ex_land.png')?>" width="30" /></a></small>
                            <input id="sheet" class="txt-box" type="text" name="sheet" required="">
                        </div>
                         <div>
                            <label for="parcel_no">เลขที่ดิน</label> <small><a title="คลิกเพื่อดูตัวอย่าง" href="<?php echo assets_url('/themes/default/img/ex_land.png')?>" class="fancybox">ตัวอย่าง (3) <img src="<?php echo assets_url('/themes/default/img/ex_land.png')?>" width="30" /></a></small>
                            <input id="parcel_no" class="txt-box" type="text" name="parcel_no" required="">
                        </div>

                        <div id="specified_square_metric" class="hid">
                            <label for="specified_square_meter">ขนาดพื้นที่</label><small>ตารางเมตร</small>
                            <input id="specified_square_meter" class="txt-box" type="text" name="specified[meter]" required="" />
                        </div>
                        <div id="specified_square_thai">
                            <label for="specified_square_rai">ขนาดพื้นที่</label><small>ไร่</small>
                            <input id="specified_square_rai" class="txt-box" type="text" name="specified[rai]" required="" />
                            <small>งาน</small>
                            <input id="specified_square_ngan" class="txt-box" type="text" name="specified[ngan]" required="" />
                            <small>ตารางวา</small>
                            <input id="specified_square_wah" class="txt-box" type="text" name="specified[wah]" required="" />
                        </div>
                        <div>
                            <label for="owner_date">วันที่เริ่มถือครอง</label>
                            <input id="owner_date" class="txt-box datepicker" type="text" name="owner_date" required="" placeholder="ปี-เดือน-วัน">
                        </div>
                        
                        <div class="_self-mt30">
                            <label for="title_deed_font_image">อัพโหลด สำเนาโฉนด (ด้านหน้า)</label>
                            <div class="row">
                                <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                                    <input id="title_deed_image" class="txt-box" type="file" name="title_deed_image" required="">
                                </p>
                            </div>
                        </div>
                        <div class="_self-mt30">
                            <label for="title_deed_back_image">อัพโหลด สำเนาโฉนด (ด้านหลัง)</label>
                            <div class="row">
                                <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                                    <input id="title_deed_image" class="txt-box" type="file" name="title_deed_image" required="">
                                </p>
                            </div>
                        </div>
                        <?php echo $this->load->get_section('address')?>
                        <div class="maps _self-cl-xs-12-sm-12-pd0" >
                            <label for="latlng">ตำแหน่งที่ดิน</label>
                            <input id="latlng" class="txt-box" type="text" name="address[location]" readonly="">
                            <p>(<a title="Mark Location" class="lb-fancy fancybox.iframe" href="#">ย้ายหมุดสีแดงเพื่อระบุพิกัด</a>)</p>
                            <div id="map" class="gmap _self-mt10" style="height:300; width:100%;"></div>
                        </div>
                        </div>
                    </div>
                    
                    <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
                        <input type="submit" value="เพิ่มข้อมูล" name="step3" class="ui-btn-yellow-post" id="btn-next-step">
                    </div>
                </fieldset>
                </form>
            </div>
            
        </div>
    </div>

    <script type="text/javascript">
        
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            autoclose:true,
            
        });
        //language: 'th'
        $('.fancybox').fancybox();

        function changeType(obj){
            console.log(obj);
            if(obj.value == "คอนโดมิเนียม/อพาร์ทเม้นท์"){
                $("#specified_square_metric").show();
                $("#specified_square_metric").removeClass('hid');
                $("#specified_square_thai").hide();
                $("#specified_square_metric input").attr('disabled',false);
                $("#specified_square_thai input").attr('disabled','disabled');

            }else{
                $("#specified_square_thai").show();
                $("#specified_square_metric").hide();
                $("#specified_square_metric input").attr('disabled','disabled');
                $("#specified_square_thai input").attr('disabled',false);
            }
        }

    </script>
    <?php echo $this->load->get_section('google_map_js');?>
    <!-- /box -->
