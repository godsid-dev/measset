<!-- box -->
<div class="bx-model">
    <h3 class="hd-box h-acc">รายการขายฝากของฉัน <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
        <div class="list-table">
            	<ol>
                	<li class="th">
                    	<ul>
                        	<li><p class="imageBased"><!--input type="checkbox" id="check0" name="check0" onClick="toggle(this)" --><label for="check0">ยูนิต</label></p></li>
                            <li>ราคา</li>
                            <li>ประกาศเมื่อ</li>
                            <li>สถานะ</li>
                            <li>เครื่องมือ</li>
                        </ul>
                    </li>
                    <?php foreach($products as $product) { ?>
                	<li>
                    	<ul>
                        	<li>
                                <p class="imageBased"><input type="checkbox" id="check1" name="check[<?php echo $product['_id']?>]"><label for="check1">
                                <a href="<?php echo base_url('/my/sell/product/'.$product['_id'])?>" title="" ><img height="40" alt="" src="<?php echo static_url((isset($product['images'])&&count($product['images'])?$product['images'][0]:''))?>"> <?php echo $product['title']?></a></label></p>
                            </li>
                            <li>฿ <?php echo number_format($product['price']) ?></li>
                            <li><?php echo dateThai("d/m/Y",strtotime($product['create_date']))?></li>
                            <li>
                                <?php echo $product['status'];?>
                            </li>
                            <li><a href="<?php echo base_url('/my/sell/product/edit/'.$product['_id'])?>" title="Edit"><i class="fa-pencil-square"></i></a> <a href="<?php echo base_url('/my/sell/product/delete/'.$product['_id'])?>" title="Delete"><i class="fa-close"></i></a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ol>
            </div>
            
        <!--<div class="loadmore">
            <a title="ดูทั้งหมด" href="#">ดูทั้งหมด</a>
        </div>-->
    </div>
</div>
<!-- /box -->

<?php echo $this->load->get_section('pageing');?>