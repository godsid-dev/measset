

<div class="inner sec-acc row">
  <h3 class="h-acc" style="margin-left: 60px;">ข้อมูลสินทรัพย์</h3>
  <div class="bx-model">
    <div class="table-responsive">
      <table class="table" bordercolor="#b5b5b5" style="background:#efefef; border:2px solid #d4d4d4;margin:auto;max-width: 90%;" width="90%"; border="1"; cellspacing="0" cellpadding="0">
        <thead>
          <tr bgcolor="#149e97">
            <th  align="center">รายการ</th>
            <th  align="center">ข้อมูล</th>
          </tr>
          <?php foreach($product as $key=>$value){
          	if(in_array($key, array('location','images','seller','investor','valuation','specified'))){
          		continue;
          	}
          ?>
          	<tr>
          		<td><?php echo $key?></td>
          		<td><?php echo $value?></td>
          	</tr>
          <?php }?>           
        </thead>
      </table>
    </div>
  </div>
</div>
