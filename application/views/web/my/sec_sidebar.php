<aside class="sl-munu _self-cl-xs-12-sm-04-lg-03">
	<h2>หน้าหลัก <i class="fa-bars"></i></h2>
    <nav class="pop-nav">
    	<h3>ข้อมูลส่วนตัว</h3>
        <ul>
        	<li><a href="<?php echo base_url('/my/profile/edit')?>" title="แก้ไขข้อมูลส่วนตัว"><i class="fa-user"></i> แก้ไขข้อมูลส่วนตัว</a></li>
            <li><a href="<?php echo base_url('/my/profile/message')?>" title="กล่องข้อความ"><i class="fa-envelope"></i> กล่องข้อความ <?php if($profile['message']){ ?><em><?php echo $profile['message']?></em><?php }?></a></li>
        </ul>
        
        <h3>การขายฝาก</h3>
        <ul>
        	<li><a href="<?php echo base_url('/my/sell/form')?>" title="ขายฝากใหม่"><i class="fa-plus"></i> ขายฝากใหม่</a></li>
            <?php
                if(isset($profile['seller'])){
            ?>
            <li><a href="<?php echo base_url('/my/sell')?>" title="รายการขายฝากของฉัน"><i class="fa-pie-chart"></i> รายการขายฝากของฉัน</a></li>
            <li><a href="<?php echo base_url('/my/sell/history')?>" title="ประวัติการขายฝาก"><i class="fa-history"></i> ประวัติการขายฝาก</a></li>
            <?php }?>
        </ul>
        
        <h3>การลงทุน</h3>
        <ul>
            
        	<li><a href="<?php echo base_url('/my/invest/form')?>" title="ลงทุนใหม่"><i class="fa-plus"></i> ลงทุนใหม่</a></li>
            
            <?php if(isset($profile['investor']) ){ ?>
            <li><a href="<?php echo base_url('/my/invest')?>" title="รายการลงทุนของฉัน"><i class="fa-pie-chart"></i> รายการลงทุนของฉัน</a></li>
            <li><a href="<?php echo base_url('/my/invest/history')?>" title="ประวัติการลงทุน"><i class="fa-history"></i> ประวัติการลงทุน</a></li>
            <li><a href="<?php echo base_url('/my/invest/confirmpayment')?>" title="แจ้งชำระเงิน"><i class="fa-history"></i> แจ้งชำระเงิน</a></li>

            <?php }?>
        </ul>
        <div class="bar"><a href="<?php echo base_url('/logout')?>" title="ออกจากระบบ"><i class="fa-sign-out"></i> ออกจากระบบ</a></div>
    </nav>    
</aside>