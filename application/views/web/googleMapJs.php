<script type="text/javascript">
    var map;
    var defaultLocation = {lat: <?php echo isset($lat)?$lat:"13.7251096"?>, lng: <?php echo isset($lng)?$lng:"100.3529085"?>};
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: defaultLocation,
        zoom: <?php echo isset($zoom)?$zoom:"8"?>
      });
      //Auto Complate
      //var autocomplete = new google.maps.places.Autocomplete(input);
      //autocomplete.bindTo('bounds', map);
      // Marker
      var marker ;

      var infoWindow = new google.maps.InfoWindow({map: map});
      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          infoWindow.setPosition(pos);
          infoWindow.setContent('Location found.');ป
          map.setCenter(pos);
          addMarker(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
          addMarker(defaultLocation);
        });
      } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }
    function addMarker(latlng) {
      var marker = new google.maps.Marker({
              position: latlng,
              map: map,
              title: 'My Location',
              draggable:true
      });
      marker.addListener('drag', handleMarkerEvent);
      marker.addListener('dragend', handleMarkerEvent);
    }
    function handleMarkerEvent(event){
      $('#latlng').val(event.latLng.lat()+', '+event.latLng.lng() );
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                      'Error: The Geolocation service failed.' :
                      'Error: Your browser doesn\'t support geolocation.');
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_map_api_key')?>&callback=initMap"
  async defer></script>