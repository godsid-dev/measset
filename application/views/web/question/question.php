  <h1 class="topic"><span class="h-line">คำถาม / คำตอบ</span></h1>
  <div class="accordion qa1">
    <!-- c1 -->
    <h3 class="AC3 current"><i class="fa-comments-o"></i> “จำนอง” กับ  “ขายฝาก”  ต่างกันอย่างไร </h3>
    <div class="pane" style="display:block;"> 
      <div>
        <p>
          ตอบ  “จำนอง”  คือ  สัญญาซึ่งบุคคลหนึ่ง  เรียกว่า  ผู้จำนองเอาทรัพย์สินตราไว้แก่บุคคลอีกคนหนึ่งเรียกว่าผู้รับจำนองเพื่อเป็นประกันการชำระหนี้  โดยไม่ส่งมอบทรัพย์สินนั้นให้แก่ผู้รับจำนอง ส่วน “ขายฝาก”  คือสัญญาซื้อขายซึ่งกรรมสิทธิ์ในทรัพย์สินตกไปยังผู้ซื้อ โดยมีข้อตกลงกันว่าผู้ขายอาจไถ่ทรัพย์นั้นคืนได้ภายในเวลาที่กำหนด  ฉะนั้น  ข้อแตกต่างที่สำคัญของสัญญาจำนองกับสัญญาขายฝากที่ควรทราบจึง  มีดังนี้
        <p>
        <ul>
        	<li>
            1.  “จำนอง”  กรรมสิทธิ์ในทรัพย์สินที่จำนองไม่โอนไปยังผู้รับจำนอง จึงไม่ต้องมีการส่งมอบทรัพย์ที่จำนองให้ผู้รับจำนอง  แต่  “ขายฝาก”  กรรมสิทธิ์ในทรัพย์สินที่ขายฝากโอนไปยังผู้รับซื้อฝาก  จึงต้องมีการส่งมอบทรัพย์สินให้แก่ผู้รับซื้อฝาก
          </li>
          <li>
            2.  จำนองไม่ต้องกำหนดระยะเวลาไถ่ถอน   หากไม่ชำระหนี้   ผู้รับจำนองจะยึดทรัพย์สินที่
            จำนองมาเป็นของตนไม่ได้ในทันทีจะต้องฟ้องบังคับจำนอง   แต่ขายฝากมีกำหนดเวลาไถ่   ถ้าไม่ไถ่ภายใน
            กำหนดเวลาที่ตกลงกันไว้ในสัญญา ผู้ขายฝากหมดสิทธิ์ที่จะไถ่ทรัพย์นั้นคืนมาเป็นของตน หากตกลงจะคืนที่ดินกันภายหลังครบกำหนดแล้ว  ก็ต้องเป็นเรื่องซื้อขายที่ดินไม่ใช่ไถ่ถอนจากการขายฝากอีก 
          </li>
          <li>
            3.  การจดทะเบียนจำนองเสียค่าธรรมเนียมร้อยละ  1  จากวงเงินจำนอง  อย่างสูงไม่เกิน  200,000 บาท  ถ้าเป็นการจำนองสำหรับการให้สินเชื่อเพื่อการเกษตรของสถาบันการเงินที่ รัฐมนตรีกำหนด
          เสียค่าธรรมเนียมร้อยละ  0.5  ของวงเงินจำนอง  อย่างสูงไม่เกิน  100,000  บาท  และไม่เสียภาษีอากร  แต่สำหรับการจดทะเบียนขายฝากเสียค่าธรรมเนียมร้อยละ  2  จากราคาประเมินทุนทรัพย์  และเสียภาษีอากรตามประมวลรัษฎากรด้วย
          </li>
        </ul>                          
      </div>
    </div>
    <!-- /c1 --> 
    <!-- c2 -->
    <h3 class="AC3"><i class="fa-comments-o"></i> ผู้ขายฝากจะได้วงเงินเท่าไร่?</h3>
    <div class="pane"> 
      <div>
        เราให้วงเงินสูงสุด 60% ของราคาประเมินของกรมที่ดิน และราคาซื้อ-ขายในปัจจุบัน<br>
        อย่างไรก็ตามวงเงินที่จะได้อนุมัตินั้นขึ้นอยู่กับสภาพหลักทรัพย์และทำเลที่ตั้งเป็นหลัก
      </div>
    </div>
    <!-- /c2 --> 
    <!-- c3 -->
    <h3 class="AC3"><i class="fa-comments-o"></i> ดอกเบี้ยขายฝากเท่าไร่?</h3>
    <div class="pane">
      <div>
        ดอกเบี้ยอยู่ที่ <big>15% ต่อปี</big> หรือ <big>1.25% ต่อเดือน</big> หรือแล้วแต่ตกลง
      </div>
    </div>
    <!-- /c3 --> 
    <!-- c4 -->
    <h3 class="AC3"><i class="fa-comments-o"></i> มีนโยบายหักดอกเบี้ยล่วงหน้าอย่างไร?</h3>
    <div class="pane"> 
      <div>
        ทางเรามีนโยบายขอหักดอกเบี้ยล่วงหน้า 3 เดือน หรือแล้วแต่ตกลง
      </div>
    </div> 
    <!-- /c4 -->
  </div>