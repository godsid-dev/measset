<div id="toc" class="container page-member">
	<section id="sec-profile" class="row _chd-cl-xs-12-sm-08-lg-09-pd0">
		<?php echo $this->load->get_section('sell');?>
		<?php echo $this->load->js('assets/themes/default/js/jquery.idTabs.min.js');?>
		<div class="z-profile">
        	<h2>
        		<div class="breadcrumb"><a href="profile.php" title="Home">Home</a> &gt; <?php echo $breadcrumb;?></div>
            	<div class="add-post">
                	<span>
	                	<a class="ui-btn-gray" href="/sell/invest_post" title="ลงทุนใหม่">
	                		<i class="fa-plus"></i> ลงทุนใหม่
	                	</a>
                	</span>
                	<span>
	                	<a class="ui-btn-green" href="/sell/sell_post" title="ขายฝากใหม่">
	                		<i class="fa-plus"></i> ขายฝากใหม่
	                	</a>
                	</span>
                </div>
            </h2>
            <div class="inner sec-acc">
                <?php echo $this->load->get_section('personal');?>
                <?php echo $this->load->get_section('message');?>
                <?php echo $this->load->get_section('notification');?>
            	<?php echo $this->load->get_section('sell_post');?>
            	<?php echo $this->load->get_section('sell_my');?>
            	<?php echo $this->load->get_section('sell_history');?>
            	<?php echo $this->load->get_section('invest_post');?>
            	<?php echo $this->load->get_section('invest_my');?>
            	<?php echo $this->load->get_section('invest_history');?>
            </div>
        </div>
	</section>
</div>
