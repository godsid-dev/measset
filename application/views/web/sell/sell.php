<aside class="sl-munu _self-cl-xs-12-sm-04-lg-03">
	<h2>หน้าหลัก <i class="fa-bars"></i></h2>
    <nav class="pop-nav">
    	<h3>ข้อมูลส่วนตัว</h3>
        <ul>
        	<li><a href="/sell/personal" title="แก้ไขข้อมูลส่วนตัว"><i class="fa-user"></i> แก้ไขข้อมูลส่วนตัว</a></li>
            <li><a href="/sell/message" title="กล่องข้อความ"><i class="fa-envelope"></i> กล่องข้อความ <em>2</em></a></li>
            <li><a href="/sell/notification" title="การแจ้งเตือน"><i class="fa-bell"></i> การแจ้งเตือน <em>2</em></a></li>
        </ul>
        
        <h3>การขายฝาก</h3>
        <ul>
        	<li><a href="/sell/sell_post" title="ขายฝากใหม่"><i class="fa-plus"></i> ขายฝากใหม่</a></li>
            <li><a href="/sell/sell_my" title="รายการขายฝากของฉัน"><i class="fa-pie-chart"></i> รายการขายฝากของฉัน</a></li>
            <li><a href="/sell/sell_history" title="ประวัติการขายฝาก"><i class="fa-history"></i> ประวัติการขายฝาก</a></li>
        </ul>
        
        <h3>การลงทุน</h3>
        <ul>
        	<li><a href="/sell/invest_post" title="ลงทุนใหม่"><i class="fa-plus"></i> ลงทุนใหม่</a></li>
            <li><a href="/sell/invest_my" title="รายการลงทุนของฉัน"><i class="fa-pie-chart"></i> รายการลงทุนของฉัน</a></li>
            <li><a href="/sell/invest_history" title="ประวัติการลงทุน"><i class="fa-history"></i> ประวัติการลงทุน</a></li>
        </ul>
        <div class="bar"><a href="/home" title="ออกจากระบบ"><i class="fa-sign-out"></i> ออกจากระบบ</a></div>
    </nav>    
</aside>