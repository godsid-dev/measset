<section id="sec-register">
	<div class="container">
        <h2>มั่นใจ ราคาดี<br><big>ถูกกฎหมาย</big></h2>
    	<!--<h2>ขายฝากโทรเลย <big><a href="tel:028888888">02-888-8888</a></big></h2>-->
        <form method="post" action="<?php echo base_url('/invest')?>" id="register-now">
        	<fieldset>
            <legend class="hid">ลงทะเบียนขายฝาก</legend>
            <p>
            <label for="category" class="cv-select">
            <select class="select-box" id="category" onchange="$('#btn-view-list').click();">
            	<option>- Select Category -</option>
                <option>ที่ดินเปล่า</option>
                <option>ที่ดินมีสิ่งปลูกสร้าง</option>
                <option>อาคารพาณิชย์</option>
            </select>
            </label>
            </p>
            <!--<p><input type="text" class="txt-box" id="email" name="email" placeholder="@ Enter your email"></p>-->
            <p><a class="ui-btn-green lb-fancy fancybox.iframe" href="<?php echo base_url('/invest')?>" id="btn-view-list">ดูรายการ</a></p>
            </fieldset>
        </form>
    </div>
</section>