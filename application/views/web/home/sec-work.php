 <section id="sec-work">
	<div class="container">
    	<h2 class="head">How it work</h2>
        <p class="title">
            <big>Q Asset คือผู้ให้บริการด้านอสังหาริมทรัพย์ หลายรูปแบบ</big><br>เราคัดกรองและรวบรวมที่ดินผู้ขายฝากและนักลงทุนที่มีคุณภาพไว้ในที่เดียว โดยทีมงานผู้มีความเชี่ยวชาญโดยเฉพาะ<br>
            ติดต่อเราได้ 24 ชั่วโมง 
            โทร: <big><a href="tel:025026000">02-502-6000</a></big>
        </p>
			<ul class="list-work row _chd-cl-xs-06-sm-03">
        	<li>
            	<a href="javascript:;" title="SUBMIT ITEM">
                	<i class="ic-work1"></i>
                	<h3>1) กรอกข้อมูล</h3>
                    <p>
                        ลงทะเบียน <!--บุคคลธรรมดาหรือนิติบุคคล--><br>
                        กรอกรายเอียดอสังหา<br>และรูปอสังหา
                    </p>
                </a>
            </li>
            <li>
            	<a href="javascript:;" title="INSTANT QUOTE">
                	<i class="ic-work2"></i>
                	<h3>2) ระบุราคา</h3>
                    <p>
                        กรอกราคาและจำนวนเดือน<br>
                        ที่ต้องการขายฝาก
                    </p>
                </a>
            </li>
            <li>
            	<a href="javascript:;" title="SHIP YOUR ITEM">
                	<i class="ic-work3"></i>
                	<h3>3) ยืนยันข้อมูล</h3>
                    <p>
                        ส่งข้อมูล และรอรับ<br>
                        รายละเอียดจาก<br>
                        บริษัทประเมิน
                    </p>
                </a>
            </li>
            <li>
            	<a href="javascript:;" title="4) RECEIVE CASH!">
                	<i class="ic-work4"></i>
                	<h3>4) รับเงิน</h3>
                    <p>
                        ทำสัญญา ตามระยะเวลา<br>
                        และ อัตราดอกเบี้ย<br>  
                        %ดอกเบี้ยที่ต้องการ
                    </p>
                </a>
            </li>
        </ul>
        <p class="ctrl-btn"><a class="ui-btn-green" href="<?php echo base_url('/howitwork')?>" title="รายละเอียด">รายละเอียด</a> </p>
    </div>
</section>