<section id="sec-contact">
	<div class="container">
        <h2 class="head">Contact US</h2>
        <p class="title">
            ปลอดภัย ถูกต้อง ยุติธรรม และซื่อสัตย์ <br>
            เรายึดมั่นในความถูกต้องและมโนธรรมเป็นที่สุด ทำงานตรงไปตรงมา พร้อมมีหลักการชัดเจน <br>
            รับเงินง่าย ทันใช้ และรวดเร็ว อย่างถูกกฎหมาย
        </p>
    </div>
    <div class="img-map">
    	<a class="lb-fancy fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14205.706155154905!2d100.5270171665247!3d13.907433286464903!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x36af40a75ccce9b0!2z4Lia4Lij4Li04Lip4Lix4LiX4Liq4Liy4Lih4Liy4Lij4LiW4Lih4Lix4Lil4LiV4Li04Lih4Li14LmA4LiU4Li14LiiIOC4iOC4s-C4geC4seC4lA!5e0!3m2!1sen!2sth!4v1469434001826">
        <img src="<?php echo assets_url('/themes/default/img/maps.jpg'); ?>" alt="Map"></a>
    	<address>
            <b>บริษัท คิว แอสเสท จำกัด (มหาชน)</b>
            อาคารซอฟต์แวร์ปาร์ค ชั้น 26, 99/3 หมู่ 4 
            ถนนแจ้งวัฒนะ ตำบลคลองเกลือ 
            อำเภอปากเกร็ด นนทบุรี  Tel : <a href="tel:025026000">02-502-6000</a> 
        </address>
    </div>
    <form action="<?php echo base_url('/contactus/submit')?>" method="post">
        <div class="container _chd-cl-xs-12-sm-06">
        	<div class="_chd-cl-xs-12-mb10">
            	<input type="text" class="txt-box" id="yourname" name="name" placeholder="ชื่อ - นามสกุล">
                <input type="text" class="txt-box" id="your-email" name="email" placeholder="E-mail">
                <input type="text" class="txt-box" id="your-phone" name="phone" placeholder="โทรศัพท์">
            </div>
            <div class="_chd-cl-xs-12">
            	<textarea class="txt-area" id="msg-contact" placeholder="รายละเอียด" name="description"></textarea>
            </div>
            <div class="_self-cl-xs-12-sm-12-mt15">
            	<button type="submit" id="sedn-contact" name="contactus" class="ui-btn-green">SEND</button>
            </div>
        </div>
    </form>
</section>