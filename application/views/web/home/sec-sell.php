<section id="sec-sell" class="dark">
	<div class="container">
    	<h2 class="head">สนใจขายฝาก</h2>
        <p class="title"> 
            คิดดอกเบี้ยตามกฎหมายกำหนด ไม่เกิน 15% ต่อปี <big>รับเงินไว</big><br>
            เรามีผู้เชี่ยวชาญด้านการประเมินสินทรัพย์ที่ได้รับการรับรองจาก กลต.<br>
            ไม่ต้องกังวลเรื่องเครดิตบูโร<br>
            สามารถขยายระยะเวลาไถ่ถอนได้นานสูงสุดถึง 10 ปี
        </p>
		<p class="ctrl-btn">
            <a class="ui-btn-gray" href="<?php echo base_url('/sell/info')?>" title="รายละเอียด">รายละเอียด</a> 
            <a class="ui-btn-green" href="<?php echo base_url('/my/sell/form')?>" title="สนใจขายฝาก">สนใจ</a>
        </p>
    </div>
</section>