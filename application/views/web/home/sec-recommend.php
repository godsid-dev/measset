<!-- zone -->
    <section id="sec-list-rec" class="dark">
    	<div class="container">
        	<h2 class="head">อสังหาริมทรัพย์ แนะนำ</h2>
            	<div class="list-archive grid-thb row _chd-cl-xs-12-sm-06-md-04">
                <?php for($i=1;$i<=3;$i++){ ?>
                    <article class="bounceInDown <?php if($i==3){ ?>hidden-sm<?php } ?>">
                    <div class="skin">
                        <figure<?php if($i<=2){ ?> class="great"<?php } ?>>
                            <a href="<?php echo base_url('/invest')?>" title="ที่ดินพร้อมสิ่งปลูกสร้าง : อาคารทาวน์เฮ้าส์ 3 ชั้น จำนวน 1 หลัง"><img src="<?php echo assets_url('/themes/default/img/thumb_404x404.jpg')?>" alt="ที่ดินพร้อมสิ่งปลูกสร้าง : อาคารทาวน์เฮ้าส์ 3 ชั้น จำนวน 1 หลัง" width="320"></a>
                        </figure>
                        <div class="info">
                            <h2 class="header"><a href="<?php echo base_url('/invest')?>" title="ที่ดินพร้อมสิ่งปลูกสร้าง : อาคารทาวน์เฮ้าส์ 3 ชั้น จำนวน 1 หลัง"><span>ประเภททรัพย์สิน : </span>ที่ดินพร้อมสิ่งปลูกสร้าง : อาคารทาวน์เฮ้าส์ 3 ชั้น จำนวน 1 หลัง</a></h2>
                            <div class="value">
                                <p><strong>มูลค่าตลาดทรัพย์สิน	: </strong> <em><big>1,800,000.-</big> บาท  </em></p>
                                <p><strong>มูลค่าขายฝาก	: </strong> <em><big>900,000.-</big> บาท </em></p>
                                <div class="ctrl-btn">
                                     <?php if($i<=4){ ?><a class="btn-investment" href="<?php echo base_url('/invest')?>" title="ผลตอบแทน 15% ต่อปี ระยะเวลาลงทุน 12 เดือน">ผลตอบแทน 15% ต่อปี ระยะเวลาลงทุน 12 เดือน</a><?php } ?>
                                    <a class="ui-btn-green-mini" href="<?php echo base_url('/invest')?>" title="รายละเอียด">รายละเอียด &raquo;</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    </article>
                    <?php } ?>
                </div>
                <div class="ui-btn-gray2-loadmore">
                <a href="<?php echo base_url('/invest')?>" title="ดูทั้งหมด">ดูทั้งหมด</a>
                </div>
        </div>
    </section>
   	<!-- /zone -->    