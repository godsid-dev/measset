<!-- zone -->
    <section id="sec-slide">
    	<div class="slide-pro flexslider">
          <ul class="slides">
          	<li>
             <div class="container">
            <a title="อ่านรายละเอียด" href="<?php echo base_url('/sell/info')?>">
                <img src="<?php echo assets_url('/themes/default/img/bg-home2.jpg')?>">
                <div class="text-c mid">
                	<h3>เราคัดกรองและรวบรวมที่ดินผู้ขายฝากและนักลงทุนที่มีคุณภาพไว้ในที่เดียว โดยทีมงานผู้มีความเชี่ยวชาญโดยเฉพาะ</h3>
                    <p class="ctrl-btn"><span class="ui-btn-green">อ่านรายละเอียด</span></p>
                </div>
                </a>
                </div>
             </li>
             <li>
             <div class="container">
             <a title="สนใจขายฝากคลิก" href="<?php echo base_url('/my/sell/form')?>">
                <img src="<?php echo assets_url('/themes/default/img/bg-home.jpg')?>">
                <div class="text-c mid">
                	<h3>เรามีผู้เชี่ยวชาญด้านการประเมินสินทรัพย์ที่ได้รับการรับรองจาก กลต.ไม่ต้องกังวลเรื่องเครดิตบูโร สามารถขยายระยะเวลาไถ่ถอนได้นานสูงสุดถึง 10 ปี</h3>
                    <p class="ctrl-btn"><span class="ui-btn-green">สนใจขายฝากคลิก</span></p>
                </div>
                </a>
                </div>
             </li>
             <li>
             <div class="container">
             <a title="สนใจลงทุนคลิก" href="<?php echo base_url('/invest')?>">
                <img src="<?php echo assets_url('/themes/default/img/bg-home3.jpg')?>">
                <div class="text-c mid">
                	<h3>ความคุ้มค่าในผลตอบแทน ทั้งในระยะสั้น และระยะยาวโปร่งใส ตรวจสอบได้</h3>
                    <p class="ctrl-btn"><span class="ui-btn-green">สนใจลงทุนคลิก</span></p>
                </div>
                </a>
                </div>
             </li>     
          </ul> 
        </div> 
    </section>
	<!-- /zone -->