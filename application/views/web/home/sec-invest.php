 <section id="sec-invest" class="dark">
	<div class="container">
    	<h2 class="head">ลงทุน</h2>
        <p class="title">
	        ความคุ้มค่าในผลตอบแทน ทั้งในระยะสั้น และระยะยาว<br>
			โปร่งใส ตรวจสอบได้<br>
			เรามีผู้เชี่ยวชาญด้านการประเมินสินทรัพย์ที่ได้รับการรับรองจาก กลต.
		</p>
		<p class="ctrl-btn">
			<a class="ui-btn-gray" href="<?php echo base_url('/invest/info')?>" title="รายละเอียด">รายละเอียด</a> 
			<a class="ui-btn-green" href="<?php echo base_url('/invest')?>" title="สนใจลงทุน">สนใจ</a>
		</p>
    </div>
</section>