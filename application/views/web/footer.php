<!-- Footer -->
<footer id="footer">
	<div class="container">
    	<h3 class="lg-footer"><a href="<?php echo base_url('/') ?>" title="QAsset"><img src=<?php echo assets_url('/themes/default/img/logo-footer.png'); ?> alt="QAsset"></a></h3>
       	<div class="sub">
        	<form method="post" id="subscribe" action="<?php echo base_url('/subscribe/submit')?>">
            <fieldset>
        		<legend>Subscribe to our Newsletter</legend>
                <input type="text" class="txt-box" placeholder="E-mail" name="email">
            	<input type="submit" class="btn" id="btn-subs" value="subscribe">
            </fieldset>
            </form>
        </div>		
		<address>
	        บริษัท คิว แอสเสท จำกัด (มหาชน) อาคารซอฟต์แวร์ปาร์ค ชั้น 26, 99/3 หมู่ 4 ถนนแจ้งวัฒนะ <br>
			ตำบลคลองเกลือ อำเภอปากเกร็ด จังหวัดนนทบุรี 11000 
        </address>
        <div class="contact">
        	<a href="tel:02-576-6000 "><i class="fa-phone"></i> 02-502-6000</a>
            <a href="mailto:admin@qasset.com"><i class="fa-envelope"></i> admin@qasset.com</a>
        </div>
        <div class="follow">
        	<b>FOLLOW US</b>
            <ul class="list-inline">
            	<li>
                	<a href="#" class="btn-share fb"><i class="fa-facebook"></i> FACEBOOK</a>
                </li>
                <li>
                	<a href="#" class="btn-share tw"><i class="fa-twitter"></i> TWITTER</a>
                </li>
                <li>
                	<a href="#" class="btn-share yt"><i class="fa-youtube-play"></i> YOUTUBE</a>
                </li>
                <li>
                	<a href="#" class="btn-share gg"><i class="fa-google-plus"></i> GOOGLE PLUS</a>
                </li>
            </ul>
          </div>
    </div>
	<p class="cright">@2016 QAsset. All right reserved</p>
</footer>
<div title="Scroll To Top" class="fa-chevron-up" id="back2top"></div>
<script type="text/javascript">
   $(document).ready(function(){

        //profile
        $('#btn-profile').click(function(){
            $(this).toggleClass('active');
            $(this).next('.pop-member').slideToggle(300);
            $('body').toggleClass('hidescroll');
        });
        $('.sl-munu>h2').click(function(){
            $(this).toggleClass('active');
            $(this).next('.pop-nav').slideToggle(300);
            $('body').toggleClass('hidescroll');
        });
        $(".pop-nav>ul>li>a").removeClass("selected");
        $('.pop-nav>ul:nth-child(4)>li:nth-child(2)>a').addClass('selected');

        $('input.money').change(function(){
            money = $(this).val();
            money = money.replace(/,/g,"");
            money = money.replace(/./g, function(c, i, a) {
                    return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                });
            $(this).val(money);
        });
    }); 
</script>
<!-- /Footer -->