<section id="sec-login" class="row _chd-cl-xs-12-sm-06">
        
    <div class="z-login">
    	<h2 class="topic"><span class="h-line small">ACCOUNT LOGIN</span> <i class="fa-lock"></i></h2>
        <div class="box-mem">
            <form id="login" action="<?php base_url('/login')?>" method="post" name="login">
                <fieldset>
                    <legend class="hid">เข้าสู่ระบบ</legend>
                        <p>
                            <label for="u-name">Email <span>(<a title="ลืมรหัสผ่าน" href="q&amp;a.php">Login Help?</a>)</span></label>
                            <input id="u-name" class="txt-box" type="text" name="u-name">
                        </p>
                        <p>
                            <label for="u-pass">Password <span>(<a title="ลืมรหัสผ่าน" href="#forgetpassword.php">Forget Password?</a>)</span></label>
                            <input id="u-pass" class="txt-box" type="password" name="u-pass">
                        </p>
                        <p class="ctrl-bn">
                        	<input id="btn-log" class="ui-btn-green-login" type="submit" name="btn-log" value="Login"> <span><a href="#" title="Security FAQ"><i class="fa-lock"></i> Security FAQ</a></span>
                        </p>
                </fieldset>
            </form>
        </div>
    </div>
    
    <div class="z-regis">
    	<h2 class="topic"><span class="h-line small">NEW ACCOUNT</span></h2>
        <div class="box-mem">
            <form id="register" action="sell.php" method="post" name="register">
                <fieldset>
                    <legend class="hid">สมัครสมาชิก</legend>
                        <p>
                            <label for="new-name">Email Address</label>
                            <input id="new-name" class="txt-box" type="text" name="new-name">
                        </p>
                        <p>
                            <label for="new-pass">Choose Password</label>
                            <input id="new-pass" class="txt-box" type="password" name="new-pass">
                        </p>
                        <p>
                            <label for="cnew-pass">Confirm Password</label>
                            <input id="cnew-pass" class="txt-box" type="password" name="cnew-pass">
                        </p>
                        <p class="ctrl-bn">
                        	<input id="btn-log" class="ui-btn-green-login" type="submit" name="btn-log" value="Register"> <span><a href="#" title="Security FAQ"><i class="fa-lock"></i> Security FAQ</a></span>
                        </p>
                </fieldset>
            </form>
        </div>
    </div>

</section>