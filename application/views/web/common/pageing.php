<?php 
  if(parse_url($url,PHP_URL_QUERY)){
    $url.="&page={page}";
  }else{
    $url.="?page={page}";
  }
  
  $maxpage = ceil($item/$limit);
  if($maxpage>1){
?>

<nav class="txt-c">
  <ol class="pagination">
  <li class="bn-prev">
    <a title="Previous" href="#">&lt;</a>
  </li>
  <?php 

    for($i=1;$i<=$maxpage;$i++){
        if($page == $i){?>
          <li>
            <span class="selected"><?php echo $i ?></span>
          </li>
        <?php }else{ ?>
          <li>
            <a title="<?php echo $i ?>" href="<?php echo str_replace('{page}',$i,$url)?>"><?php echo $i?></a>
          </li>      
    <?php }
    }
  ?>
  <li class="bn-next">
    <a title="Next" href="#">&gt;</a>
  </li>
</ol>
</nav>

<?php 
  }
?>