<!-- นิติบุคคล -->
<form id="sell-step1" action="<?php echo base_url('/my/invest/form2/corporation')?>" id="corporateProfile" method="post" enctype="multipart/form-data" >
    <div id="corporate" class="bx-tab">
        <div class="imageBased _self-cl-xs-12">
                <input type="checkbox" name="objective" id="objective" value="YES" <?php echo (isset($objective) && $objective=='YES')?'checked="checked"':'';?>><label for="objective">บริษัทมีวัตถุประสงค์ที่เกี่ยวข้องกับการขายอสังหาริมทรัพย์</label>
        </div>
        
        <div>
            <label for="company_name">ชื่อบริษัท</label>
            <input id="company_name" class="txt-box" type="text" name="company_name" value="<?php echo (isset($company_name))?$company_name:''?>">
        </div>
        <div>
            <label for="registration_tax">เลขทะเบียนนิติบุคคล</label>
            <input id="registration_tax" class="txt-box" type="text" name="registration_tax" value="<?php echo (isset($registration_tax))?$registration_tax:''?>">
        </div>
        
        <?php echo $this->load->get_section('business_type')?>
        
        <div>
            <label for="contact_name">ชื่อ-นามสกุล ผู้ติดต่อ</label>
            <input id="contact_name" class="txt-box" type="text" name="contact_name" value="<?php echo (isset($contact_name))?$contact_name:''?>">
        </div>
        
        <div>
            <label for="contact_phone">เบอร์โทรศัพท์ ผู้ติดต่อ</label>
            <input id="contact_phone" class="txt-box" type="text" name="contact_phone" value="<?php echo (isset($contact_phone))?$contact_phone:''?>">
        </div>
        
        <?php echo $this->load->get_section('bank')?>

        <div class="_self-mt30">
            <?php if(isset($corporation_image) && !empty($corporation_image)){ ?>
            <a target="_blank" class="fancybox" href="<?php echo static_url($corporation_image)?>" title="ดูรูปใหญ่" ><img src="<?php echo static_url($corporation_image)?>" width="100" /></a>
            <?php }?>
            <label for="corporation_image">อัพโหลด หนังสือรับรองบริษัท</label>
            <div class="row">
                <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                    <input id="corporation_image" class="txt-box" type="file" name="corporation_image">
                </p>
            </div>
        </div>

        <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
            <input type="submit" value="บันทึกข้อมูล" name="investor_edit" class="ui-btn-tsmall-yellow-edit" id="btn-edit">
        </div>
    </div>    
</form>