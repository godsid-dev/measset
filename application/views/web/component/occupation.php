<?php 
    $occupations = array('กฎหมาย',  'กราฟฟิกดีไซน์',  'การคมนาคมขนส่ง',  'การค้าระหว่างประเทศ',  'การตลาด',  
            'การศึกษา','การฝึกอบรม',  'การเกษตรกรรมและทรัพยากรธรรมชาติ',  'การเงิน',  'งานขาย',  'จัดซื้อ',  'ทรัพยากรบุคคล',  
            'ธุรการ',  'บริการลูกค้า',  'บัญชี',  'ผู้บริหารระดับสูง',  'ฝ่ายผลิต','ผลิตภัณฑ์',  'มนุษยศาสตร์',  'วิจัยและพัฒนา',
            'วิทยาศาสตร์',  'วิศวกร','ช่างเทคนิค',  'ศิลปกรรมศาสตร์',  'สถาปนิก','มัณฑนากร',  'สังคมสงเคราะห์', 
             'อาหารและเครื่องดื่ม','เภสัชกร','แพทย์','สาธารณสุข',  'เลขานุการ',  'เศรษฐศาสตร์',  'โฆษณา','ประชาสัมพันธ์',
             'นิเทศศาสตร์',  'ไอที','คอมพิวเตอร์' );

?>
<div>
    <label for="occupation">อาชีพ</label>
    <span class="cv-select" for="category">
        <select id="occupation" required="" class="select-box" name="occupation">
            <option>เลือกอาชีพ</option>
            <?php
            
            foreach ($occupations as $item) { 
                $selected = (isset($occupation)&&$occupation == $item)?"selected":"";
                ?>
                <option value="<?php echo $item?>" <?php echo $selected?>><?php echo $item?></option>
            <?php }
             ?>
        </select>
    </span>    
</div>