<div>
    <label for="address">ที่อยู่</label>
    <input id="address" class="txt-box" type="text" name="address" value="<?php echo isset($profile['default'])?$profile['default']['address']:'' ?>">
</div>
<div>
    <label for="province">จังหวัด</label>
    <span class="cv-select" for="category">
    <select id="province" required="" class="select-box" name="province">
        <option value="">- เลือกจังหวัด -</option>
		<option value="กระบี่">  กระบี่ </option>
		<option value="กรุงเทพมหานคร">  กรุงเทพมหานคร </option>
		<option value="กาญจนบุรี">  กาญจนบุรี </option>
		<option value="กาฬสินธุ์">  กาฬสินธุ์ </option>
		<option value="กำแพงเพชร">  กำแพงเพชร </option>
		<option value="ขอนแก่น">  ขอนแก่น </option>
		<option value="จันทบุรี">  จันทบุรี </option>
		<option value="ฉะเชิงเทรา">  ฉะเชิงเทรา </option>
		<option value="ชลบุรี">  ชลบุรี </option>
		<option value="ชัยนาท">  ชัยนาท </option>
		<option value="ชัยภูมิ">  ชัยภูมิ </option>
		<option value="ชุมพร">  ชุมพร </option>
		<option value="ตรัง">  ตรัง </option>
		<option value="ตราด">  ตราด </option>
		<option value="ตาก">  ตาก </option>
		<option value="นครนายก">  นครนายก </option>
		<option value="นครปฐม">  นครปฐม </option>
		<option value="นครพนม">  นครพนม </option>
		<option value="นครราชสีมา">  นครราชสีมา </option>
		<option value="นครศรีธรรมราช">  นครศรีธรรมราช </option>
		<option value="นครสวรรค์">  นครสวรรค์ </option>
		<option value="นนทบุรี">  นนทบุรี </option>
		<option value="นราธิวาส">  นราธิวาส </option>
		<option value="น่าน">  น่าน </option>
		<option value="บุรีรัมย์">  บุรีรัมย์ </option>
		<option value="ปทุมธานี">  ปทุมธานี </option>
		<option value="ประจวบคีรีขันธ์">  ประจวบคีรีขันธ์ </option>
		<option value="ปราจีนบุรี">  ปราจีนบุรี </option>
		<option value="ปัตตานี">  ปัตตานี </option>
		<option value="พระนครศรีอยุธยา">  พระนครศรีอยุธยา </option>
		<option value="พะเยา">  พะเยา </option>
		<option value="พังงา">  พังงา </option>
		<option value="พัทลุง">  พัทลุง </option>
		<option value="พิจิตร">  พิจิตร </option>
		<option value="พิษณุโลก">  พิษณุโลก </option>
		<option value="ภูเก็ต">  ภูเก็ต </option>
		<option value="มหาสารคาม">  มหาสารคาม </option>
		<option value="มุกดาหาร">  มุกดาหาร </option>
		<option value="ยะลา">  ยะลา </option>
		<option value="ยโสธร">  ยโสธร </option>
		<option value="ระนอง">  ระนอง </option>
		<option value="ระยอง">  ระยอง </option>
		<option value="ราชบุรี">  ราชบุรี </option>
		<option value="ร้อยเอ็ด">  ร้อยเอ็ด </option>
		<option value="ลพบุรี">  ลพบุรี </option>
		<option value="ลำปาง">  ลำปาง </option>
		<option value="ลำพูน">  ลำพูน </option>
		<option value="ศรีสะเกษ">  ศรีสะเกษ </option>
		<option value="สกลนคร">  สกลนคร </option>
		<option value="สงขลา">  สงขลา </option>
		<option value="สตูล">  สตูล </option>
		<option value="สมุทรปราการ">  สมุทรปราการ </option>
		<option value="สมุทรสงคราม">  สมุทรสงคราม </option>
		<option value="สมุทรสาคร">  สมุทรสาคร </option>
		<option value="สระบุรี">  สระบุรี </option>
		<option value="สระแก้ว">  สระแก้ว </option>
		<option value="สิงห์บุรี">  สิงห์บุรี </option>
		<option value="สุพรรณบุรี">  สุพรรณบุรี </option>
		<option value="สุราษฎร์ธานี">  สุราษฎร์ธานี </option>
		<option value="สุรินทร์">  สุรินทร์ </option>
		<option value="สุโขทัย">  สุโขทัย </option>
		<option value="หนองคาย">  หนองคาย </option>
		<option value="หนองบัวลำภู">  หนองบัวลำภู </option>
		<option value="อำนาจเจริญ">  อำนาจเจริญ </option>
		<option value="อุดรธานี">  อุดรธานี </option>
		<option value="อุตรดิตถ์">  อุตรดิตถ์ </option>
		<option value="อุทัยธานี">  อุทัยธานี </option>
		<option value="อุบลราชธานี">  อุบลราชธานี </option>
		<option value="อ่างทอง">  อ่างทอง </option>
		<option value="เชียงราย">  เชียงราย </option>
		<option value="เชียงใหม่">  เชียงใหม่ </option>
		<option value="เพชรบุรี">  เพชรบุรี </option>
		<option value="เพชรบูรณ์">  เพชรบูรณ์ </option>
		<option value="เลย">  เลย </option>
		<option value="แพร่">  แพร่ </option>
		<option value="แม่ฮ่องสอน">  แม่ฮ่องสอน </option>
    </select>
    </span>
</div>
<div>
    <label for="u-name">อำเภอ</label>
    <span class="cv-select" for="district">
    <select id="district" required="" class="select-box" name="district">
        <option value="">- เลือกอำเภอ -</option>
    </select>
    </span>
</div>

<div>
    <label for="u-name">ตำบล</label>
    <span class="cv-select" for="sub_district">
        <select id="tambon" required="" class="select-box" name="tambon">
            <option value="">- เลือกตำบล -</option>
        </select>
    	
    </span>
</div>
<div>
    <label for="postcode">รหัสไปรษณีย์</label>
    <input id="postcode" class="txt-box" type="text" name="postcode" value="<?php echo isset($profile['default'])?$profile['default']['postcode']:'' ?>" required="">
</div>

<script type="text/javascript">
	var locationUrl = "<?php echo base_url('/location')?>";
    var district = [];

    $('#province').change(function(){
        getDistrict($(this).val());
    });

    $('#district').change(function(){
        getTambon($(this).val());
    });

    $(document).ready(function(){
        var proviceSelected = '<?php echo (isset($province)?$province:'')?>';
        if(proviceSelected!=''){
            $('#province option[value='+proviceSelected+']').attr('selected','selected');
            getDistrict(proviceSelected);
        }
    });

    function getDistrict(name){
        var districtSelected = '<?php echo (isset($district)?$district:'')?>';
        $.get(locationUrl+'/district/'+name,function(resp){
            var html = "<option>- เลือกอำเภอ -</option>";
            for(i=0,j=resp.length;i<j;i++){
                html+='<option value="'+resp[i]['name']+'">'+resp[i]['name']+'</option>';
            }
            $('#district').html(html);
            if(districtSelected!=''){
                $('#district option[value='+districtSelected+']').attr('selected','selected');
                getTambon(districtSelected);
            }
        });
    }

    function getTambon(district){
        var tambonSelected = '<?php echo (isset($tambon)?$tambon:'')?>';
        $.get(locationUrl+'/tambon/'+district,function(resp){
            var html = "<option>- เลือกตำบล -</option>";
            for(i=0,j=resp.length;i<j;i++){
                html+='<option value="'+resp[i]['name']+'">'+resp[i]['name']+'</option>';
            }
            $('#tambon').html(html);
            if(tambonSelected!=''){
                $('#tambon option[value='+tambonSelected+']').attr('selected','selected');
            }
        });
    }

</script>