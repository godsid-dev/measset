<!-- บุคคลธรรมดา -->
<form id="editprofile" name="editprofile" method="post" action="<?php echo base_url('/my/profile/submit/individual')?>" enctype="multipart/form-data">
<div id="individual" class="bx-tab">
    <div class="before-name">
        <p class="imageBased _self-cl-xs-12-sm-05-md-04">
            <label for="status">สถานภาพ</label>
            <input type="radio" required="" name="status" id="status01" value="โสด" <?php echo (isset($profile['default']) && $profile['default']['status']=='โสด')?'checked="checked"':'';?>><label for="status01">โสด</label>
            <input type="radio" required="" name="status" id="status02" value="สมรส" <?php echo (isset($profile['default']) && $profile['default']['status']=='สมรส')?'checked="checked"':'';?>><label for="status02">สมรส</label>
        </p>
        <p class="imageBased _self-cl-xs-12-sm-05-md-08">
            <label for="info-1">คำนำหน้าชื่อ</label>
            <input type="radio" required="" name="title" id="sir01" <?php echo (isset($profile['default']) && $profile['default']['title']=='นาย')?'checked="checked"':'';?> value="นาย"><label for="sir01" >นาย</label>
            <input type="radio" required="" name="title" id="sir02" <?php echo (isset($profile['default']) && $profile['default']['title']=='นาง')?'checked="checked"':'';?> value="v"><label for="sir02">นาง</label>
            <input type="radio" required="" name="title" id="sir03" <?php echo (isset($profile['default']) && $profile['default']['title']=='นางสาว')?'checked="checked"':'';?> value="นางสาว"><label for="sir03">นางสาว</label>
        </p>
    </div>
    <div>
        <label for="firstname">ชื่อ</label>
        <input id="firstname" class="txt-box form-control" required="" type="text" name="firstname" value="<?php echo isset($profile['default'])?$profile['default']['firstname']:'' ?>">
    </div>
    <div>
        <label for="lastname">นามสกุล</label>
        <input id="lastname" class="txt-box form-control" required="" type="text" name="lastname" value="<?php echo isset($profile['default'])?$profile['default']['lastname']:'' ?>">
    </div>

    <div>
        <div class="row">
            <p class="_self-cl-xs-12-sm-07-md-08">
            <label for="birth_date">วัน-เดือน-ปีเกิด</label>
            <input id="birth_date" class="txt-box datepicker" required="" type="text" name="birth_date" placeholder="วัน-เดือน-ปี" value="<?php echo isset($profile['default'])?$profile['default']['birth_date']:'' ?>">
            </p>
        </div>
    </div>
    <div>
        <label for="id_card">เลขบัตรประจำตัวประชาชน</label>
        <input id="id_card" required="" class="txt-box" type="text" name="id_card" placeholder="x-xxxx-xxxxx-xx-x" maxlength="13" value="<?php echo isset($profile['default'])?$profile['default']['id_card']:'' ?>">
    </div>
    <div>
        <label for="id_card_issue">วันที่ออกบัตร</label>
        <input id="id_card_issue" required="" class="txt-box datepicker" type="text" name="id_card_issue" placeholder="DD/MM/YYYY" value="<?php echo (isset($profile['default'])&&isset($profile['default']['id_card_issue']))?$profile['default']['id_card_issue']:'' ?>">
    </div>

    <div>
        <label for="telephone">เบอร์โทรศัพท์</label>
        <input id="telephone" required="" class="txt-box" type="text" name="telephone" value="<?php echo isset($profile['default'])?$profile['default']['telephone']:'' ?>">
    </div>
    <div>
        <label for="email">อีเมล์</label>
        <input id="email" class="txt-box" type="text" name="email" disabled value="<?php echo $profile['email'] ?>">
    </div>

    <?php echo $this->load->get_section('occupation')?>
    
    <?php echo $this->load->get_section('address')?>

    <div class="_self-mt30">
        <?php if(isset($id_card_image) && !empty($id_card_image)){?>
                <a target="_blank" class="fancybox" href="<?php echo static_url($id_card_image)?>" title="ดูรูปใหญ่"><img src="<?php echo static_url($id_card_image)?>" width="100" /></a>
            <?php }?>
        <label for="id_card_image">อัพโหลด สำเนาบัตรประชาชน</label>
        <div class="row">
            <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                <input id="id_card_image" class="txt-box" type="file" name="id_card_image">
            </p>
        </div>
    </div>

    <div class="_self-mt30">
        <?php if(isset($home_document_image) && !empty($home_document_image)){ ?>
            <a target="_blank" class="fancybox" href="<?php echo static_url($home_document_image)?>" title="ดูรูปใหญ่" ><img src="<?php echo static_url($home_document_image)?>" width="100" /></a>
        <?php }?>
        <label for="home_document_image">อัพโหลด สำเนาทำเบียนบ้าน</label>
        <div class="row">
            
            <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                <input id="home_document_image" class="txt-box" type="file" name="home_document_image">
            </p>
        </div>
    </div>


   <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
        <input type="submit" value="แก้ไข้ข้อมูลส่วนตัว" name="profile_edit" class="ui-btn-tsmall-red-edit" id="btn-edit">&nbsp;&nbsp;&nbsp;
        <a href="<?php echo base_url('/my/profile/changePassword')?>" class="ui-btn-tsmall-yellow-cpw" id="btn-cpw">เปลี่ยนรหัสผ่าน</a>
    </div>
</div>
</form>