<div>
    <label for="contract">จำนวนเดือนที่จะทำสัญญา</label>
    <span class="cv-select" for="category">
    <select id="contract" class="select-box form-control" name="contract" required="" >
        <option>- เลือกจำนวนเดือน -</option>
        <option value="3">3 เดือน</option>
        <option value="6">6 เดือน</option>
        <option value="12">12 เดือน (1 ปี)</option>
        <option value="18">18 เดือน (1.5 ปี)</option>
        <option value="24">24 เดือน (2 ปี)</option>
        <option value="30">30 เดือน (2.5 ปี)</option>
        <option value="36">36 เดือน (3 ปี)</option>
        <option value="42">42 เดือน (3.5 ปี)</option>
        <option value="48">48 เดือน (4 ปี)</option>
        <option value="54">54 เดือน (4.5 ปี)</option>
        <option value="60">60 เดือน (5 ปี)</option>
    </select>
    </span>
</div>