<div>
        <label for="bank_id">เลขบัญชี</label>
        <input id="bank_id" class="txt-box" type="text" name="bank_id" value="<?php if(isset($id)){echo $id;} ?>" required="">
    </div>

    <div>
        <label for="bank_name">ชื่อบัญชีธนาคาร</label>
        <input id="bank_name" class="txt-box" type="text" name="bank_name" value="<?php if(isset($name)){echo $name;} ?>" required="">
    </div>

    <div>
        <label for="bank">ชื่อธนาคาร</label>
        <select name="bank" id="bank" class="select-box" required="">
            <option value="">- เลือกชื่อธนาคาร -</option>
            <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
            <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
            <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
            <option value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
            <option value="ธนาคารซีไอเอ็มบีไทย">ธนาคารซีไอเอ็มบีไทย</option>
            <option value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
            <option value="ธนาคารทิสโก้">ธนาคารทิสโก้</option>
            <option value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
            <option value="ธนาคารธนชาต">ธนาคารธนชาต</option>
            <option value="ธนาคารยูโอบี">ธนาคารยูโอบี</option>
            <option value="ธนาคารแลนด์แอนด์เฮาส์">ธนาคารแลนด์แอนด์เฮาส์</option>
            <option value="ธนาคารสแตนดาร์ดชาร์เตอร์ด">ธนาคารสแตนดาร์ดชาร์เตอร์ด</option>
            <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
            <option value="ธนาคารออมสิน">ธนาคารออมสิน</option>
            <option value="ธนาคารอาคารสงเคราะห์">ธนาคารอาคารสงเคราะห์</option>
            <option value="ธนาคารอิสลามแห่งประเทศไทย">ธนาคารอิสลามแห่งประเทศไทย</option>
            <option value="ธนาคารไอซีบีซี">ธนาคารไอซีบีซี</option>
            <option value="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร">ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร</option>
            <option value="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย">ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย</option>
            <option value="ธนาคารไทยเครดิตเพื่อรายย่อย">ธนาคารไทยเครดิตเพื่อรายย่อย</option>
        </select>
        <script type="text/javascript">
            <?php if(isset($bank)){?>
                $("#bank option[value=<?php echo $bank?>]").attr("selected","selected");
            <?php }?>
        </script>
    </div>

    <div class="_self-mt30">
        <?php 
            if(isset($image) && !empty($image)){?>
                <a target="_blank" class="fancybox" href="<?php echo static_url($image)?>" title="ดูรูปใหญ่"><img src="<?php echo static_url($image)?>" width="100" /></a>
            <?php }?>
        <label for="bank_image">อัพโหลด หน้าสมุดเงินฝาก</label>
        <div class="row">
            <p class="_self-cl-xs-12-sm-07-md-08-pr0">
                <input id="bank_image" class="txt-box" type="file" name="bank_image" <?php echo (isset($image))?"":"required"; ?>>
            </p>
        </div>
    </div>