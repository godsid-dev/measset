<?php 
      $businessTypes = array(
            'การเกษตร','การท่องเที่ยวและนันทนาการ','การแพทย์‎','ขนส่งและโลจิสติกส์‎','เครื่องใช้ในครัวเรือน‎',
            'เงินทุนและหลักทรัพย์','เทคโนโลยีสารสนเทศและการสื่อสาร‎','แฟชั่น','วัสดุก่อสร้าง','เหมืองแร่',
            'ปิโตรเลียมและเคมีภัณฑ์','ประกันภัยและประกันชีวิต','พลังงานและสาธารณูปโภค‎','พัฒนาอสังหาริมทรัพย์‎',
            'พาณิชย์‎','ยานยนต์‎','วัสดุอุตสาหกรรมและเครื่องจักร‎','สื่อและสิ่งพิมพ์','อาหารและเครื่องดื่ม'
            );
?>
<div>
  <label for="business_type">ประเภทธุรกิจ</label>
  <span class="cv-select" for="business_type">
      <select id="business_type" class="select-box" name="business_type">
            <option>- เลือกประเภทธุรกิจ -</option>
            <?php
            
            foreach ($businessTypes as $item) { 
                $selected = (isset($business_type)&&$business_type == $item)?"selected":"";
                ?>
                <option value="<?php echo $item?>" <?php echo $selected?>><?php echo $item?></option>
            <?php }
             ?>
      </select>
      </span>
</div>