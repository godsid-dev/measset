 <?php //print_r($detail);?>
  <h1 class="topic"><span class="h-line">อสังหาริมทรัพย์</span></h1>
  <h2 class="header"><span>ประเภททรัพย์สิน : </span><?php echo $detail['0']['type'].': '.$detail['0']['title']; ?></h2>  
  <article class="row _chd-cl-xs-12-sm-06 main-info">
  	<!-- gallery -->
      <div class="gallery great">
          <div id="gallery-slide" class="royalSlider rsUni">
            <?php
              for ($i=0; $i<count($detail['0']['images']); $i++) {
            ?>
              <a class="rsImg" data-rsBigImg="<?php echo $detail['0']['images'][$i]; ?>" href="<?php echo static_url($detail['0']['images'][$i]); ?>" data-rsw="400" data-rsh="400">
                <?php echo $detail['0']['title']?>
                <img class="rsTmb" src="<?php echo static_url($detail['0']['images'][$i]); ?>" />
              </a>
            <?php 
              } 
            ?>            
          </div>
      </div>
      <!-- /gallery -->
      <!-- info -->
    <div class="info">
        <ul class="list">
            <li>
                <strong>ที่ตั้งของทรัพย์สิน   :</strong>
                <span>
                  <?php 
                    echo $detail['0']['tambon'].' '.
                    $detail['0']['district'].' '.
                    $detail['0']['province'];
                  ?>
                </span>
              </li>
              <li>
                <strong>เอกสารสิทธิ์ที่ดิน    :</strong>
                <span>
                  <?php 
                    echo 'โฉนดที่ดินเลขที่ '.$detail['0']['title_deed_number'];
                  ?>
                </span>
              </li>
              <li>
                <strong>เนื้อที่ดิน    :</strong>
                <span>
                  <?php 
                    if(isset($detail['0']['specified']['meter'])&&!empty($detail['0']['specified']['meter'])){
                      echo $detail['0']['specified']['meter'],' ตารางเมจร';
                    }else{
                    echo $detail['0']['specified']['rai'],'-',$detail['0']['specified']['ngan'],'-',$detail['0']['specified']['wah'],' ตารางวา';
                    }
                  ?>
                </span>
              </li>
              <!-- <li>
                <strong>ภาระผูกพัน    :</strong>
                <span>ติดจำนองกับ ธนาคารทหารไทย จำกัด (มหาชน)</span>
              </li> -->
              <!-- <li>
                <strong>วิธีประเมินมูลค่า :</strong>
                <span>วิธีเปรียบเทียบราคาตลาด (Market Approach)</span>
              </li> -->
              <li>
                <strong>วันที่ประเมินมูลค่า : </strong>
                <span>
                  <?php echo dateThai('d F Y',strtotime($detail[0]['estimates']['valuation_date']))?>
                </span>
              </li>
          </ul>
          
          <div class="value">
            <p>
              <strong>มูลค่าตลาดทรัพย์สิน  : </strong> 
              <em>
                <big>
                  <?php echo number_format($detail['0']['estimates']['cost_estimate']).'.-';?>
                </big> บาท  
              </em>
            </p>
            <p>
              <strong>มูลค่าขายฝาก : </strong> 
              <em>
                <big>
                  <?php echo number_format($detail['0']['price']).'.-';?>
                </big> บาท 
              </em>
            </p>
            <p>
              <strong>ระยะเวลาลงทุน : </strong> 
              <em>
                <big>
                  <?php echo $detail['0']['contract'];?>
                </big> เดือน (<?php echo $detail['0']['contract']/12?>) ปี 
              </em>
            </p>
            
            <?php 
              if(isset($profile['investor']['status'])&&$profile['investor']['status']) { ?>
            <div class="ctrl-btn">
              <?php 
                  $endPrice60 = $detail['0']['estimates']['cost_estimate']*0.6;
                  $endPrice = $endPrice60>$detail['0']['estimates']?$detail['0']['estimates']:$endPrice60;
                  $priceStep = $endPrice > 1000000?100000:500000;
                  $startPrice = $detail['0']['price']*0.3;
              ?>
              <label>เงินลงทุน</label>
              <input class="range-price" type="text" value="<?php echo $detail[0]['price']?>" min="<?php echo $startPrice?>" max="<?php echo $endPrice?>" id="bidPrice" name="bidPrice" step="<?php echo $priceStep ?>" <?php echo $detail[0]['bidtype']=='interest'?'disabled':''?> /> บาท
            </div>
            <div class="ctrl-btn">
              <?php 
                $interest = floor($detail['0']['interest']);
                $interestPoint = $detail['0']['interest'] - $interest;
              ?>
                <label>อัตราดอกเบี้ย</label>
                <input class="range-interest" type="text" value="<?php echo $detail[0]['interest']?>" min="1" max="15" id="bidInterest" name="bidInterest" step="0.25" <?php echo $detail[0]['bidtype']=='price'?'disabled':''?> /> % / ปี
            </div>
            <div class="ctrl-btn">
              <a class="btn-investment" id="invest" href="#" title="<?php echo 'ผลตอบแทน '.$detail['0']['interest'].'% ต่อปี ระยะเวลาลงทุน '.$detail['0']['contract'].' เดือน'?>">
                สนใจลงทุน<br/>
                ระยะเวลาลงทุน <?php echo $detail['0']['contract']?> เดือน
              </a>
              <a class="ui-btn-green-mini hid" href="#" title="PURCHASE OFFER">PURCHASE OFFER</a>
            </div>
            
            <?php }else{ ?>
            <br/>
            <p>คุณยังไม่ได้ชำระค่ามัดจำการประมูล</p>
            <div class="ctrl-btn">
            <a href="<?php echo site_url('/my/invest/confirmpayment')?>" class="btn btn-investment">ชำระค่ามัดจำการประมูล</a>
            </div>
            <?php }?>
          </div>
      </div>
      <!-- info -->
  </article>
<script>
  function addComma(money){
    money = money+"";
    return money.replace(/./g, function(c, i, a) {
                    return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                });
  }
  $(document).ready(function(){
    $("#bidPrice").asRange({
      keyboard: true,
      format: function(value) {
        return addComma(value);
      }
    });
    $("#bidInterest").asRange({
      keyboard: true,
      scale: true,
      format: function(value) {
        return value+' %';
      }
    });
    if($('#bidInterest').attr('disabled')=='disabled'){
      $("#bidInterest").asRange('disable');  
    }
    if($('#bidPrice').attr('disabled')=='disabled'){
      $("#bidPrice").asRange('disable');  
    }
  });
  
  

  $('#invest').click(function() {
    var interest = parseFloat($("#bidInterest").val());
    var price = addComma($("#bidPrice").val());
    alert("คุณสนใจลงทุนสินทรัพย์มูลค่า: "+price+" บาท\nอัตราดกเบี้ย: "+interest+"%\nระยะเวลา: <?php echo $detail['0']['contract']?> เดือน");

      var investor_id = "<?php echo $user['_id']; ?>";
      var product_id = "<?php echo $detail['0']['_id']; ?>";
      var url_update = "<?php echo site_url('invest/invest_update'); ?>";
      $.ajax({
        type: 'POST',
        url: url_update,
        data: { investor_id: investor_id, product_id: product_id,price:$("#bidPrice").val(),interest:interest },
        success: function(data)
        {
          //alert("ได้ส่งข้อเสนอของท่านถึงผู้ขายฝากแล้ว \n กรุณารอการติดต่อกลับ");
          window.location.href = '<?php echo base_url('/my/invest')?>';
        }
      });
    });
</script>