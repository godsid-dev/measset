    <h1 class="topic">
    	<span class="h-line">อสังหาริมทรัพย์</span>
    	<p class="list-all">ทั้งหมด <span><?php echo count($list);?></span> รายการ</p>
    </h1>    
    <div class="sort-bar">
        <div class="sort">
            <label for="dropdown-sort">จัดเรียงตาม : </label>
            <select class="select-box" id="dropdown-sort" name="dropdown-sort">
                <option value="" class="" data-name="ราคา: สูง - ต่ำ" data-url="?sort=pricedesc&amp;dir=desc">ราคา</option> 
                <option value="" class="" data-name="ราคา: ต่ำ - สูง" data-url="?sort=priceasc&amp;dir=asc">ราคา: ต่ำ - สูง</option>
                <option value="" data-isactive="true" data-name="Recommendation" data-url="?sort=popularity&amp;dir=desc">ความนิยม</option> 
                <option value="" class="" data-name="ส่วนลด" data-url="?sort=discountspecial&amp;dir=desc">ส่วนลด</option>
            </select>
        </div>
        <div class="sort-layout">
            <span> แสดงผล </span>
            <a title="grid" onclick="$('.list-archive').addClass('grid-thb row _chd-cl-xs-12-xsh-06-md-04'); $('.sort-layout>a').removeClass('selected');$(this).addClass('selected');" href="javascript:;"><i class="fa-th-large"></i></a>
            <a class="selected" title="list" onclick="$('.list-archive').removeClass('grid-thb row _chd-cl-xs-12-xsh-06-md-04'); $('.sort-layout>a').removeClass('selected');$(this).addClass('selected');" href="javascript:;"><i class="fa-th-list"></i></a>
        </div>
    </div>    
    <div class="list-archive">
    <?php for($i=0; $i<count($list); $i++){ ?>
         <article>
            <div class="skin">
                <figure>
                    <a href="<?php echo site_url('/invest/detail/'.$list[$i]['_id']->{'$id'})?>" title="<?php echo $list[$i]['title'];?>">
                        <img src="<?php echo static_url($list[$i]['images'][0])?>" alt="<?php echo $list[$i]['type'].': '.$list[$i]['title'];?>" width="320">
                    </a>
                </figure>
                <div class="info">
                    <h2 class="header">
                        <a href="<?php echo site_url('/invest/detail/'.$list[$i]['_id']->{'$id'})?>" title="<?php echo $list[$i]['type'].': '.$list[$i]['title'];?>">
                            <span>ประเภททรัพย์สิน : </span>
                            <?php echo $list[$i]['type'].': '.$list[$i]['title'];?>
                        </a>
                    </h2>                    
                    <ul class="list">
                        <li>
                            <strong>ที่ตั้งของทรัพย์สิน     :</strong>
                            <span>
                                <?php echo $list[$i]['tambon'].' '.
                                $list[$i]['district'].' '.
                                $list[$i]['province'];?>
                            </span>
                        </li>
                        <li>
                            <strong>เอกสารสิทธิ์ที่ดิน      :</strong>
                            <span>
                                <?php echo 'โฉนดที่ดินเลขที่ '.$list[$i]['title_deed_number'].' เนื้อที่ดิน '.$list[$i]['specified']['wah'].' ตารางวา';?>
                            </span>
                        </li>
                        <!--<li>
                            <strong>ภาระผูกพัน      :</strong>
                            <span>ติดจำนองกับ ธนาคารทหารไทย จำกัด (มหาชน)</span>
                        </li>-->
                        <!--<li>
                            <strong>วิธีประเมินมูลค่า   :</strong>
                            <span>วิธีเปรียบเทียบราคาตลาด (Market Approach)</span>
                        </li>-->
                        <li>
                            <strong>วันที่ประเมินมูลค่า : </strong>
                            <span><?php echo dateThai('d F Y',strtotime($list[$i]['estimates']['valuation_date']))?></span>
                        </li>

                    </ul>                    
                    <div class="value">
                        <p>
                            <strong>มูลค่าตลาดทรัพย์สิน : </strong> 
                            <em>
                                <big>
                                    <?php
                                        echo number_format($list[$i]['estimates']['total_value']).'.-';
                                    ?>  
                                </big> บาท  
                            </em>
                        </p>
                        <p>
                            <strong>มูลค่าขายฝาก    : </strong> 
                            <em>
                                <big>
                                    <?php
                                        echo number_format($list[$i]['estimates']['cost_estimate']).'.-';
                                    ?>
                                </big> บาท 
                            </em>
                        </p>
                        <div class="ctrl-btn">
                            <a class="ui-btn-green-mini" href="<?php echo site_url('/invest/detail/'.$list[$i]['_id']->{'$id'})?>" title="รายละเอียด">รายละเอียด &raquo;</a>
                        </div>
                    </div>                    
                </div>
            </div>
        </article>
    <?php } ?>        
    </div>