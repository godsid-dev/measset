<div class="bx-model">
    <h3 class="hd-box h-acc">เพิ่มข้อมูลขายฝากใหม่ <i class="fa-angle-down"></i></h3>
    <div class="bx-body pane">
    	<!-- step -->
        <div class="wp-step">
        	<span class="step1 active">
            	<a href="#" title="Step1">
                <b>Step 1</b>
                <small>ข้อมูลทั่วไป</small>
                </a>
            </span>
            <span class="step2">
            	<a href="#" title="Step2">
            	<b>Step 2</b>
                <small>แผนที่</small>
                </a>
            </span>
            <span class="step3">
            	<a href="#" title="Step3">
            	<b>Step 3</b>
                <small>รูปภาพ</small>
                </a>
            </span>
        </div>
        <!-- /step -->
        <div class="z-post">
            <?php echo (isset($title) ? $title : '');?>
            <?php if (isset($title) && $title=="นางสาว") echo "checked";?>
            <form id="sell-step1" name="sell" method="post" action="sell-post-step2.php">
            <fieldset>
            	<legend class="hid">รายละเอียดลูกค้าขายฝาก</legend>
                <ul class="idTabs _chd-cl-xs-06">
                	<li><a href="#user1" title="บุคคลธรรมดา"><i class="fa-check"></i> บุคคลธรรมดา</a></li>
                    <li><a href="#user2" title="นิติบุคคล"><i class="fa-check"></i> นิติบุคคล</a></li>
                </ul>
                <div class="contentTab">
                	<!-- บุคคลธรรมดา -->
                	<div id="user1" class="bx-tab">                    
                        <div class="before-name">
                            <label for="info-1">คำนำหน้าชื่อ</label>
                            <p class="imageBased">
                                <input type="radio" name="sir" id="sir01" <?php if (isset($title) && $title=="นาย") echo "checked";?> value="นาย">
                                <label for="sir01">นาย</label>
                                <input type="radio" name="sir" id="sir02" <?php if (isset($title) && $title=="นาง") echo "checked";?> value="นาง">
                                <label for="sir02">นาง</label>
                                <input type="radio" name="sir" id="sir03" <?php if (isset($title) && $title=="นางสาว") echo "checked";?> value="นางสาว">
                                <label for="sir03">นางสาว</label>
                                <span>
                                    <input type="radio" name="sir" id="sir04" <?php if (isset($title) && $title=="อื่นๆ") echo "checked";?> value="อื่นๆ">
                                    <label for="sir04">อื่นๆ</label> 
                                    <input id="info-1" class="txt-box inline" type="text" name="info-1">
                                </span>
                            </p>
                        </div>
                        <div>
                            <label for="info-2">ชื่อ-นามสกุล</label>
                            <input id="info-2" class="txt-box" type="text" name="info-2">
                        </div>                        
                        <div>
                            <div class="row">
                                <p class="_self-cl-xs-12-sm-07-md-08">
                                <label for="info-1">วัน-เดือน-ปีเกิด</label>
                                <input id="info-1" class="txt-box" type="text" name="info-1" placeholder="DD/MM/YYYY">
                                </p>
                                <p class="imageBased _self-cl-xs-12-sm-05-md-04">
                                    <label for="info-1">เพศ</label>
                                    <input type="radio" name="sex" id="sex01"><label for="sex01">ชาย</label>
                                    <input type="radio" name="sex" id="sex02"><label for="sex02">หญิง</label>
                                </p>
                            </div>
                        </div>
                        <div>
                            <label for="info-2">เลขบัตรประจำตัวประชาชน</label>
                            <input id="info-2" class="txt-box" type="text" name="info-2" placeholder="x-xxxx-xxxxx-xx-x" maxlength="13">
                        </div>
                        <div>
                            <label for="info-1">เบอร์โทรศัพท์</label>
                            <input id="info-1" class="txt-box" type="text" name="info-1">
                        </div>
                        <div>
                            <div class="row">
                                <p class="imageBased _self-cl-xs-12-sm-05-md-04">
                                    <label for="info-1">สถานภาพ</label>
                                    <input type="radio" name="status" id="status01"><label for="status01">โสด</label>
                                    <input type="radio" name="status" id="status02"><label for="status02">สมรส</label>
                                </p>
                                <p class="_self-cl-xs-12-sm-07-md-08">
                                <label for="info-1">อาชีพ</label>
                                <input id="info-1" class="txt-box" type="text" name="info-1">
                                </p>
                                
                            </div>
                        </div>                        
                        <div class="_self-mt30">
                            <label for="upload-1">อัพโหลด สำเนาบัตรประชาชน</label>
                            <div class="row">
                            	<p class="_self-cl-xs-12-sm-07-md-08-pr0">
                                	<input id="upload-1" class="txt-box" type="file" name="upload-1">
                                </p>
                                <p class="_self-cl-xs-12-sm-05-md-04-pd0">
                                	<span class="_self-cl-xs-06-pr0"><a class="ui-btn-mini-full-red" href="#" title="ตกลง">ตกลง</a></span>
                                    <span class="_self-cl-xs-06"><a class="ui-btn-mini-full-yellow" href="#" title="ยกเลิก">ยกเลิก</a></span>
                                </p>
                            </div>
                        </div>                    
                        <div class="_self-mt30">
                            <label for="upload-2">อัพโหลด สำเนาทำเบียนบ้าน</label>
                            <div class="row">
                            	<p class="_self-cl-xs-12-sm-07-md-08-pr0">
                                	<input id="upload-2" class="txt-box" type="file" name="upload-2">
                                </p>
                                <p class="_self-cl-xs-12-sm-05-md-04-pd0">
                                	<span class="_self-cl-xs-06-pr0"><a class="ui-btn-mini-full-red" href="#" title="ตกลง">ตกลง</a></span>
                                    <span class="_self-cl-xs-06"><a class="ui-btn-mini-full-yellow" href="#" title="ยกเลิก">ยกเลิก</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- นิติบุคคล -->
                	<div id="user2" class="bx-tab">                    	
                        <div>
                            <label for="info-2">ชื่อบริษัท</label>
                            <input id="info-2" class="txt-box" type="text" name="info-2">
                        </div>                        
                        <div>
                            <label for="info-2">เลขทะเบียนนิติบุคคล</label>
                            <input id="info-2" class="txt-box" type="text" name="info-2">
                        </div>                        
                        <div>
                            <label for="info-2">ประเภทธุรกิจ</label>
                            <span class="cv-select" for="category">
                            <select id="category" class="select-box">
                                <option>- เลือกธุรกิจ -</option>
                                <option>ตัวเลือก1</option>
                                <option>ตัวเลือก2</option>
                                <option>ตัวเลือก3</option>
                            </select>
                            </span>
                        </div>                        
                        <div>
                            <label for="info-2">ชื่อ-นามสกุล ผู้ติดต่อ</label>
                            <input id="info-2" class="txt-box" type="text" name="info-2">
                        </div>                        
                        <div>
                            <label for="info-1">เบอร์โทรศัพท์</label>
                            <input id="info-1" class="txt-box" type="text" name="info-1">
                        </div>                        
                        <div class="_self-mt30">
                            <label for="upload-3">อัพโหลด หนังสือรับรองบริษัท</label>
                            <div class="row">
                            	<p class="_self-cl-xs-12-sm-07-md-08-pr0">
                                	<input id="upload-1" class="txt-box" type="file" name="upload-1">
                                </p>
                                <p class="_self-cl-xs-12-sm-05-md-04-pd0">
                                	<span class="_self-cl-xs-06-pr0"><a class="ui-btn-mini-full-red" href="#" title="ตกลง">ตกลง</a></span>
                                    <span class="_self-cl-xs-06"><a class="ui-btn-mini-full-yellow" href="#" title="ยกเลิก">ยกเลิก</a></span>
                                </p>
                            </div>
                        </div>                        
                    </div>
                </div>                
                <div class="ctrl-bn txt-c _self-cl-xs-12-sm-12-mt20">
                    <input type="submit" value="เพิ่มข้อมูล" name="btn-step" class="ui-btn-yellow-post" id="btn-next-step">
                </div>
            </fieldset>
            </form>
        </div>        
    </div>
</div>