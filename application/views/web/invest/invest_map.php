<li>
	<h3 class="h-acc">แผนที่ตั้งทรัพย์สิน <i class="fa-chevron-down"></i></h3>
    <div class="pane">
    	<div class="img-map">
            <a class="lb-fancy fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14205.706155154905!2d100.5270171665247!3d13.907433286464903!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x36af40a75ccce9b0!2z4Lia4Lij4Li04Lip4Lix4LiX4Liq4Liy4Lih4Liy4Lij4LiW4Lih4Lix4Lil4LiV4Li04Lih4Li14LmA4LiU4Li14LiiIOC4iOC4s-C4geC4seC4lA!5e0!3m2!1sen!2sth!4v1469434001826">
            <img src="<?php echo base_url('assets/themes/default/img/maps.jpg'); ?>" alt="Map"></a>
            <address>
                <b>บริษัท คิว แอสเสท จำกัด (มหาชน)</b>
                อาคารซอฟต์แวร์ปาร์ค ชั้น 33, 99/3 หมู่ 4 
                ถนนแจ้งวัฒนะ ตำบลคลองเกลือ 
                อำเภอปากเกร็ด นนทบุรี  Tel : <a href="tel:025765599">02-576-5599</a> 
            </address>
        </div>
    </div>
</li>