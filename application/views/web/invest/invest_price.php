<li>
<h3 class="h-acc active">ข้อมูลการลงทุน <i class="fa-chevron-down"></i></h3>
  <div class="pane" style="display: block;">
    <div class="table-responsive">
      <table class="table" bordercolor="#b5b5b5" style="background:#efefef; border:2px solid #d4d4d4" width="100%" border="1" cellspacing="0" cellpadding="0">
        <thead>
          <tr bgcolor="#149e97">
            <th  align="center">รายการ</th>
            <th  align="center">มูลค่า</th>
          </tr>
          <tr>
            <td>จำนวนเงินที่กู้</td>
            <td><?php echo number_format($product['price']); ?> บาท</td>
          </tr>
          <tr>
            <td>อัตราดอกเบี้ย (ต่อปี)</td>
            <td><?php echo $product['interest'] ?>%</td>
          </tr>
          <tr>
            <td>ระยะเวลาขายฝาก</td>
            <td><?php echo $product['contract'] ?> เดือน</td>
          </tr>
          <tr>
            <td>ดอกเบี้ยที่ได้รับ เมื่อครบกำหนดไถ่ถอน</td>
            <td><?php echo number_format($price['interestPrice']) ?> บาท</td>
          </tr>
          <tr>
            <td>ค่าธรรมและดอกเบี้ยรวม</td>
            <td><?php echo number_format($price['additionalSale']) ?> บาท</td>
          </tr>
          <tr>
            <td>จ่ายเงิน ณ วันที่โอน</td>
            <td><?php echo number_format($product['price']+$price['additionalSale'])?> บาท</td>
          </tr>
          <tr>
            <td colspan="2" >ค่าสินไถ่ เมื่อไถ่ถอนที่ดิน</td>
          </tr>
          <tr>
            <td> - เงินต้น</td>
            <td><?php echo number_format($product['price'])?> บาท</td>
          </tr>
          <tr>
            <td> - ดอกเบี้ย</td>
            <td><?php echo number_format($price['interestPrice']) ?> บาท</td>
          </tr>
          <tr>
            <td> - รวม</td>
            <td><?php echo number_format($product['price']+$price['interestPrice']) ?> บาท</td>
          </tr>
          <tr>
            <td>สรุปรายได้</td>
            <td><?php echo number_format($price['interestPrice'] - $price['additionalSale']) ?> บาท</td>
          </tr>             
        </thead>
      </table>
    </div>
  </div>
</li>