<div id="toc" class="container page-detail">
	<section id="sec-detail">
		<?php echo $this->load->get_section('invest_detail');?>
		<ul class="sec-acc list-detail">
			<?php echo $this->load->get_section('invest_price');?>
			<?php echo $this->load->get_section('invest_info');?>
			<?php echo $this->load->get_section('invest_building');?>
			<?php echo $this->load->get_section('invest_law');?>
			<?php echo $this->load->get_section('invest_summary');?>
			<?php echo $this->load->get_section('invest_compare');?>
			<?php echo $this->load->get_section('invest_shortmap');?>
			<?php echo $this->load->get_section('invest_map');?>
			<?php echo $this->load->get_section('invest_scheme');?>
			<?php echo $this->load->get_section('invest_gallary');?>
			
		</ul>
	</section>
</div>

<script>
	jQuery(document).ready(function($) {
	  $('#gallery-slide').royalSlider({
	    fullscreen: {
	      enabled: true,
	      nativeFS: true
	    },
	    controlNavigation: 'thumbnails',
	    thumbs: {
	      orientation: 'vertical',
	      paddingBottom: 5,
	      appendSpan: true
	    },
	    transitionType:'fade',
	    autoScaleSlider: false, 
	    autoScaleSliderWidth: 506,     
	    autoScaleSliderHeight: 415,
	    loop: true,
	    arrowsNav: false,
	    keyboardNavEnabled: true
	  });
	});
</script>
