<li>
  <h3 class="h-acc active">รายละเอียดที่ดิน <i class="fa-chevron-down"></i></h3>
  <div class="pane" style="display: block;">
  	<div class="table-responsive">
      <table class="table" bordercolor="#b5b5b5" style="background:#efefef; border:2px solid #d4d4d4" width="100%" border="1" cellspacing="0" cellpadding="0">
        <thead>
          <tr bgcolor="#149e97">
            <th rowspan="2" align="center">ลำดับที่</th>
            <th colspan="3" align="center">เอกสารสิทธิ์</th>
            <th colspan="3" align="center">เนื้อที่ดิน</th>
          </tr>
          <tr bgcolor="#149e97">
            <th align="center">โฉนดที่ดินเลขที่</th>
            <th align="center">เลขที่ดิน</th>
            <th align="center">หน้าสำรวจ</th>
            <th align="center">ไร่</th>
            <th align="center">งาน</th>
            <th align="center">ตารางวา</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <?php //$square =  toThaiMatric($detail[0]['specified']['wah']);
            ?>
            <td align="center">1</td>
            <td align="center"><?php echo $detail['0']['sheet']?></td>
            <td align="center"><?php echo $detail['0']['parcel_no']?></td>
            <td align="center"><?php echo $detail['0']['title_deed_number']?></td>
            <td align="center"><?php echo $detail['0']['specified']['rai']?></td>
            <td align="center"><?php echo $detail['0']['specified']['ngan']?></td>
            <td align="center"><?php echo $detail['0']['specified']['wah']?></td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr bgcolor="#9c9c9c">
            <td colspan="4" align="center">รวม</td>
            <td align="center"><?php echo $detail['0']['specified']['rai']?></td>
            <td align="center"><?php echo $detail['0']['specified']['ngan']?></td>
            <td align="center"><?php echo $detail['0']['specified']['wah']?></td>
          </tr>
        </tbody>
      </table>

    </div>
      
      <dl>
      	<!-- row -->
      	<dt>
          <h3>สิทธิตามกฎหมาย</h3>
          </dt>
          <dd>
         	  <div class="imageBased _chd-cl-xs-12-sm-04-pd0 _ps-rlt">
                  <p><input type="checkbox" name="place-info01" id="place-info01" checked disabled><label for="place-info01">กรรมสิทธิ์สมบูรณ์ (Freehold)</label></p>
                  <p><input type="checkbox" name="place-info02" id="place-info02"  disabled><label for="place-info02">สิทธิครอบครอง</label></p>
                  <p><input type="checkbox" name="place-info03" id="place-info03"  disabled><label for="place-info03">สิทธิการเช่า (Leasehold)</label></p>
              </div>
          </dd>
          <!-- /row -->
          <!-- row -->
      	<dt>
          <h3>ภาระผูกพัน</h3>
          </dt>
          <dd>
         	  <div class="imageBased _chd-cl-xs-12-pd0 _ps-rlt">
                  <p><input type="checkbox" name="place-info1-1" id="place-info1-1"  disabled><label for="place-info1-1">ไม่มีภาระผูกพันใดๆ</label></p>
                  <p><input type="checkbox" name="place-info1-2" id="place-info1-2" checked disabled><label for="place-info1-2">มี จำนองเป็นประกันกับ <span class="box-line _self-cl-xs-12-sm-06">ธนาคาร xxxxxxxxxxx จำกัด (มหาชน)</span></label></p>
                  <p><input type="checkbox" name="place-info1-3" id="place-info1-3"  disabled><label for="place-info1-3">อื่นๆ   <span class="box-line _self-cl-xs-04"> &nbsp; </span></label></p>                                                                                                                                                        
              </div>
          </dd>
          <!-- /row -->
          <!-- row -->
      	<dt>
          <h3>ราคาประเมินทุนทรัพย์ในการจดทะเบียนสิทธิ และนิติกรรม</h3>
          </dt>
          <dd>
         	  <div class="imageBased _chd-cl-xs-12-pd0 _ps-rlt">
                  <p><input type="checkbox" name="place-info2-1" id="place-info2-1" checked disabled><label for="place-info2-1">เป็นรายแปลง ตารางวาละ <span class="box-line">30,000</span>บาท</label></p>
                  <p><input type="checkbox" name="place-info2-2" id="place-info2-2"  disabled><label for="place-info2-2">เป็นรายบล๊อก-โซน  ทรัพย์สินอยู่โซน <span class="box-line _self-cl-xs-03-md-02">&nbsp;</span>บล๊อก                   <span class="box-line _self-cl-xs-03-md-02">&nbsp;</span> ตำบล <span class="box-line _self-cl-xs-03-md-02">&nbsp;</span></label></p>
                  <p><input type="checkbox" name="place-info2-3" id="place-info2-3"  disabled><label for="place-info2-3">อำเภอ <span class="box-line _self-cl-xs-04"> &nbsp; </span> จังหวัด   <span class="box-line _self-cl-xs-04"> &nbsp; </span></label></p>                                                                                                                                          
              </div>
          </dd>
          <!-- /row -->
          <!-- row -->
          <dt>
            <div class="point-gps btn-group btn-group-justified">
            	<div class="hd btn btn-default btn-lg">ค่าพิกัด GPS</div>
                <div class="btn btn-default btn-lg">LAT</div>
                <div class="btn btn-default btn-lg">13.871166</div>
                <div class="btn btn-default btn-lg">LONG</div>
                <div class="btn btn-default btn-lg">100.6237362</div>
            </div>
          </dt>
          <!-- /row -->
          <!-- row -->
      	<dt>
          <h3>ลักษณะรูปแปลงที่ดิน</h3>
          </dt>
          <dd>
            <div class="imageBased _chd-cl-xs-12-sm-03-pd0 _ps-rlt">
                  <p><input type="checkbox" name="place-info31" id="place-info31"  disabled><label for="place-info31">สี่เหลี่ยมผืนผ้า</label></p>
                  <p><input type="checkbox" name="place-info32" id="place-info32"  disabled><label for="place-info32">สี่เหลี่ยมจตุรัส</label></p>
                  <p><input type="checkbox" name="place-info33" id="place-info33"  disabled><label for="place-info33">สี่เหลี่ยมคางหมู</label></p>
                  <p><input type="checkbox" name="place-info34" id="place-info34" checked disabled><label for="place-info34">สี่เหลี่ยมด้านไม่เท่า</label></p>
                  <p><input type="checkbox" name="place-info35" id="place-info35"  disabled><label for="place-info35">หลายเหลี่ยม</label></p>
                  <p><input type="checkbox" name="place-info36" id="place-info36"  disabled><label for="place-info36">สูงเสมอถนนผ่านหน้า</label></p>
                  <p><input type="checkbox" name="place-info37" id="place-info37"  disabled><label for="place-info37">อื่นๆ</label></p>
            </div>
         	  <div class="_chd-cl-xs-12-pd0 _ps-rlt">
                  <p>กว้างติดถนนประมาณ <span class="box-line"> 6.50 </span> เมตร <span class="box hidden-xs _self-cl-sm-01">&nbsp;</span> ลึกประมาณ<span class="box-line">16.00 </span> เมตร</p>
              </div>
          </dd>
          <!-- /row -->
          <!-- row -->
      	<dt>
          <h3>สภาพที่ดิน</h3>
          </dt>
          <dd>
         	  <div class="imageBased _chd-cl-xs-12-sm-04-pd0 _ps-rlt">
                  <p><input type="checkbox" name="place-info41" id="place-info41" checked disabled><label for="place-info41">ถมแล้ว</label></p>
                  <p><input type="checkbox" name="place-info42" id="place-info42" checked disabled><label for="place-info42">สูงเสมอถนนผ่านหน้า</label></p>
                  <p><input type="checkbox" name="place-info43" id="place-info43"  disabled><label for="place-info43">สูง/ต่ำกว่าถนนผ่านหน้า<span class="box-line"> &nbsp; </span>เมตร</label></p>
                  <p><input type="checkbox" name="place-info44" id="place-info44"  disabled><label for="place-info44">ยังไม่ถม</label></p>
                  <p><input type="checkbox" name="place-info45" id="place-info45"  disabled><label for="place-info45">สูงเสมอถนนผ่านหน้า</label></p>
                  <p><input type="checkbox" name="place-info46" id="place-info46"  disabled><label for="place-info46">สูง/ต่ำกว่าถนนผ่านหน้า<span class="box-line"> &nbsp; </span>เมตร</label></p>
                  <p><input type="checkbox" name="place-info47" id="place-info47"  disabled><label for="place-info47">สูงต่ำตามภูมิประเทศ</label></p>
            </div>


          </dd>
          <!-- /row -->
          <!-- row -->
      	<dt>
          <h3>การใช้ประโยชน์บนที่ดินปัจจุบัน</h3>
          </dt>
          <dd>
         	  <div class="imageBased _chd-cl-xs-12-sm-03-lg-25-pd0 _ps-rlt">
                  <p class="_self-cl-xs-12-sm-12-lg-12"><input type="checkbox" name="place-info5-1" id="place-info5-1" checked disabled><label for="place-info1-1">ที่ดินพร้อมสิ่งปลูกสร้าง เป็น <span class="box-line"> ทาว์เฮ้าส์ 3 ชั้น จำนวน 1 หลัง </span></label></p>
                  <p><input type="checkbox" name="place-info51" id="place-info51"  disabled><label for="place-info51">อยู่อาศัย</label></p>
                  <p><input type="checkbox" name="place-info52" id="place-info52"  disabled><label for="place-info52">บ้านใหม่ยังไม่เข้าอาศัย</label></p>
                  <p><input type="checkbox" name="place-info53" id="place-info53"  disabled><label for="place-info53">ปล่อยให้เช่า</label></p>
                  <p><input type="checkbox" name="place-info54" id="place-info54"  disabled><label for="place-info54">ปล่อยทิ้งร้าง</label></p>
                  <p><input type="checkbox" name="place-info55" id="place-info55"  disabled><label for="place-info55">อื่นๆ<span class="box-line"> &nbsp; </span></label></p>        
              </div>
          </dd>
          <!-- /row -->
        </dl>        
    </div>
</li>