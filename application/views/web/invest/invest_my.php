<!-- box -->
    <div class="bx-model">
        <h3 class="hd-box h-acc">รายการลงทุนของฉัน <i class="fa-angle-down"></i></h3>
        <div class="bx-body pane">
            <div class="list-table">
                <ol>
                    <li class="th">
                        <ul>
                            <li><p class="imageBased"><input type="checkbox" id="check0" name="check0" onClick="toggle(this)"><label for="check0">ยูนิต</label></p></li>
                            <li>ราคา</li>
                            <li>Bid</li>
                            <li>ประกาศเมื่อ</li>
                            <li>สถานะ</li>
                            <li>เครื่องมือ</li>
                        </ul>
                    </li>
                    <?php 
                        if(isset($products)) {
                            for ($i=0; $i<count($products); $i++) {
                    ?>
                        <li>
                            <ul>
                                <li>
                                    <p class="imageBased">
                                        <input type="checkbox" id="check1" name="foo">
                                        <label for="check1">
                                            <a href="<?php echo base_url('/invest/detail/'.$products[$i]['_id']);?>" title="<?php echo $products[$i]['type'];?>">
                                                <img height="40" alt="<?php echo $products[$i]['type'].' : '.$products[$i]['title'] ;?>" src="<?php echo static_url($products[$i]['images'][0]); ?>"> 
                                                <?php echo $products[$i]['title'];?>
                                            </a>
                                        </label>
                                    </p>
                                </li>
                                <li><?php echo '฿ '.number_format($products[$i]['price']); ?></li>
                                <li><?php 
                                        for($i=0;$i<sizeof($products[$i]['investor']);$i++){
                                            if($products[$i]['investor'][$i]['_id'] == $profile['_id']){
                                                echo '฿ '.number_format($products[$i]['investor'][$i]['price']).'/ '.$products[$i]['investor'][$i]['interest'].'%';       
                                                break;
                                            }
                                        }
                                ?></li>
                                <li><?php echo $products[$i]['create_date']; ?></li>
                                <li><?php echo $products[$i]['status']; ?></li>
                                <li><a href="#" title="Edit"><i class="fa-pencil-square"></i></a> <a href="#" title="Delete"><i class="fa-close"></i></a></li>
                            </ul>
                        </li>
                    <?php 
                            }
                        }else{
                            echo '<li>ไม่มีข้อมูล</li>';
                        }
                    ?>              
                </ol>
            </div>        
            <!--<div class="loadmore">
                <a title="ดูทั้งหมด" href="#">ดูทั้งหมด</a>
            </div>-->
        </div>
    </div>
    <!-- /box -->    
    <nav class="txt-c">
        <?php echo $this->load->get_section('pageing');?>
    </nav>
</div>