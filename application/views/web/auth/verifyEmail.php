<div id="toc" class="container page-detail">
	<?php if(isset($error)){ ?> <p class="error"><?php echo $error?></p><?php }?>
	<section id="sec-detail">
		  <h1 class="topic"><span class="h-line">ยืนยันอีเมล์</span></h1>
			<p>กรุณายืนยันอีเมล์</p>
			<p> <a href="<?php echo base_url('/auth/reverify') ?>">ส่งข้อมูลยืนยันอีเมล์อีกครั้ง</a></p>
	</section>
</div>