<section id="sec-login" class="row _chd-cl-xs-12-sm-06">        
    <div class="z-login">
    	<h2 class="topic"><span class="h-line small">ACCOUNT LOGIN</span> <i class="fa-lock"></i></h2>
        <div class="box-mem">
            <form id="login" action="<?php echo base_url('/login')?>" method="post" name="login">
                <?php if($this->input->get_post('ref')){?>
                    <input type="hidden" name="ref" value="<?php echo $this->input->get_post('ref')?>" />

                <?php  }?>
                
                <fieldset>
                    <legend class="hid">เข้าสู่ระบบ</legend>
                        <?php 
                            if(isset($error)){?>
                                <p class="error"><?php echo $error ?></p>
                            <?php }
                        ?>
                        <p>
                            <label for="u-name">Email </label>
                            <input id="u-name" class="txt-box" type="email" name="email" required="">
                        </p>
                        <p>
                            <label for="u-pass">Password </label>
                            <input id="u-pass" class="txt-box" type="password" name="password" required="">
                        </p>
                        <p class="ctrl-bn">
                        	<input id="btn-log" class="ui-btn-green-login" type="submit" name="login" value="Login"> 
                            <input type="checkbox" name="isremember" id="isremember" value="true" /> <label for="isremember" style="display:inline">จดจำรหัสผ่าน</label>
                            
                        </p>
                </fieldset>
            </form>
        </div>
    </div>