    <div class="z-regis">
    	<h2 class="topic"><span class="h-line small">NEW ACCOUNT</span></h2>
        <div class="box-mem">
            <form id="register" action="<?php echo base_url('/register')?>" method="post" name="register">
                <?php if($this->input->get_post('ref')){?>
                    <input type="hidden" name="ref" value="<?php echo $this->input->get_post('ref')?>" />

                <?php  }?>
                <fieldset>
                    <legend class="hid">สมัครสมาชิก</legend>
                        <?php 
                            if(isset($error)){?>
                                <p class="error"><?php echo $error ?></p>
                            <?php }
                        ?>
                        <p>
                            <label for="new-name">Email Address</label>
                            <input id="new-name" class="txt-box" type="email" name="email" required="" value="<?php echo isset($email)?$email:""?>" >
                        </p>
                        <p>
                            <label for="new-pass">Choose Password</label>
                            <input id="new-pass" class="txt-box" type="password" name="password" required="">
                        </p>
                        <p>
                            <label for="cnew-pass">Confirm Password</label>
                            <input id="cnew-pass" class="txt-box" type="password" name="confirm_password" required="">
                        </p>
                        <p class="ctrl-bn">
                        	<input id="btn-log" class="ui-btn-green-login" type="submit" name="register" value="Register"> <span><a href="#" title="Security FAQ"><i class="fa-lock"></i> Security FAQ</a></span>
                        </p>
                </fieldset>
            </form>
        </div>
    </div>
</section>