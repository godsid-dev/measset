<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?php
		if(!empty($meta)){
			foreach($meta as $name=>$content){
				$tmp_content=is_array($content)?implode(", ", $content) : $content;
				echo "\n\t\t","<meta name='{$name}' content='{$tmp_content}' />";
			}
		}
		echo "\n\t";

		foreach($css as $file){ 
			echo "\n\t\t", "<link rel='stylesheet' href='{$file}' type='text/css' />";
		}
	?>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
    	<?php echo $this->load->get_section('header');?>
        <div id="page-wrapper">
        	<?php echo $output; ?>
        </div>
    </div>
<?php
	 foreach($js as $file){
		echo "\n\t\t","<script src='{$file}'></script>"; 
	 } 
?>	
</body>

</html>
