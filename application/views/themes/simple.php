<html lang="en">
	<head>
		<title><?php echo $title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Chrome, Firefox OS, Opera and Vivaldi -->
		<meta name="theme-color" content="#38AFAA">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#38AFAA">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#38AFAA">

		<?php
			/** -- Copy from here -- */
			if(!empty($meta))
			foreach($meta as $name=>$content){
				echo "\n\t\t";
				?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
					 }
			echo "\n";

			if(!empty($canonical))
			{
				echo "\n\t\t";
				?><link rel="canonical" href="<?php echo $canonical?>" /><?php

			}
			echo "\n\t";

			foreach($css as $file){
			 	echo "\n\t\t";
				?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
			} echo "\n\t";

			foreach($js as $file){
					echo "\n\t\t";
					?><script src="<?php echo $file; ?>"></script><?php
			} echo "\n\t";

			/** -- to here -- */
		?>
	    <!-- Le styles -->
	    <!--link href="<?php echo assets_url('/themes/default/hero_files/bootstrap.css'); ?>" rel="stylesheet">
	    <link href="<?php echo assets_url('/themes/default/hero_files/bootstrap-responsive.css'); ?>" rel="stylesheet"-->
	    <link href="<?php echo assets_url('/themes/default/img/favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />
    </head>

  <body>
  	<?php echo $this->load->get_section('header');?>
	<!-- /container -->
	<?php echo $output;?>
    <!-- /container -->
    <?php echo $this->load->get_section('footer');?>
	</body>
	
</html>