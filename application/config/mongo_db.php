<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------------
| This file will contain the settings needed to access your Mongo database.
|
|
| ------------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| ------------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['write_concerns'] Default is 1: acknowledge write operations.  ref(http://php.net/manual/en/mongo.writeconcerns.php)
|	['journal'] Default is TRUE : journal flushed to disk. ref(http://php.net/manual/en/mongo.writeconcerns.php)
|	['read_preference'] Set the read preference for this connection. ref (http://php.net/manual/en/mongoclient.setreadpreference.php)
|	['read_preference_tags'] Set the read preference for this connection.  ref (http://php.net/manual/en/mongoclient.setreadpreference.php)
|
| The $config['mongo_db']['active'] variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
*/

//var_dump(ENVIRONMENT);
//die();

$config['mongo_db']['active'] = ENVIRONMENT;

//var_dump($config['mongo_db']['active']);
//die();

$config['mongo_db']['default']['no_auth'] = FALSE;
$config['mongo_db']['default']['hostname'] = 'localhost';
$config['mongo_db']['default']['port'] = '27017';
$config['mongo_db']['default']['username'] = 'username';
$config['mongo_db']['default']['password'] = 'password';
$config['mongo_db']['default']['database'] = 'database';
$config['mongo_db']['default']['db_debug'] = TRUE;
$config['mongo_db']['default']['return_as'] = 'array';
$config['mongo_db']['default']['write_concerns'] = (int)1;
$config['mongo_db']['default']['journal'] = FALSE;
$config['mongo_db']['default']['read_preference'] = NULL;
$config['mongo_db']['default']['read_preference_tags'] = NULL;

//development
$config['mongo_db']['development']['no_auth'] = TRUE;
$config['mongo_db']['development']['hostname'] = 'mongodb';
$config['mongo_db']['development']['port'] = '27017';
$config['mongo_db']['development']['username'] = 'qassetdev';
$config['mongo_db']['development']['password'] = 'Y32m5EduT697';
$config['mongo_db']['development']['database'] = 'qasset';
$config['mongo_db']['development']['db_debug'] = TRUE;
$config['mongo_db']['development']['return_as'] = 'array';
$config['mongo_db']['development']['write_concerns'] = (int)1;
$config['mongo_db']['development']['journal'] = FALSE;
$config['mongo_db']['development']['read_preference'] = NULL;
$config['mongo_db']['development']['read_preference_tags'] = NULL;


//production
$config['mongo_db']['production']['no_auth'] = TRUE;
$config['mongo_db']['production']['hostname'] = '172.17.0.3';
$config['mongo_db']['production']['port'] = '27017';
$config['mongo_db']['production']['username'] = 'qasset@production';
$config['mongo_db']['production']['password'] = 'Y32m5EduT697';
$config['mongo_db']['production']['database'] = 'qasset';
$config['mongo_db']['production']['db_debug'] = TRUE;
$config['mongo_db']['production']['return_as'] = 'array';
$config['mongo_db']['production']['write_concerns'] = (int)1;
$config['mongo_db']['production']['journal'] = FALSE;
$config['mongo_db']['production']['read_preference'] = NULL;
$config['mongo_db']['production']['read_preference_tags'] = NULL;


/*user@qasset*/
$config['mongo_db']['user@qasset']['no_auth'] = FALSE;
$config['mongo_db']['user@qasset']['hostname'] = '192.168.101.44';
$config['mongo_db']['user@qasset']['port'] = '27017';
$config['mongo_db']['user@qasset']['username'] = 'user@qasset';
$config['mongo_db']['user@qasset']['password'] = 'Y32m5EduT697';
$config['mongo_db']['user@qasset']['database'] = 'qasset';
$config['mongo_db']['user@qasset']['db_debug'] = TRUE;
$config['mongo_db']['user@qasset']['return_as'] = 'array';
$config['mongo_db']['user@qasset']['write_concerns'] = (int)1;
$config['mongo_db']['user@qasset']['journal'] = FALSE;
$config['mongo_db']['user@qasset']['read_preference'] = NULL;
$config['mongo_db']['user@qasset']['read_preference_tags'] = NULL;

/*local*/
$config['mongo_db']['local']['no_auth'] = TRUE;
$config['mongo_db']['local']['hostname'] = 'mongo';
$config['mongo_db']['local']['port'] = '27017';
$config['mongo_db']['local']['username'] = 'admin';
$config['mongo_db']['local']['password'] = '1q2w3e4r';
$config['mongo_db']['local']['database'] = 'qasset';
$config['mongo_db']['local']['db_debug'] = TRUE;
$config['mongo_db']['local']['return_as'] = 'array';
$config['mongo_db']['local']['write_concerns'] = (int)1;
$config['mongo_db']['local']['journal'] = FALSE;
$config['mongo_db']['local']['read_preference'] = NULL;
$config['mongo_db']['local']['read_preference_tags'] = NULL;


/*local@porchn*/
$config['mongo_db']['local@porchn']['no_auth'] = TRUE;
$config['mongo_db']['local@porchn']['hostname'] = '128.199.216.138';
$config['mongo_db']['local@porchn']['port'] = '27017';
$config['mongo_db']['local@porchn']['username'] = 'qasset@dev';
$config['mongo_db']['local@porchn']['password'] = 'Y32m5EduT697';
$config['mongo_db']['local@porchn']['database'] = 'qasset';
$config['mongo_db']['local@porchn']['db_debug'] = TRUE;
$config['mongo_db']['local@porchn']['return_as'] = 'array';
$config['mongo_db']['local@porchn']['write_concerns'] = (int)1;
$config['mongo_db']['local@porchn']['journal'] = FALSE;
$config['mongo_db']['local@porchn']['read_preference'] = NULL;
$config['mongo_db']['local@porchn']['read_preference_tags'] = NULL;


/* End of file database.php */
/* Location: ./application/config/database.php */
