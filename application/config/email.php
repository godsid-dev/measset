<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'localhost';
$config['smtp_port'] = '25';
$config['smtp_timeout'] = 5;
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['validate'] = 'true';


if(ENVIRONMENT == 'local'){
	$config['protocol'] = 'smtp';
	$config['smtp_host'] = 'mail.measset.com';
	
}elseif(ENVIRONMENT == 'production'){
	$config['protocol'] = 'smtp';
	$config['smtp_host'] = 'mail.measset.com';
}


$config['from'] = 'webmaster@measset.com';
$config['from_name'] = 'Webmaster MeAsset';
$config['assessor'] = 'banpot.sr@gmail.com';
$config['admin'] = 'banpot.sr@gmail.com';