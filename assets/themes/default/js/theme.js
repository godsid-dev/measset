$(window).load(function() {
	 //expand nav
	$('.btn-nv-m .b-ex').click(function(){
		$(this).toggleClass('active');
		$('body').toggleClass('expand');
	});
	  /* scroll to animate */	
	if($('#sec-work').length > 0) {
		var top2 = $('#sec-work').offset().top - parseFloat($('#sec-work').prev().innerHeight() / 1.5) ;
		$(window).scroll(function (event) {
		var y2 = $(this).scrollTop();
		if (y2 >= top2) {$('#sec-work').addClass('animate');$('#sec-work .list-work>li').addClass('zoomInDown');} else {$('#sec-work').removeClass('animate');$('#sec-work .list-work>li').removeClass('zoomInDown');}
		});
	}
	/*animate*/
	if($('#sec-sell').length > 0) {
		var top3 = $('#sec-sell').offset().top - parseFloat($('#sec-sell').prev().innerHeight() / 1.1) ;
		$(window).scroll(function (event) {
		var y3 = $(this).scrollTop();
		if (y3 >= top3) {$('#sec-sell>.container').addClass('fadeInUp');} /*else {$('#sec-sell>.container,#sec-invest>.container').removeClass('fadeInUp');}*/
		});
	}
	if($('#sec-invest').length > 0) {
		var top4 = $('#sec-invest').offset().top - parseFloat($('#sec-invest').prev().innerHeight() / 1.1) ;
		$(window).scroll(function (event) {
		var y4 = $(this).scrollTop();
		if (y4 >= top4) {$('#sec-invest>.container').addClass('fadeInUp');} /*else {$('#sec-sell>.container,#sec-invest>.container').removeClass('fadeInUp');}*/
		});
	}

	//back to top
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('#back2top').css({bottom:"15px"});
		} else {
			$('#back2top').css({bottom:"-100px"});
		}
	});
	$('#back2top').click(function(){
		$('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	}); 

	/*scroll link*/
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});  

	$(".accordion").tabs(".accordion div.pane" , {tabs: '.AC3', effect: 'slide', initialIndex: null});
	$('.sec-acc .h-acc').click(function() {
		$(this).toggleClass('active').next().toggleClass('active').slideToggle(200);
    });

    $('.home .flexslider').flexslider({
		animation: "slide",
		controlNav: true,
		directionNav: true,
		animationLoop: true,
		slideshow: false,
		itemMargin: 0,
		slideshowSpeed: 8000, 
	});
   	//gallery
    $('#main-gr').flexslider({
	    animation: "fade",
	    controlNav: "thumbnails",
		slideshow: true,
		directionNav: true,
	    animationLoop: true,
		slideshowSpeed: 5000
    });

    $(".pop-nav>ul>li>a").removeClass("selected");
	$('.pop-nav>ul:nth-child(4)>li:nth-child(2)>a').addClass('selected');

	$(".lb-full-fancy").fancybox({
		maxWidth	: '100%',
		maxHeight	: '100%',
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		//closeBtn	: false,
		title			: false,
		closeEffect	: 'none'
	});
	$(".lb-fancy").fancybox({
		padding : '0',
		maxWidth	: '100%',
		maxHeight	: '100%',
		fitToView	: false,
		width		: '1024',
		height		: '500',
		autoSize	: false,
		closeClick	: false,
		//closeBtn	: false,
		title			: false,
		closeEffect	: 'none'
	});

});